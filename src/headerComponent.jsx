import React, { Component } from 'react';

class Header extends Component {
    
    state = {  }
    render() { 
        return ( 

            <header id="header" class="header-effect-shrink" data-plugin-options="{'stickyEnabled': false, 'stickyEffect': 'shrink', 'stickyEnableOnBoxed': false, 'stickyEnableOnMobile': false, 'stickyChangeLogo': false, 'stickyStartAt': 30, 'stickyHeaderContainerHeight': 70}">
											<div class="header-body">
					<div class="header-container container">
						<div class="header-row">
							<div class="header-column">
								<div class="header-row">
									<div class="header-logo" style={{width: "200px", height: "46.5625px" }} itemScope="" itemType="http://schema.org/Organization">
                        				<a itemProp="url" href="https://codecorp.com" >
                           					<img itemProp="logo" alt="Code Logo" width="200" height="auto" data-sticky-width="82" data-sticky-height="auto" src="https://codecorp.com/site/img/code_logo_300w.png" />
                        				</a>

                     				</div>
								</div>
							</div>
							<div class="header-column justify-content-end"> 
								<div class="header-row">
									<div class="header-nav header-nav-line header-nav-top-line header-nav-top-line-with-border order-2 order-lg-1">
										<div class="header-nav-main header-nav-main-square header-nav-main-effect-2 header-nav-main-sub-effect-1">


                        <nav class="collapse" aria-label="Navigation" itemScope="" itemType="https://schema.org/SiteNavigationElement">
                            <ul class="nav nav-pills" id="mainNav">
                            
                                        <li class="dropdown">
                                    <a class="dropdown-item dropdown-toggle text-2" href="https://codecorp.com/products" title="Products" >Products<i class="fas fa-chevron-down"></i></a>
                                                        <ul class="dropdown-menu">
                                                                                                    <li class="dropdown-submenu dropdown-reverse">
                                                        <a class="dropdown-item text-2" href="https://codecorp.com/products/hardware" title="Hardware" itemProp="url" >Hardware<i class="fas fa-chevron-down"></i></a>
                                                    </li>
                                                                                                                                <li class="dropdown-submenu dropdown-reverse">
                                                        <a class="dropdown-item text-2" href="https://codecorp.com/products/software" title="Software" itemProp="url" >Software<i class="fas fa-chevron-down"></i></a>
                                                    </li>
                                                                                                                                <li class="dropdown-submenu dropdown-reverse">
                                                        <a class="dropdown-item text-2" href="https://codecorp.com/products/services" title="Code Services" itemProp="url" >Code Services<i class="fas fa-chevron-down"></i></a>
                                                    </li>
                                                                                                                                <li class="dropdown-submenu dropdown-reverse">
                                                        <a class="dropdown-item text-2" href="https://codecorp.com/products/accessories" title="Accessories" itemProp="url" >Accessories<i class="fas fa-chevron-down"></i></a>
                                                    </li>
                                                                                                                                <li class="dropdown-submenu dropdown-reverse">
                                                        <a class="dropdown-item text-2" href="https://codecorp.com/products/why-code" title="Why Code" itemProp="url" >Why Code<i class="fas fa-chevron-down"></i></a>
                                                    </li>
                                                                                            </ul>
                                                </li> 
                                    
                            
                                        <li class="dropdown">
                                    <a class="dropdown-item dropdown-toggle text-2" href="https://codecorp.com/#" title="Solutions" >Solutions<i class="fas fa-chevron-down"></i></a>
                                                        <ul class="dropdown-menu">
                                                                                                    <li class="dropdown-submenu dropdown-reverse">
                                                        <a class="dropdown-item text-2" href="https://codecorp.com/solutions/developers" title="Software Developers" itemProp="url" >Software Developers<i class="fas fa-chevron-down"></i></a>
                                                    </li>
                                                                                                                                <li class="dropdown-submenu dropdown-reverse">
                                                        <a class="dropdown-item text-2" href="https://codecorp.com/solutions/healthcare" title="Healthcare / Medical" itemProp="url" >Healthcare / Medical<i class="fas fa-chevron-down"></i></a>
                                                    </li>
                                                                                                                                <li class="dropdown-submenu dropdown-reverse">
                                                        <a class="dropdown-item text-2" href="https://codecorp.com/solutions/ehr-overview" title="EHR Integrations" itemProp="url" >EHR Integrations<i class="fas fa-chevron-down"></i></a>
                                                    </li>
                                                                                                                                <li class="dropdown-submenu dropdown-reverse">
                                                        <a class="dropdown-item text-2" href="https://codecorp.com/solutions/industrial" title="Industrial / MFG" itemProp="url" >Industrial / MFG<i class="fas fa-chevron-down"></i></a>
                                                    </li>
                                                                                                                                <li class="dropdown-submenu dropdown-reverse">
                                                        <a class="dropdown-item text-2" href="https://codecorp.com/solutions/retail" title="Retail / Commercial" itemProp="url" >Retail / Commercial<i class="fas fa-chevron-down"></i></a>
                                                    </li>
                                                                                                                                <li class="dropdown-submenu dropdown-reverse">
                                                        <a class="dropdown-item text-2" href="https://codecorp.com/solutions/fips" title="FIPS" itemProp="url" >FIPS<i class="fas fa-chevron-down"></i></a>
                                                    </li>
                                                                                                                                <li class="dropdown-submenu dropdown-reverse">
                                                        <a class="dropdown-item text-2" href="https://codecorp.com/solutions/oem" title="OEM" itemProp="url" >OEM<i class="fas fa-chevron-down"></i></a>
                                                    </li>
                                                                                            </ul>
                                                </li> 
                                    
                            
                                        <li class="dropdown">
                                    <a class="dropdown-item dropdown-toggle text-2" href="https://codecorp.com/#" title="Partners" >Partners<i class="fas fa-chevron-down"></i></a>
                                                        <ul class="dropdown-menu">
                                                                                                    <li class="dropdown-submenu dropdown-reverse">
                                                        <a class="dropdown-item text-2" href="https://codecorp.com/partners/promos" title="Promos" itemProp="url" >Promos<i class="fas fa-chevron-down"></i></a>
                                                    </li>
                                                                                                                                <li class="dropdown-submenu dropdown-reverse">
                                                        <a class="dropdown-item text-2" href="https://codecorp.com/partners/code-alliance" title="CodeAlliance" itemProp="url" >CodeAlliance<i class="fas fa-chevron-down"></i></a>
                                                    </li>
                                                                                                                                <li class="dropdown-submenu dropdown-reverse">
                                                        <a class="dropdown-item text-2" href="https://codecorp.com/solutions/oem" title="OEMs" itemProp="url" >OEMs<i class="fas fa-chevron-down"></i></a>
                                                    </li>
                                                                                                                                <li class="dropdown-submenu dropdown-reverse">
                                                        <a class="dropdown-item text-2" href="https://codecorp.com/partners/deal-registration" title="Deal Registration" itemProp="url" >Deal Registration<i class="fas fa-chevron-down"></i></a>
                                                    </li>
                                                                                                                                <li class="dropdown-submenu dropdown-reverse">
                                                        <a class="dropdown-item text-2" href="https://codecorp.com/partners/partner-portal" title="Partner Portal" itemProp="url" >Partner Portal<i class="fas fa-chevron-down"></i></a>
                                                    </li>
                                                                                                                                <li class="dropdown-submenu dropdown-reverse">
                                                        <a class="dropdown-item text-2" href="https://devportal.codecorp.com/" title="Developer Portal" itemProp="url" >Developer Portal<i class="fas fa-chevron-down"></i></a>
                                                    </li>
                                                                                            </ul>
                                                </li> 
                                    
                            
                                        <li class="dropdown">
                                    <a class="dropdown-item dropdown-toggle text-2" href="https://codecorp.com/#" title="About" >About<i class="fas fa-chevron-down"></i></a>
                                                        <ul class="dropdown-menu">
                                                                                                    <li class="dropdown-submenu dropdown-reverse">
                                                        <a class="dropdown-item text-2" href="https://codecorp.com/about" title="About Us" itemProp="url" >About Us<i class="fas fa-chevron-down"></i></a>
                                                    </li>
                                                                                                                                <li class="dropdown-submenu dropdown-reverse">
                                                        <a class="dropdown-item text-2" href="https://codecorp.com/about/careers" title="Careers" itemProp="url" >Careers<i class="fas fa-chevron-down"></i></a>
                                                    </li>
                                                                                                                                <li class="dropdown-submenu dropdown-reverse">
                                                        <a class="dropdown-item text-2" href="https://codecorp.com/events" title="Events &amp; Shows" itemProp="url" >Events &amp; Shows<i class="fas fa-chevron-down"></i></a>
                                                    </li>
                                                                                                                                <li class="dropdown-submenu dropdown-reverse">
                                                        <a class="dropdown-item text-2" href="https://codecorp.com/about/sales-team" title="Sales Team" itemProp="url" >Sales Team<i class="fas fa-chevron-down"></i></a>
                                                    </li>
                                                                                                                                <li class="dropdown-submenu dropdown-reverse">
                                                        <a class="dropdown-item text-2" href="https://codecorp.com/about/combined-media" title="News, Content &amp; Media" itemProp="url" >News, Content &amp; Media<i class="fas fa-chevron-down"></i></a>
                                                    </li>
                                                                                            </ul>
                                                </li> 
                                    
                            
                                        <li class="dropdown">
                                    <a class="dropdown-item dropdown-toggle text-2" href="https://codecorp.com/#" title="Support" >Support<i class="fas fa-chevron-down"></i></a>
                                                        <ul class="dropdown-menu">
                                                                                                    <li class="dropdown-submenu dropdown-reverse">
                                                        <a class="dropdown-item text-2" href="https://codecorp.com/support/rma-request" title="RMA / Support" itemProp="url" >RMA / Support<i class="fas fa-chevron-down"></i></a>
                                                    </li>
                                                                                                                                <li class="dropdown-submenu dropdown-reverse">
                                                        <a class="dropdown-item text-2" href="https://codecorp.com/support/config-guides" title="Config Catalog" itemProp="url" >Config Catalog<i class="fas fa-chevron-down"></i></a>
                                                    </li>
                                                                                                                                <li class="dropdown-submenu dropdown-reverse">
                                                        <a class="dropdown-item text-2" href="https://codecorp.com/support/mcodes" title="Device Configuration" itemProp="url" >Device Configuration<i class="fas fa-chevron-down"></i></a>
                                                    </li>
                                                                                                                                <li class="dropdown-submenu dropdown-reverse">
                                                        <a class="dropdown-item text-2" href="https://licensing.codecorp.com/goto.php" title="License Generation" itemProp="url" >License Generation<i class="fas fa-chevron-down"></i></a>
                                                    </li>
                                                                                                                                <li class="dropdown-submenu dropdown-reverse">
                                                        <a class="dropdown-item text-2" href="https://codecorp.com/about/patent-marking" title="Patent Marking" itemProp="url" >Patent Marking<i class="fas fa-chevron-down"></i></a>
                                                    </li>
                                                                                                                                <li class="dropdown-submenu dropdown-reverse">
                                                        <a class="dropdown-item text-2" href="https://codecorp.com/support/privacy" title="Privacy Policy" itemProp="url" >Privacy Policy<i class="fas fa-chevron-down"></i></a>
                                                    </li>
                                                                                                                                <li class="dropdown-submenu dropdown-reverse">
                                                        <a class="dropdown-item text-2" href="https://codecorp.com/support/eula" title="EULA Statement" itemProp="url" >EULA Statement<i class="fas fa-chevron-down"></i></a>
                                                    </li>
                                                                                                                                <li class="dropdown-submenu dropdown-reverse">
                                                        <a class="dropdown-item text-2" href="https://codecorp.com/support/warranty-coverage-terms" title="Warranty Terms" itemProp="url" >Warranty Terms<i class="fas fa-chevron-down"></i></a>
                                                    </li>
                                                                                                                                <li class="dropdown-submenu dropdown-reverse">
                                                        <a class="dropdown-item text-2" href="https://codecorp.com/support/warranty" title="Limited Warranty" itemProp="url" >Limited Warranty<i class="fas fa-chevron-down"></i></a>
                                                    </li>
                                                                                            </ul>
                                                </li> 
                                    
                            
                                                <li class="dropdown dropdown-reverse">
                                            <a class="dropdown-item dropdown-toggle text-2" href="https://codecorp.com/contact" title="Contact" >Contact<i class="fas fa-chevron-down"></i></a>
                                                        </li> 
                                            
                                    
                                            
                                            </ul>
                                        </nav>
										</div>
										<button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main nav">
											<i class="fas fa-bars"></i>
										</button>
									</div>

									


									<div class="header-nav-features header-nav-features-no-border header-nav-features-lg-show-border order-1 order-lg-2">
										<div class="header-nav-feature header-nav-features-search d-inline-flex">
											<a href="#" class="header-nav-features-toggle" data-focus="headerSearch" ><i class="fas fa-search header-nav-top-icon"></i></a>
											<div class="header-nav-features-dropdown" id="headerTopSearchDropdown">

												<form role="search" action="https://codecorp.com/search/result" method="GET">
													
													<div class="simple-search input-group">
														<input class="form-control text-1" id="headerSearch" name="q" type="search" value="" placeholder="Search..." />
														<span class="input-group-append">
															<button class="btn" type="submit">
																<i class="fa fa-search header-nav-top-icon"></i>
															</button>
														</span>
													</div>
												</form>

											</div>
										</div>
									</div>


								</div>
							</div>
						</div>
					</div>
                    <div class="config-header">
                        <div id="top-block" class="col-md-12 align-self-center p-static order-2 text-center" style={{backgroundColor:"#D3D3D3"}}>
                            <br /><h1 class="process-header text-white font-weight-bold text-6 tshadow">Advanced Configuation</h1><br />
                        </div>
                    </div>
				</div>
 

            </header>

        //     <div>
        //     <header id="header" style = {{backgroundColor : "#e1e1e1", padding : "30px"}}>
        //         <div style = {{textAlign : "center"}} className="header-container">
        //             <img style = {{justifySelf : "left"}} src="CODE-Expect-More-2.png"></img>
        //             <h1 style={{color : "#000000", display : "inline"}}>Cortex Web</h1>
        //         </div>
        //     </header>
        // </div>  
        );
    }
}
 
export default Header;