import React from 'react';
import cancelpic from './img/cancel.png';
import deletepic from './img/delete.png';
import Multiselect from "multiselect-react-dropdown";
import {TextField} from '@material-ui/core';
import { useState, useEffect } from 'react';
import {Button} from '@material-ui/core';


var FormatOutputSelected = "";
var AIMOptionIsSelected = "not selected";
var PrefixSelected = "";
var SendPartialBarcode = [];
var CharacterToInsert = [];
var SuffixSelected = "";
var lastDataInserted = "";
var optionSelected;
var AIMSelected;
var partialBcodeOption = "";
var insertCharoption = "";
var optionsSelectedInTotal = "\n";
var handleSpecialChars = "";
var endingPositionString = "0";

export function AllOptionsSelectedDataFormatting(){
    return optionsSelectedInTotal;
}

export function FormatOutput() {
    return FormatOutputSelected;
}
export function AIMIDOselected(){
    return AIMOptionIsSelected; 
}
export function PrefixInserted() {
    return PrefixSelected;  
}
export function SendPartialInserted() {
    return SendPartialBarcode;  
}
export function CharToInsertSelected(){
    return CharacterToInsert;   
}
export function SuffixInserted() {
    return SuffixSelected;  
}

export function DataFormatting({effectOnParsing}) {
    var [FormatOutputoptions, setFormatOutputOptions] = useState(["No Change", "Hexadecimal", "Uppercase", "Lowercase"]);
    var [AIMIDOptions, setAIMIDOptions] = useState(["Before Prefix", "After Prefix"]);
    var [FormatOutputString, setFormatOutputString] = useState();
    var [formatSample, setFormatSample] = useState("");
    var [AIMIDString, setAIMIDString] = useState();
    var [AIMIDSample, setAIMIDSample] = useState("");
    var [PrefixString, setPrefixString] = useState();
    var [PrefixSample, setPrefixSample] = useState("");
    var [partialBarcodeString, setPartialBarcodeString] = useState();
    var [partialBarcodeSample, setPartialBarcodeSample] = useState("");
    var [insertCharString, setInsertCharString] = useState();
    var [insertCharSample, setinsertCharSample] = useState("");
    var [SufixString, setSufixString] = useState();
    var [SufixSample, setSufixSample] = useState("");
    const [option1, setOption1] = useState(false);
    const [option2, setOption2] = useState(false);
    const [option3, setOption3] = useState(false);
    const [option4, setOption4] = useState(false);
    const [option5, setOption5] = useState(false);
    const [option6, setOption6] = useState(false);
    var finalBarcodeSample = effectOnParsing;
    
   
    const hex = require('string-hex');
    var barcodeSample = effectOnParsing; 

    useEffect(() => {
        
        if(formatSample !== ""){
            handleFormatoptions();
        }
        if(AIMIDSample !== ""){
            handleAIMIDOptions();
        }
        if(PrefixSample !== ""){
            insertPrefix();
        }
        if(partialBarcodeSample !== ""){
            partialBarcode();
        }
        if(insertCharSample !== ""){ 
           insertChar();
        }
        if(SufixSample !== ""){ 
            insertSufix();
        }
      }, [effectOnParsing]);
    
   function handleFormatoptions(e){
    if(e !== undefined){
         optionSelected = e.toString();
         console.log(optionSelected);
        
    }
    
    lastDataInserted = "FormatOutput"; 
    if(optionSelected === "No Change"){
        setFormatOutputString("<Output Format case>No Change");
        setFormatSample(barcodeSample);
    }
    if(optionSelected === "Hexadecimal"){
        setFormatOutputString("<Output Format case>Hexadecimal");
       var hexBarcode = hex(barcodeSample);
       setFormatSample(hexBarcode.toUpperCase());
       FormatOutputSelected = "hexadecimal";
       barcodeSample = hexBarcode.toUpperCase();
    }
    if(optionSelected === "Uppercase"){
        setFormatOutputString("<Output Format case>Uppercase");
        setFormatSample(barcodeSample.toUpperCase());
        FormatOutputSelected = "uppercase";
        barcodeSample = barcodeSample.toUpperCase();
    }
    if(optionSelected === "Lowercase"){
        setFormatOutputString("<Output Format case>Lowercase");
        setFormatSample(barcodeSample.toLowerCase());
        FormatOutputSelected = "lowercase";
        barcodeSample = barcodeSample.toLowerCase();
    }
   }
   
   function handleAIMIDOptions(e){
    
    if(e !== undefined){
        AIMSelected = e.toString();
        console.log(AIMSelected);
       
   }
       lastDataInserted = "AIMID";
    if(AIMSelected === "Before Prefix"){
        setAIMIDString("<AIMD-ID>Before Prefix");
        setAIMIDSample("]HH" + barcodeSample);
        AIMOptionIsSelected = "before"
    }
    if(AIMSelected === "After Prefix"){
        setAIMIDString("<AIMD-ID>After Prefix");
        setAIMIDSample("]HH" + barcodeSample);
        AIMOptionIsSelected = "after";
    }
   }

  const [prefixStringValue, setPrefixStringValue] = useState("");
   function insertPrefix(e){
    lastDataInserted = "Prefix"; 
    setPrefixStringValue(e);
    var prefix = e;
    PrefixSelected = e;
    setPrefixString("<Insert Prefix>" + prefix);
    if(AIMOptionIsSelected !== "not selected"){
        if(AIMOptionIsSelected === "before"){       
            setPrefixSample("]HH" + prefix + barcodeSample);
        }
        if(AIMOptionIsSelected === "after"){
            setPrefixSample(prefix + "]HH" + barcodeSample);
        }       
    }else{
        setPrefixSample(prefix + barcodeSample);
    }
   }

   const [partialStart, setPartialStart] = useState(0);
   const [partialEnd, setPartialEnd] = useState(0);
   

   function partialbarcodeEnd(e){
    setPartialEnd(e);
    endingPositionString = e;
    partialBarcode(partialStart);
   }

   function partialBarcode(e){
    lastDataInserted = "PartialBarcode";
    setPartialStart(e); 
    var startingPosition = e;
    var endingPosition = endingPositionString;
    setPartialBarcodeString("<Send partial>Position " + startingPosition + " To Position " + endingPosition);
    if(endingPosition == "0"){
        
        endingPosition = 50;
        SendPartialBarcode = [startingPosition, "End"];   
    }else{
        SendPartialBarcode = [startingPosition, endingPosition]; 
    }
    console.log(endingPosition);
    var barcodeSplitted = barcodeSample.substring((startingPosition - 1), endingPosition);
    console.log(barcodeSplitted);
    
    if(AIMOptionIsSelected !== "not selected"){
        if(AIMOptionIsSelected === "before"){ 
            if(PrefixSelected !== ""){
                setPartialBarcodeSample("]HH" + PrefixSelected + barcodeSplitted);
                partialBcodeOption = "]HH" + PrefixSelected + barcodeSplitted;
            }else{
                setPartialBarcodeSample("]HH"  + barcodeSplitted);
                partialBcodeOption = "]HH"  + barcodeSplitted;
            }            
        }
        if(AIMOptionIsSelected === "after"){
            if(PrefixSelected !== ""){
                setPartialBarcodeSample(PrefixSelected + "]HH" + barcodeSplitted);
                partialBcodeOption = PrefixSelected + "]HH" + barcodeSplitted;
            }else{
                setPartialBarcodeSample("]HH"  + barcodeSplitted);
                partialBcodeOption = "]HH"  + barcodeSplitted;
            }  
           
        }       
    }else{
        setPartialBarcodeSample(PrefixSelected + barcodeSplitted);
        partialBcodeOption = PrefixSelected + barcodeSplitted;
    }

   }

const [insertCharStringValue, setInsertCharStringValue] = useState("");
const [afterPositionValue, setAfterPositionValue] = useState(0);
var afterPositionVariable = "0";
function afterPositionFunction(e){
setAfterPositionValue(e);
afterPositionVariable = e;
insertChar(insertCharStringValue);
}
  
   function insertChar(e){
    lastDataInserted = "Char"; 
    setInsertCharStringValue(e);
    var charToinsert = e;
    var afterPosition = afterPositionVariable;
    CharacterToInsert = [charToinsert, afterPosition];
    setInsertCharString("<Insert Characters>" + charToinsert + " After Position " + afterPosition);
    var barcodeSplitted1 = barcodeSample.substring(0, afterPosition);
    var barcodeSplitted2 = barcodeSample.substring(afterPosition, 50);
    if(partialBarcodeSample !== ""){
        setinsertCharSample(partialBcodeOption + barcodeSplitted1 + charToinsert + barcodeSplitted2);
        insertCharoption = partialBcodeOption + barcodeSplitted1 + charToinsert + barcodeSplitted2;
    }else{
        setinsertCharSample(PrefixSelected + barcodeSplitted1 + charToinsert + barcodeSplitted2);
        insertCharoption = PrefixSelected + barcodeSplitted1 + charToinsert + barcodeSplitted2;
    }
    
    
   }


  const [suffixValue, setSuffixValue] = useState("");
   function insertSufix(e){
       
       lastDataInserted = "Suffix"; 
       setSuffixValue(e);
        var suffix = e;
       SuffixSelected = suffix;
       setSufixString("<Insert Suffix>" + suffix);
       if(insertCharSample !== ""){
        
           setSufixSample(insertCharoption + suffix);
       }else{
        if(partialBarcodeSample !== ""){
            setSufixSample(partialBarcodeSample + suffix);
        }else{
            if(PrefixSelected !== ""){
                setSufixSample(PrefixSelected + barcodeSample + suffix);
            }else{
                setSufixSample(barcodeSample + suffix);
            }
            
        }
       }
       
   }
   const RemoveLastDataEntered = () =>{  
    setshowButtonOptions(true);
    setShowOptionsToDelete(false);
    
      
   }
   const[showInsertChar, setShowInsertChar] = useState(false);
   const[showPrefixLabel, setShowPrefixLabel] = useState(false);
   const[showSendPartial, setShowSendPartial] = useState(false);
   const[showInsertSuffix, setShowInsertSuffix] = useState(false);
   
const NULL = () =>{
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + "<NULL>";
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + "<NULL>";
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + "<NULL>";
        insertSufix(newString3);
    }
    
}
const SOH = () =>{
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + "<SOH>";
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + "<SOH>";
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + "<SOH>";
        insertSufix(newString3);
    }
}
const STX = () =>{
    
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + "<STX>";
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + "<STX>";
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + "<STX>";
        insertSufix(newString3);
    }
}
const ETX = () =>{
  
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + "<ETX>";
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + "<ETX>";
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + "<ETX>";
        insertSufix(newString3);
    }
}
const EOT = () =>{
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + "<EOT>";
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + "<EOT>";
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + "<EOT>";
        insertSufix(newString3);
    }
}
const ENQ = () =>{

    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + "<ENQ>";
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + "<ENQ>";
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + "<ENQ>";
        insertSufix(newString3);
    }
}
const ACK = () =>{

    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + "<ACK>";
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + "<ACK>";
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + "<ACK>";
        insertSufix(newString3);
    }
}
const BEL = () =>{
    
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + "<BEL>";
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + "<BEL>";
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + "<BEL>";
        insertSufix(newString3);
    }
}
const BS = () =>{
    var charValue = "<BS>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }
}
const TAB = () =>{
    var charValue = "<TAB>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }
}
const LF = () =>{
    var charValue = "<LF-NL>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }
}
const VT = () =>{
    var charValue = "<VT>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }
}
const FF = () =>{
    var charValue = "<FF-NP>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }
}
const CR = () =>{
    var charValue = "<CR>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }
}
const SO = () =>{
    var charValue = "<SO>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }
}
const SI = () =>{
    var charValue = "<SI>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }
}
const DLE = () =>{
    var charValue = "<DLE>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }
}
const DC1 = () =>{
    var charValue = "<DC1>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }
}
const DC2 = () =>{
    var charValue = "<DC2>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }
}
const DC3 = () =>{
    var charValue = "<DC3>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }
}
const DC4 = () =>{
    var charValue = "<DC4>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }
}
const NAK = () =>{
    var charValue = "<NAK>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }
}
const SYN = () =>{
    var charValue = "<SYN>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }
}
const ETB = () =>{
    var charValue = "<ETB>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }
}
const CAN = () =>{
    var charValue = "<CAN>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }
}
const EM = () =>{
    var charValue = "<EM>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }
}
const SUB = () =>{
    var charValue = "<SUB>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }
}
const ESC = () =>{
    var charValue = "<ESC>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }
}
const FS = () =>{
    var charValue = "<FS>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }
}
const GS = () =>{
    var charValue = "<GS>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }
}
const RS = () =>{
    var charValue = "<RS>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }
}
const US = () =>{
    var charValue = "<US>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }
}
const DEL = () =>{
    var charValue = "<DEL>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }
}

const UpArrow = () =>{
    var charValue = "<Up Arrow-Keyboard>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }
}
const LeftArrow = () =>{
    var charValue = "<Left Arrow-Keyboard>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }
}
const RightArrow = () =>{
    var charValue = "<Right Arrow-Keyboard>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }
}
const DownArrow = () =>{
    var charValue = "<Down Arrow-Keyboard>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }
}
const Tab = () =>{
    var charValue = "<Tab-Keyboard>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }
}
const Delete = () =>{
    var charValue = "<Delete-Keyboard>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }
}
const Esc = () =>{
    var charValue = "<Esc-Keyboard>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }
}
const Enter = () =>{
    var charValue = "<Enter-Keyboard>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }
}
const End = () =>{
    var charValue = "<End-Keyboard>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }
}
const Backspace = () =>{
    var charValue = "<Backspace-Keyboard>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }
}
const Insert = () =>{
    var charValue = "<Insert-Keyboard>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }
}
const PageUp = () =>{
    var charValue = "<Page Up-Keyboard>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }
}
const PageDown = () =>{
    var charValue = "<Page Down-Keyboard>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }
}
const Home = () =>{
    var charValue = "<Home-Keyboard>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }
}
const Number0 = () =>{
    var charValue = "<0>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }
}
const Number1 = () =>{
    var charValue = "<1>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }
}
const Number2 = () =>{
    var charValue = "<2>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }
}
const Number3 = () =>{
    var charValue = "<3>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }}
const Number4 = () =>{
    var charValue = "<4>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }}
const Number5 = () =>{
    var charValue = "<5>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }}
const Number6 = () =>{
    var charValue = "<6>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }}
const Number7 = () =>{
    var charValue = "<7>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }}
const Number8 = () =>{
    var charValue = "<8>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }}
const Number9 = () =>{
    var charValue = "<9>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }}
const F1 = () =>{
    var charValue = "<F1-Keyboard>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }}
const F2 = () =>{
    var charValue = "<F2-Keyboard>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }}
const F3 = () =>{
    var charValue = "<F3-Keyboard>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }}
const F4 = () =>{
    var charValue = "<F4-Keyboard>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }}
const F5 = () =>{
    var charValue = "<F5-Keyboard>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }}
const F6 = () =>{
    var charValue = "<F6-Keyboard>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }}
const F7 = () =>{
    var charValue = "<F7-Keyboard>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }}
const F8 = () =>{
    var charValue = "<F8-Keyboard>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }}
const F9 = () =>{
    var charValue = "<F9-Keyboard>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }}
const F10 = () =>{
    var charValue = "<F10-Keyboard>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }}
const F11 = () =>{
    var charValue = "<F11-Keyboard>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }}
const F12 = () =>{
    var charValue = "<F12-Keyboard>";
    if(handleSpecialChars === "prefix"){
        var newString = prefixStringValue + charValue;
        insertPrefix(newString);
    }
    if(handleSpecialChars === "insertChars"){
        var newString2 = insertCharStringValue + charValue;
        insertChar(newString2);
    }
    if(handleSpecialChars === "suffix"){
        var newString3 = suffixValue + charValue;
        insertSufix(newString3);
    }}
    const [showKeyboardChars, setShowKeyboardChars] = useState(false);
    const [showNonPrintableChars, setShowNonPrintableChars] = useState(false);
const KeyboardChars = () =>{
    handleSpecialChars = "prefix";  
   if(showKeyboardChars === false){
    setShowKeyboardChars(true);
    setShowNonPrintableChars(false);
   }else{
    setShowKeyboardChars(false);
   }
}
const nonPrintableChars = () =>{
    handleSpecialChars = "prefix";
     if(showNonPrintableChars === false){
        setShowKeyboardChars(false);
        setShowNonPrintableChars(true);
       }else{
        setShowNonPrintableChars(false);
       }
}
const [showInsertKeyboardChars, setShowInsertKeyboardChars] = useState(false);
const [showInsertNonPrintableChars, setShowInsertNonPrintableChars] = useState(false);
const KeyboardChars2 = () =>{
    handleSpecialChars = "insertChars";
    if(showInsertKeyboardChars === false){
        setShowInsertKeyboardChars(true);
        setShowInsertNonPrintableChars(false);
       }else{
        setShowInsertKeyboardChars(false);
       }
}
const nonPrintableChars2 = () =>{
    handleSpecialChars = "insertChars";
    if(showInsertNonPrintableChars === false){
        setShowInsertKeyboardChars(false);
        setShowInsertNonPrintableChars(true);
       }else{
        setShowInsertNonPrintableChars(false);
       }
}
const [showSuffixKeyboardChars, setShowSuffixKeyboardChars] = useState(false);
const [showSuffixNonPrintableChars, setShowsuffixNonPrintableChars] = useState(false);
const KeyboardCharsSuffix = () =>{
    handleSpecialChars = "suffix";
    if(showSuffixKeyboardChars === false){
        setShowSuffixKeyboardChars(true);
        setShowsuffixNonPrintableChars(false);
       }else{
        setShowSuffixKeyboardChars(false);
       }
}
const nonPrintableCharsSuffix = () =>{
    handleSpecialChars = "suffix";  
    if(showSuffixNonPrintableChars === false){
        setShowSuffixKeyboardChars(false);
        setShowsuffixNonPrintableChars(true);
       }else{
        setShowsuffixNonPrintableChars(false);
       }
}
const insertCharHidden = () =>{
    
    if(showInsertChar === false){
        setShowInsertChar(true);
    }else{
        setShowInsertChar(false);
    }
}
const prefixOptionHidden = () =>{
    if(showPrefixLabel === false){
        setShowPrefixLabel(true);
    }else{
        setShowPrefixLabel(false);
    }
}
const sendPartialHidden = () =>{
    
    if(showSendPartial === false){
        setShowSendPartial(true);
    }else{
        setShowSendPartial(false);
    }
}
const insertSuffixHidden = () =>{
    
    if(showInsertSuffix === false){
        setShowInsertSuffix(true);
    }else{
        setShowInsertSuffix(false);
    }
}
const [showOptionsToDelete, setShowOptionsToDelete] = useState(true);
const [showButtonsOptions, setshowButtonOptions] = useState(false);
const RemoveAllDataEntered = () =>{
    
    setFormatOutputString("");
    setFormatSample("");
    FormatOutputSelected = "";
   
   
    setAIMIDString("");
    setAIMIDSample("");
    AIMOptionIsSelected = "not selected";
   
 
    setPrefixSample("");
    setPrefixString("");
    PrefixSelected = "";
    setPrefixStringValue("");

    setPartialBarcodeString("");
    setPartialBarcodeSample("");
    SendPartialBarcode = ""; 
    setPartialStart(0);
    setPartialEnd(0);
 
    CharacterToInsert = "";
    setInsertCharString("");
    setinsertCharSample("");
    setInsertCharStringValue("");
   
   
    SuffixSelected = "";
    setSufixString("");
    setSufixSample("");
    setSuffixValue("");

   setShowInsertChar(false);
   setShowPrefixLabel(false);
   setShowSendPartial(false);
   setShowInsertSuffix(false);
   setShowKeyboardChars(false);
   setShowNonPrintableChars(false);
   setShowInsertNonPrintableChars(false);
   setShowInsertKeyboardChars(false);
   setShowSuffixKeyboardChars(false);
   setShowsuffixNonPrintableChars(false);
   
   setshowButtonOptions(false);
   setShowOptionsToDelete(true); 
}
const deleteOptionsSelected = () =>{
    
    
    if(option1 === true){
        setFormatOutputString("");
        setFormatSample("");
        FormatOutputSelected = "";
        setOption1(false);
    }
    if(option2 === true){
        setAIMIDString("");
        setAIMIDSample("");
        AIMOptionIsSelected = "not selected";
        setOption2(false);
    }
    if(option3 === true){
        setPrefixSample("");
        setPrefixString("");
        PrefixSelected = "";
        setPrefixStringValue("");
        setOption3(false);
    }
    if(option4 === true){
        setPartialBarcodeString("");
        setPartialBarcodeSample("");
        SendPartialBarcode = ""; 
        setPartialStart(0);
        setPartialEnd(0);
        setOption4(false);
    }
    if(option5 === true){
    setInsertCharStringValue("");
     CharacterToInsert = "";
     setInsertCharString("");
     setinsertCharSample("");
     setOption5(false);
    }
    if(option6 === true){
        SuffixSelected = "";
        setSufixString("");
        setSufixSample("");
        setSuffixValue("");
        setOption6(false);
    }
    
    setshowButtonOptions(false);
    setShowOptionsToDelete(true);
 }
const FOOption = () =>{
    setOption1(!option1); 
 }
 const IAIMOption = () =>{
    setOption2(!option2);
 }
 const IPOption = () =>{
    setOption3(!option3);
 }
 const SPOption = () =>{
    setOption4(!option4);
 }
 const ICOption = () =>{
    setOption5(!option5);
 }
 const ISOption = () =>{
    setOption6(!option6);
 }
 const CancelButton = () =>{
    
    setshowButtonOptions(false);
   setShowOptionsToDelete(true);
 }
optionsSelectedInTotal = FormatOutputString + "\n" + AIMIDString + "\n" + PrefixString + "\n" + partialBarcodeString + "\n" + insertCharString + "\n" + SufixString;
optionsSelectedInTotal = optionsSelectedInTotal.replace(/undefined/g, "");
        return ( 
            <>
                        <small><p class="text">Data-Formatting Operations</p></small>


                            <div class="box2">
                                    <div className="dataformattingoptions">
                                    {showOptionsToDelete &&     
                                    <div id="buttonsoptions">
                                    <div className="overtext"><small>Format Output Options: </small></div>
                                    <Multiselect id="css_custom" 
                                    isObject={false}
                                    options={FormatOutputoptions} 
                                    onSelect={handleFormatoptions}
                                    singleSelect
                                    style={{  
                                    multiselectContainer: {
                                    width: '260px',   
                                     } }}
                                    />
                                    
                                    <div className="overtext"><small>Insert AIM-ID: </small></div>
                                    <Multiselect id="css_custom" 
                                    isObject={false}
                                    options={AIMIDOptions} 
                                    onSelect={handleAIMIDOptions}
                                    singleSelect
                                    style={{  
                                    multiselectContainer: {
                                    width: '260px',   
                                     } }}
                                    /><br/>
                                    <button className="MatchButtons" onClick={prefixOptionHidden}><small>Insert Prefix</small></button>
                                    {showPrefixLabel &&
                                    <div id="prefixLabel" >
                                    <TextField className="MatchButtons" id="PrefixTextfield" label="Insert Prefix" variant="outlined" value={prefixStringValue} onChange={(e) => insertPrefix(e.target.value)}/>
                                    <div class="divSmallButtons" >
                                    <button className="smallButtons" onClick={nonPrintableChars}><small><small>Non-Printable Chars</small></small></button>&nbsp;<button className="smallButtons" onClick={KeyboardChars}><small><small>keyboard Chars</small></small></button>
                                    </div>
                                    {showNonPrintableChars && 
                                    <div id="Nonprintable" className="NonPrintable">
                                        <table>
                                        
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={NULL}><small>NULL</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={SOH}><small>SOH</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={STX}><small>STX</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={ETX}><small>ETX</small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={EOT}><small>EOT</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={ENQ}><small>ENQ</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={ACK}><small>ACK</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={BEL}><small>BEL</small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={BS}><small>BS</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={TAB}><small>TAB</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={LF}><small>LF/NL</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={VT}><small>VT</small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={FF}><small>FF/NP</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={CR}><small>CR</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={SO}><small>SO</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={SI}><small>SI</small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={DLE}><small>DLE</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={DC1}><small>DC1</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={DC2}><small>DC2</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={DC3}><small>DC3</small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={DC4}><small>DC4</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={NAK}><small>NAK</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={SYN}><small>SYN</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={ETB}><small>ETB</small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={CAN}><small>CAN</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={EM}><small>EM</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={SUB}><small>SUB</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={ESC}><small>ESC</small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={FS}><small>FS</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={GS}><small>GS</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={RS}><small>RS</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={US}><small>US</small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={DEL}><small>DEL</small></button>
                                                        </td>
                                                       
                                                    </tr>
                                                   
                                                    
                                        </table>

                                    </div>
                                    }
                                    {showKeyboardChars && 
                                    <div id="KeyboardChars" className="KeyboardChars">
                                        <table>
                                        
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={UpArrow}><small><small>Up arrow</small></small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={LeftArrow}><small><small>Left Arrow</small></small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={RightArrow}><small><small>Right Arrow</small></small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={DownArrow}><small><small>Down Arrow</small></small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Tab}><small><small>Tab</small></small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Delete}><small><small>Delete</small></small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Esc}><small><small>Esc</small></small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Enter}><small><small>Enter</small></small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={End}><small><small>End</small></small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Backspace}><small><small>Backspace</small></small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Insert}><small><small>Insert</small></small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={PageUp}><small><small>Page Up</small></small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={PageDown}><small><small>Page Down</small></small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Home}><small><small>Home</small></small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Number0}><small>0</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Number1}><small>1</small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Number2}><small>2</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Number3}><small>3</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Number4}><small>4</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Number5}><small>5</small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Number6}><small>6</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Number7}><small>7</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Number8}><small>8</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Number9}><small>9</small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={F1}><small>F1</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={F2}><small>F2</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={F3}><small>F3</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={F4}><small>F4</small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={F5}><small>F5</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={F6}><small>F6</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={F7}><small>F7</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={F8}><small>F8</small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={F9}><small>F9</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={F10}><small>F10</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={F11}><small>F11</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={F12}><small>F12</small></button>
                                                        </td>
                                                    </tr>
                                          
                                                    
                                        </table>

                                    </div>
                                    }
                                    </div>
                                    }
                                    <button className="MatchButtons" onClick={sendPartialHidden}><small>Send Partial Barcode</small></button>
                                    {showSendPartial && 
                                    <div id="sendPartialbarcode" className="sendpartialbarcode">
                                    <small>Starting Position:</small><TextField id="startingPosition" defaultValue="0" variant="outlined" small type="number" value={partialStart} inputProps={{min: 0}} onChange={(e) => partialBarcode(e.target.value)}></TextField>
                                    <small>Ending Position: </small><TextField id="endingPosition" defaultValue="0" variant="outlined" small type="number" value={partialEnd} inputProps={{min: 0}} onChange={(e) => partialbarcodeEnd(e.target.value)}></TextField><small><small>Set as 0 to send the rest of the barcode.</small></small>
                                    </div>
                                    }
                                    <button className="MatchButtons" onClick={insertCharHidden}><small>Insert Character in Barcode</small></button>
                                    {showInsertChar && 
                                    <div id="InsertChar">
                                    <TextField className="MatchButtons" id="InsertChartextfield" label="Insert Character in Barcode" variant="outlined" value={insertCharStringValue} onChange={(e) => insertChar(e.target.value)}/>
                                    
                                    <div class="divSmallButtons">
                                    <button className="smallButtons" onClick={nonPrintableChars2}><small><small>Non-Printable Chars</small></small></button>&nbsp;<button className="smallButtons" onClick={KeyboardChars2}><small><small>keyboard Chars</small></small></button>
                                    </div>
                                    {showInsertNonPrintableChars && 
                                    <div id="NonPrintable2" className="NonPrintable">
                                        <table>
                                        
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={NULL}><small>NULL</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={SOH}><small>SOH</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={STX}><small>STX</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={ETX}><small>ETX</small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={EOT}><small>EOT</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={ENQ}><small>ENQ</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={ACK}><small>ACK</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={BEL}><small>BEL</small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={BS}><small>BS</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={TAB}><small>TAB</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={LF}><small>LF/NL</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={VT}><small>VT</small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={FF}><small>FF/NP</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={CR}><small>CR</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={SO}><small>SO</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={SI}><small>SI</small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={DLE}><small>DLE</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={DC1}><small>DC1</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={DC2}><small>DC2</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={DC3}><small>DC3</small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={DC4}><small>DC4</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={NAK}><small>NAK</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={SYN}><small>SYN</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={ETB}><small>ETB</small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={CAN}><small>CAN</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={EM}><small>EM</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={SUB}><small>SUB</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={ESC}><small>ESC</small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={FS}><small>FS</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={GS}><small>GS</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={RS}><small>RS</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={US}><small>US</small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={DEL}><small>DEL</small></button>
                                                        </td>
                                                       
                                                    </tr>
                                                   
                                                    
                                        </table>

                                    </div>
                                    }
                                    {showInsertKeyboardChars && 
                                    <div id="Keyboardchars2" className="KeyboardChars">
                                        <table>
                                        
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={UpArrow}><small><small>Up arrow</small></small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={LeftArrow}><small><small>Left Arrow</small></small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={RightArrow}><small><small>Right Arrow</small></small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={DownArrow}><small><small>Down Arrow</small></small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Tab}><small><small>Tab</small></small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Delete}><small><small>Delete</small></small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Esc}><small><small>Esc</small></small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Enter}><small><small>Enter</small></small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={End}><small><small>End</small></small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Backspace}><small><small>Backspace</small></small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Insert}><small><small>Insert</small></small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={PageUp}><small><small>Page Up</small></small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={PageDown}><small><small>Page Down</small></small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Home}><small><small>Home</small></small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Number0}><small>0</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Number1}><small>1</small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Number2}><small>2</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Number3}><small>3</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Number4}><small>4</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Number5}><small>5</small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Number6}><small>6</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Number7}><small>7</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Number8}><small>8</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Number9}><small>9</small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={F1}><small>F1</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={F2}><small>F2</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={F3}><small>F3</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={F4}><small>F4</small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={F5}><small>F5</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={F6}><small>F6</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={F7}><small>F7</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={F8}><small>F8</small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={F9}><small>F9</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={F10}><small>F10</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={F11}><small>F11</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={F12}><small>F12</small></button>
                                                        </td>
                                                    </tr>
                                          
                                                    
                                        </table>

                                    </div>
                                    }
                                    <div className="afterpositionlabel">
                                    <small>After Position:</small><TextField id="AfterPositionchar" defaultValue="1" variant="outlined" small type="number" value={afterPositionValue} onChange={(e) => afterPositionFunction(e.target.value)}></TextField>
                                    </div>
                                    </div>
                                    }
                                    <button className="MatchButtons" onClick={insertSuffixHidden}><small>Insert Suffix</small></button>
                                    {showInsertSuffix && 
                                    <div id="insertsuffixhidden">
                                    <TextField className="MatchButtons" id="suffixTextField" label="Insert Suffix" variant="outlined" value={suffixValue} onChange={(e) => insertSufix(e.target.value)}/>
                                    <div class="divSmallButtons" >
                                    <button className="smallButtons" onClick={nonPrintableCharsSuffix}><small><small>Non-Printable Chars</small></small></button>&nbsp;<button className="smallButtons" onClick={KeyboardCharsSuffix}><small><small>keyboard Chars</small></small></button>
                                    </div>
                                    {showSuffixNonPrintableChars && 
                                    <div id="NonprintableSuffix" className="NonPrintable">
                                        <table>
                                        
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={NULL}><small>NULL</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={SOH}><small>SOH</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={STX}><small>STX</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={ETX}><small>ETX</small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={EOT}><small>EOT</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={ENQ}><small>ENQ</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={ACK}><small>ACK</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={BEL}><small>BEL</small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={BS}><small>BS</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={TAB}><small>TAB</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={LF}><small>LF/NL</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={VT}><small>VT</small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={FF}><small>FF/NP</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={CR}><small>CR</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={SO}><small>SO</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={SI}><small>SI</small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={DLE}><small>DLE</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={DC1}><small>DC1</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={DC2}><small>DC2</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={DC3}><small>DC3</small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={DC4}><small>DC4</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={NAK}><small>NAK</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={SYN}><small>SYN</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={ETB}><small>ETB</small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={CAN}><small>CAN</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={EM}><small>EM</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={SUB}><small>SUB</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={ESC}><small>ESC</small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={FS}><small>FS</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={GS}><small>GS</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={RS}><small>RS</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={US}><small>US</small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={DEL}><small>DEL</small></button>
                                                        </td>
                                                       
                                                    </tr>
                                                   
                                                    
                                        </table>

                                    </div>
                                    }
                                    {showSuffixKeyboardChars && 
                                    <div id="KeyboardCharsSuffix" className="KeyboardChars">
                                        <table>
                                        
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={UpArrow}><small><small>Up arrow</small></small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={LeftArrow}><small><small>Left Arrow</small></small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={RightArrow}><small><small>Right Arrow</small></small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={DownArrow}><small><small>Down Arrow</small></small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Tab}><small><small>Tab</small></small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Delete}><small><small>Delete</small></small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Esc}><small><small>Esc</small></small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Enter}><small><small>Enter</small></small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={End}><small><small>End</small></small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Backspace}><small><small>Backspace</small></small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Insert}><small><small>Insert</small></small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={PageUp}><small><small>Page Up</small></small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={PageDown}><small><small>Page Down</small></small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Home}><small><small>Home</small></small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Number0}><small>0</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Number1}><small>1</small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Number2}><small>2</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Number3}><small>3</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Number4}><small>4</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Number5}><small>5</small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Number6}><small>6</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Number7}><small>7</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Number8}><small>8</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Number9}><small>9</small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={F1}><small>F1</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={F2}><small>F2</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={F3}><small>F3</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={F4}><small>F4</small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={F5}><small>F5</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={F6}><small>F6</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={F7}><small>F7</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={F8}><small>F8</small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={F9}><small>F9</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={F10}><small>F10</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={F11}><small>F11</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={F12}><small>F12</small></button>
                                                        </td>
                                                    </tr>
                                          
                                                    
                                        </table>

                                    </div>
                                    }
                                    </div>
                                    }
                                    <br/><br/>
                                    
                                    </div>
                                    }
                                    {showButtonsOptions && 
                                    <div id="optionstodelete" className="deleteoptions">
                                        <small>Delete the following parameters: </small><br/><br/>
                                    <input type="checkbox"  value="formatOutput" onChange={FOOption} checked={option1}/> Format Output Options
                                    <br/><br/><br/>
                                    <input type="checkbox" value="insertAIM" onChange={IAIMOption} checked={option2}/> Insert AIM-ID
                                    <br/><br/><br/>
                                    <input type="checkbox" value="insertPrefix" onChange={IPOption} checked={option3}/> Insert Prefix
                                    <br/><br/><br/>
                                    <input type="checkbox" value="sendPartial" onChange={SPOption} checked={option4}/> Send Partial Barcode
                                    <br/><br/><br/>
                                    <input type="checkbox" value="insertChar" onChange={ICOption}  checked={option5}/> Insert Character in Barcode
                                    <br/><br/><br/>
                                    <input type="checkbox" value="insertSuffix" onChange={ISOption}  checked={option6}/> Insert Suffix
                                    <br/><br/><br/>

                                        <div class="cancelButtons">
                                        <Button variant="outlined" color="primary" onClick={CancelButton}>Cancel</Button>&nbsp;&nbsp; <Button variant="outlined" color="primary" onClick={deleteOptionsSelected}>Confirm</Button>
                                       
                                        </div>
                                        <br/>
                                    </div>
                                    }
                                    </div>
                                    <div class="imagebuttons">
                                    </div>
                                    <div class="databox">
                                    <small><p class="text">Chosen Format Output</p></small>
                                                <small>{FormatOutputString}</small>
                                                <small>{AIMIDString}</small>
                                                <small>{PrefixString}</small>
                                                <small>{partialBarcodeString}</small>
                                                <small>{insertCharString}</small>
                                                <small>{SufixString}</small>
                                    </div>
                                    <div class="databox">
                                    <small><p class="text">Formatted Sample Output</p></small>
                                            <small>{formatSample}</small>
                                            <small>{AIMIDSample}</small>
                                            <small>{PrefixSample}</small>
                                            <small>{partialBarcodeSample}</small>
                                            <small>{insertCharSample}</small>
                                            <small>{SufixSample}</small>
                                    </div>
                                    
                                    <div class="buttonsdownMatch">
                            <div><input type="image" src={deletepic} alt='delete' height="40px" width="40px" title="Remove all Data Formatting Output Operations." onClick={RemoveAllDataEntered}/></div>
                            <div><input type="image" src={cancelpic} alt='cancel' height="40px" width="40px" title="Remove Last Data Formatting Operation Entered." onClick={RemoveLastDataEntered}/></div>
                            
                            </div>

                            </div>
            </>
        );
    
}
 
export default DataFormatting;