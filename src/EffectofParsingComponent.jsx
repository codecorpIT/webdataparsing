import React, { Component } from 'react';
import { useState } from 'react';


var BarcodeContentsDefault = "CortexTools2";
//export function BarcodeContentsOnSample({setEffectOnParsing}) {
 //   setEffectOnParsing(BarcodeContentsDefault) ;
//}

export function EffectOfParsing({setEffectOnParsing}) {
    const [barcodeContents, setBarcodeContents] = useState("CortexTools2");
    const SetSampleButton = () =>{
        var barcodeContentsEnteredByUser = prompt("Sample Barcode Contents:");
        if (barcodeContentsEnteredByUser === null){
            return;
        }
        BarcodeContentsDefault = barcodeContentsEnteredByUser;
        setBarcodeContents(barcodeContentsEnteredByUser);
        setEffectOnParsing(barcodeContentsEnteredByUser);
    }
    
        return ( 
            <>
                    <small><p class="text">Effect of Parsing Action on Barcode</p></small>
                                                <div class="panel2">
                                                <br/><br/>
                                                        <div class="samplebutton">
                                                        <button onClick={SetSampleButton}>Sample</button>
                                                        </div>
                                                        <br/>
                                                        
                                                        
                                                        <div>Sample Barcode Type</div>
                                                        <div><b>Any</b></div>
                                                        <div>Barcode Before Parsing:</div>
                                                        <div><b>{barcodeContents}</b></div>
                                                        
                                                       
                                                        
                                                        
                                                        
                                                </div>   
            </>
        );
    
}
 
export default EffectOfParsing;