import React, { Component } from 'react';
import ConditionsToMatch from './ConditionsToMatchComponent';
import DataFormatting from './DataFormattingComponent';
import MatchSubstitute from './Match-substituteOpComponent';
//import DefinedRules from './DefinedRulesActionComponent';
import EffectOfParsing from './EffectofParsingComponent';
import CreateBarcode from './GetBarcodeRulesCreated';
import { useState } from 'react';
function SimpleParsing(){
   
const [effectOnParsing, setEffectOnParsing] = useState('CortexTools2');
const [effectOnAfterParsing, setEffectOnAfterParsing] = useState('CortexTools2');



        return ( 
                
                <div class="containerbox">   
                            <div class="leftpanel">     
                            <div>
                            <EffectOfParsing setEffectOnParsing={setEffectOnParsing}></EffectOfParsing>  
                             
                            </div>                      
                                                                                                                                 
                            </div>
                        <div class="newaction">
                                    <h5>New Action</h5>
                                    <ConditionsToMatch></ConditionsToMatch>
                                    <MatchSubstitute effectOnParsing={effectOnParsing} setEffectOnAfterParsing={setEffectOnAfterParsing}></MatchSubstitute>
                                    <DataFormatting effectOnParsing={effectOnAfterParsing}></DataFormatting> 
                                    <div className="createbarcode">    
                                         <CreateBarcode />              
                                </div>                                               
                        </div>
                        
                </div>
              
                
    )

}
         
 export default SimpleParsing;