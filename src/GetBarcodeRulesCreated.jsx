import React, {useState, useRef} from 'react';
import axios from 'axios';
import {Button} from '@material-ui/core';
import { useReactToPrint } from 'react-to-print';
import {SymbologiesSelectedByUser, DataLenghtEqualToSelectedByUser, DataLenghtNotEqualToSelectedByUser, 
    DLGreaterThanANDLessThanSelectedByUser,DataLenghtGreaterThanSelectedByUser, DLNotEqualANDGreaterThanSelectedByUser, 
    DataLenghtLessThanSelectedByUser, DLNotEqualANDLessThanSelectedByUser, DataLenghtGreaterThanEqualToSelectedByUser, 
    DLNotEqualANDGreaterThanEqualToSelectedByUser, DataLenghtLessThanEqualToSelectedByUser, 
    DLNotEqualANDLessThanEqualToSelectedByUser, DLNotEqualANDGreaterThanANDLessThanSelectedByUser, DLNotEqualANDGreaterThanEqualToANDLessThanEqualToSelectedByUser, DLGreaterThanEqualToANDLessThanEqualToSelectedByUser,
    BarcodeDataStartsWith, BarcodeDataContains, BarcodeDataEndsWith, BarcodeDataStartsANDEnds, BarcodeDataStartANDContains, 
    BarcodeDataContainsANDEnds, BarcodeDataStartsANDContainsANDEnds, AllOptionsSelectedConditions} from './ConditionsToMatchComponent';
import { DoNotTransmitBarcodeselected, SubstitutedatainbarcodeOp, SubstitutedatainbarcodeOp2, SubstitutedatainbarcodeOp3, SubstitutedatainbarcodeOp4, SubstitutedatainbarcodeOp5, AllOptionsSelectedMatchSubs} from './Match-substituteOpComponent';
import {FormatOutput, AIMIDOselected, PrefixInserted, SendPartialInserted, CharToInsertSelected, SuffixInserted, AllOptionsSelectedDataFormatting} from './DataFormattingComponent';
function App(){
    
    const [imageBarcode, setImageBarcode] = useState('');
    const [optionsSelected, setOptionsSelected] = useState('');
    const [optionsSelected2, setOptionsSelected2] = useState('');
    const [optionsSelected3, setOptionsSelected3] = useState('');
    const [showDownloadButton, setShowDownloadButton] = useState(false);

    const buttonClick = () => {
   let header = "\x01Y\x1d\x02";
   let split = "\x03";
   let footer = "\x03\x04";
   var finalCode = "";
   var finalCode2 = "";
   var finalCodeString = "";
   var dataSmValidationCommand = "CDOPSDV4"; //Command to use String Matching
   var stringMatchingCmd = "CDOPSSM"; //Send a String matching command for 82x readers.
   var dataDfValidationCommand = "CDOPSDF1"; //Command to use Data Formatting
   var formatDataCmd = "CDOPSFD"; //Send a Data Formatting command to 82x readers.
   var offCommand = "CDOPSAS0";
   var endMarker = "\\x" + "01@";
   var SYMB_Sum = 0;
   var SYMB_Ex_Sum = 0;
   var SymbologyBitsFinal = "";
   var SymbologyBitsFinal2 = "";
   var BarcodeLenghtFinal = "";
   var BarcodeDataFinal = "";
   var SubstituteDataHeader = "!,,";
   var BarcodeSubstitutionFinal = "";
   var PrefixFinal = "";
   var SendpartialBcodeFinal = "";
   var CharInsertedFinal = "";
   var SuffixFinal = "";
   var optionsSelectedByUser = AllOptionsSelectedConditions();
    var optionsSelectedByUser2 = AllOptionsSelectedMatchSubs();
    var optionsSelectedByUser3 = AllOptionsSelectedDataFormatting();
    setOptionsSelected(optionsSelectedByUser);
    setOptionsSelected2(optionsSelectedByUser2);
    setOptionsSelected3(optionsSelectedByUser3);
   function MetaCharReplaced(string) {
            if(string.includes("/")){
                string = string.replace(/\//g, "/2F");
            }
            if(string.includes("!")){
            string = string.replace(/\!/g, '/21');
            }
            if(string.includes("#")){
                string = string.replace(/\#/g, "/23");
            }
            if(string.includes("$")){
                string = string.replace(/\$/g, "/24");
            }
            if(string.includes("*")){
                string = string.replace(/\*/g, "/2A");
            }
            if(string.includes(",")){
                string = string.replace(/\,/g, "/2C");
            }
            if(string.includes("^")){
                string = string.replace(/\^/g, "/5E");
            }
            if(string.includes("|")){
                string = string.replace(/\|/g, "/7C");
            }
            if(string.includes("@")){
                string = string.replace(/@/g, "/40");
            }
            if(string.includes("`")){
                string = string.replace(/`/g, "/60");
            }
            if(string.includes("<NULL>")){
                string = string.replace(/<NULL>/g, "/00");
            }
            if(string.includes("<SOH>")){
                string = string.replace(/<SOH>/g, "/01");
             }
             if(string.includes("<STX>")){
                string = string.replace(/<STX>/g, "/02");
             }
             if(string.includes("<ETX>")){
                string = string.replace(/<ETX>/g, "/03");
             }
             if(string.includes("<EOT>")){
                string = string.replace(/<EOT>/g, "/04");
             }
             if(string.includes("<ENQ>")){
                string = string.replace(/<ENQ>/g, "/05");
             }
             if(string.includes("<ACK>")){
                string = string.replace(/<ACK>/g, "/06");
             }
             if(string.includes("<BEL>")){
                string = string.replace(/<BEL>/g, "/07");
             }
             if(string.includes("<BS>")){
                string = string.replace(/<BS>/g, "/08");
             }
             if(string.includes("<TAB>")){
                string = string.replace(/<TAB>/g, "/09");
             }
             if(string.includes("<LF-NL>")){
                string = string.replace(/<LF-NL>/g, "/0A");
             }
             if(string.includes("<VT>")){
                string = string.replace(/<VT>/g, "/0B");
             }
             if(string.includes("<FF-NP>")){
                string = string.replace(/<FF-NP>/g, "/0C");
             }
             if(string.includes("<CR>")){
                string = string.replace(/<CR>/g, "/0D");
             }
             if(string.includes("<SO>")){
                string = string.replace(/<SO>/g, "/0E");
             }
             if(string.includes("<SI>")){
                string = string.replace(/<SI>/g, "/0F");
             }
             if(string.includes("<DLE>")){
                string = string.replace(/<DLE>/g, "/10");
             }
             if(string.includes("<DC1>")){
                string = string.replace(/<DC1>/g, "/11");
             }
             if(string.includes("<DC2>")){
                string = string.replace(/<DC2>/g, "/12");
             }
             if(string.includes("<DC3>")){
                string = string.replace(/<DC3>/g, "/13");
             }
             if(string.includes("<DC4>")){
                string = string.replace(/<DC4>/g, "/14");
             }
             if(string.includes("<NAK>")){
                string = string.replace(/<NAK>/g, "/15");
             }
             if(string.includes("<SYN>")){
                string = string.replace(/<SYN>/g, "/16");
             }
             if(string.includes("<ETB>")){
                string = string.replace(/<ETB>/g, "/17");
             }
             if(string.includes("<CAN>")){
                string = string.replace(/<CAN>/g, "/18");
             }
             if(string.includes("<EM>")){
                string = string.replace(/<EM>/g, "/19");
             }
             if(string.includes("<SUB>")){
                string = string.replace(/<SUB>/g, "/1A");
             }
             if(string.includes("<ESC>")){
                string = string.replace(/<ESC>/g, "/1B");
             }
             if(string.includes("<FS>")){
                string = string.replace(/<FS>/g, "/1C");
             }
             if(string.includes("<GS>")){
                string = string.replace(/<GS>/g, "/1D");
             }
             if(string.includes("<RS>")){
                string = string.replace(/<RS>/g, "/1E");
             }
             if(string.includes("<US>")){
                string = string.replace(/<US>/g, "/1F");
             }
             if(string.includes("<DEL>")){
                string = string.replace(/<DEL>/g, "/7F");
             }           
            
             if(string.includes("<Up Arrow-Keyboard>")){
                string = string.replace(/<Up Arrow-Keyboard>/g, "/01Y/1Ean/2F/2Fu/04");
             } 
             if(string.includes("<Left Arrow-Keyboard>")){
                string = string.replace(/<Left Arrow-Keyboard>/g, "/01Y/1Ean/2F/2Fl/04");
             }   
             if(string.includes("<Right Arrow-Keyboard>")){
                string = string.replace(/<Right Arrow-Keyboard>/g, "/01Y/1Ean/2F/2Fr/04");
             }   
             if(string.includes("<Down Arrow-Keyboard>")){
                string = string.replace(/<Down Arrow-Keyboard>/g, "/01Y/1Ean/2F/2Fd/04");
             }   
             if(string.includes("<Tab-Keyboard>")){
                string = string.replace(/<Tab-Keyboard>/g, "/01Y/1Ean/2F/2Ft/04");
             }   
             if(string.includes("<Delete-Keyboard>")){
                string = string.replace(/<Delete-Keyboard>/g, "/01Y/1Ean/2F/2Fz/04");
             }   
             if(string.includes("<Esc-Keyboard>")){
                string = string.replace(/<Esc-Keyboard>/g, "/01Y/1Ean/2F/2Fe/04");
             }   
             if(string.includes("<Enter-Keyboard>")){
                string = string.replace(/<Enter-Keyboard>/g, "/01Y/1Ean/2F/2Fn/04");
             }   
             if(string.includes("<End-Keyboard>")){
                string = string.replace(/<End-Keyboard>/g, "/01Y/1Ean/2F/2Fv/04");
             }   
             if(string.includes("<Backspace-Keyboard>")){
                string = string.replace(/<Backspace-Keyboard>/g, "/01Y/1Ean/2F/2Fb/04");
             }   
             if(string.includes("<Insert-Keyboard>")){
                string = string.replace(/<Insert-Keyboard>/g, "/01Y/1Ean/2F/2Fi/04");
             }   
             if(string.includes("<Page Up-Keyboard>")){
                string = string.replace(/<Page Up-Keyboard>/g, "/01Y/1Ean/2F/2Fp/04");
             }   
             if(string.includes("<Page Down-Keyboard>")){
                string = string.replace(/<Page Down-Keyboard>/g, "/01Y/1Ean/2F/2Fx/04");
             }   
             if(string.includes("<Home-Keyboard>")){
                string = string.replace(/<Home-Keyboard>/g, "/01Y/1Ean/2F/2Fh/04");
             }    
             if(string.includes("<0>")){
                string = string.replace(/<0>/g, "/01Y/1Ean/2F/2F0/04");
             }   
             if(string.includes("<1>")){
                string = string.replace(/<1>/g, "/01Y/1Ean/2F/2F1/04");
             }   
             if(string.includes("<2>")){
                string = string.replace(/<2>/g, "/01Y/1Ean/2F/2F2/04");
             }   
             if(string.includes("<3>")){
                string = string.replace(/<3>/g, "/01Y/1Ean/2F/2F3/04");
             }   
             if(string.includes("<4>")){
                string = string.replace(/<4>/g, "/01Y/1Ean/2F/2F4/04");
             }   
             if(string.includes("<5>")){
                string = string.replace(/<5>/g, "/01Y/1Ean/2F/2F5/04");
             }   
             if(string.includes("<6>")){
                string = string.replace(/<6>/g, "/01Y/1Ean/2F/2F6/04");
             } 
             if(string.includes("<7>")){
                string = string.replace(/<7>/g, "/01Y/1Ean/2F/2F7/04");
             } 
             if(string.includes("<8>")){
                string = string.replace(/<8>/g, "/01Y/1Ean/2F/2F8/04");
             } 
             if(string.includes("<9>")){
                string = string.replace(/<9>/g, "/01Y/1Ean/2F/2F9/04");
             } 
             if(string.includes("<F1-Keyboard>")){
                string = string.replace(/<F1-Keyboard>/g, "/01Y/1Ean/2F/2Ff1/04");
             }   
             if(string.includes("<F2-Keyboard>")){
                string = string.replace(/<F2-Keyboard>/g, "/01Y/1Ean/2F/2Ff2/04");
             }   
             if(string.includes("<F3-Keyboard>")){
                string = string.replace(/<F3-Keyboard>/g, "/01Y/1Ean/2F/2Ff3/04");
             }   
             if(string.includes("<F4-Keyboard>")){
                string = string.replace(/<F4-Keyboard>/g, "/01Y/1Ean/2F/2Ff4/04");
             }   
             if(string.includes("<F5-Keyboard>")){
                string = string.replace(/<F5-Keyboard>/g, "/01Y/1Ean/2F/2Ff5/04");
             }   
             if(string.includes("<F6-Keyboard>")){
                string = string.replace(/<F6-Keyboard>/g, "/01Y/1Ean/2F/2Ff6/04");
             }   
             if(string.includes("<F7-Keyboard>")){
                string = string.replace(/<F7-Keyboard>/g, "/01Y/1Ean/2F/2Ff7/04");
             }   
             if(string.includes("<F8-Keyboard>")){
                string = string.replace(/<F8-Keyboard>/g, "/01Y/1Ean/2F/2Ff8/04");
             }   
             if(string.includes("<F9-Keyboard>")){
                string = string.replace(/<F9-Keyboard>/g, "/01Y/1Ean/2F/2Ff9/04");
             }   
             if(string.includes("<F10-Keyboard>")){
                string = string.replace(/<F10-Keyboard>/g, "/01Y/1Ean/2F/2Ff10/04");
             }   
             if(string.includes("<F11-Keyboard>")){
                string = string.replace(/<F11-Keyboard>/g, "/01Y/1Ean/2F/2Ff11/04");
             }   
             if(string.includes("<F12-Keyboard>")){
                string = string.replace(/<F12-Keyboard>/g, "/01Y/1Ean/2F/2Ff12/04");
             }   

             return string;
        }
        var SymbologiesReceive = SymbologiesSelectedByUser();   // receive an Array with all the symbologies selected by user
        var BarcodelenghtEqualTo = DataLenghtEqualToSelectedByUser(); //receive an individual string with the number that the user entered
        var BarcodelenghtNotEqualTo = DataLenghtNotEqualToSelectedByUser(); //receive an individual string with the number that the user entered
        var BarcodelenghtGreaterThan = DataLenghtGreaterThanSelectedByUser(); //receive an individual string with the number that the user entered
        var BarcodelenghtLessThan = DataLenghtLessThanSelectedByUser(); //receive an individual string with the number that the user entered
        var BarcodelenghtGreaterThanEqualTo = DataLenghtGreaterThanEqualToSelectedByUser(); //receive an individual string with the number that the user entered
        var BarcodelenghtLessThanEqualTo = DataLenghtLessThanEqualToSelectedByUser(); //receive an individual string with the number that the user entered
        var BLGreaterANDLessthan = DLGreaterThanANDLessThanSelectedByUser(); //receive an Array with all numbers that user entered
        var BLGreaterEqualANDLessEqual = DLGreaterThanEqualToANDLessThanEqualToSelectedByUser(); //receive an Array with all numbers that user entered
        var BLNotEqualANDGreaterThan = DLNotEqualANDGreaterThanSelectedByUser(); //receive an Array with all numbers that user entered
        var BLNotEqualANDLessThan = DLNotEqualANDLessThanSelectedByUser(); //receive an Array with all numbers that user entered
        var BLNotEqualANDGreaterthanEqualTo = DLNotEqualANDGreaterThanEqualToSelectedByUser(); //receive an Array with all numbers that user entered
        var BLNotEqualANDLessThanEqualTo = DLNotEqualANDLessThanEqualToSelectedByUser(); //receive an Array with all numbers that user entered
        var BLNotEqualANDGreaterThanANDLessThan = DLNotEqualANDGreaterThanANDLessThanSelectedByUser(); //receive an Array with all numbers that user entered
        var BLNotEqualANDGreaterEqualtoANDLessEqualTo = DLNotEqualANDGreaterThanEqualToANDLessThanEqualToSelectedByUser(); //receive an Array with all numbers that user entered
        var BarcodeDataStartsString = BarcodeDataStartsWith();
        var BarcodeDataContainsString = BarcodeDataContains();
        var BarcodeDataEndsString = BarcodeDataEndsWith();
        var BarcodeDataStartsANDEndsString = BarcodeDataStartsANDEnds();
        var BarcodeDataStartsANDContainsString = BarcodeDataStartANDContains();
        var BarcodeDataContainsANDEndsString = BarcodeDataContainsANDEnds();
        var BarcodeDataStartsANDContainsANDEndsString = BarcodeDataStartsANDContainsANDEnds();
        var donotTransmitBcode = DoNotTransmitBarcodeselected();
        var DataSubstitutioninBarcode = SubstitutedatainbarcodeOp();
        var DataSubstitutioninBarcode2 = SubstitutedatainbarcodeOp2();
        var DataSubstitutioninBarcode3 = SubstitutedatainbarcodeOp3();
        var DataSubstitutioninBarcode4 = SubstitutedatainbarcodeOp4();
        var DataSubstitutioninBarcode5 = SubstitutedatainbarcodeOp5();
        var FormatOutputOperation = FormatOutput();
        var AIMIDOperation = AIMIDOselected();
        var PrefixOperation = PrefixInserted();
        var SendPartialBarcodeOperation = SendPartialInserted();
        var CharToInsertOperation = CharToInsertSelected();
        var SuffixOperation = SuffixInserted();
        
        console.log(SymbologiesReceive);
        console.log("OR" + BarcodelenghtEqualTo);
        console.log("OR" + BarcodelenghtNotEqualTo);
        console.log("OR" + BarcodelenghtGreaterThan);
        console.log("OR" + BarcodelenghtLessThan);
        console.log("OR" + BarcodelenghtGreaterThanEqualTo);
        console.log("OR" + BarcodelenghtLessThanEqualTo);
       
        console.log("AND" + BLGreaterANDLessthan);
        console.log("AND" + BLGreaterEqualANDLessEqual);
        console.log("AND" + BLNotEqualANDGreaterThan);
        console.log("AND" + BLNotEqualANDLessThan);
        console.log("AND" + BLNotEqualANDGreaterthanEqualTo);
        console.log("AND" + BLNotEqualANDLessThanEqualTo);
        console.log("AND" + BLNotEqualANDGreaterThanANDLessThan);
        console.log("AND" + BLNotEqualANDGreaterEqualtoANDLessEqualTo);
        
        console.log("Starts: " + BarcodeDataStartsString);
        console.log("Contains: " + BarcodeDataContainsString);
        console.log("Ends: " + BarcodeDataEndsString);
        console.log("Starts AND Ends: " + BarcodeDataStartsANDEndsString);
        console.log("Starts AND contains: " + BarcodeDataStartsANDContainsString);
        console.log("Contains AND Ends: " + BarcodeDataContainsANDEndsString);
        console.log("Start AND Contains AND Ends: " + BarcodeDataStartsANDContainsANDEndsString);
        console.log("Do not transmit: " + donotTransmitBcode);
        console.log("Sustitute Data: " + DataSubstitutioninBarcode);
        console.log("Sustitute Data: " + DataSubstitutioninBarcode2);
        console.log("Sustitute Data: " + DataSubstitutioninBarcode3);
        console.log("Sustitute Data: " + DataSubstitutioninBarcode4);
        console.log("Sustitute Data: " + DataSubstitutioninBarcode5);
        console.log("Prefix: " + PrefixOperation);
        console.log("Char to insert: " + CharToInsertOperation);
        
        if(SymbologiesReceive.includes("Australian Post")){
                SYMB_Ex_Sum = SYMB_Ex_Sum + 32768;
                
            }
        if(SymbologiesReceive.includes("Aztec")){
            SYMB_Sum = SYMB_Sum + 8;
        }
        if(SymbologiesReceive.includes("BC412")){
            SYMB_Ex_Sum = SYMB_Ex_Sum + 33554432;
        }
        if(SymbologiesReceive.includes("Canada Post")){
            SYMB_Ex_Sum = SYMB_Ex_Sum + 16777216;
        }
        if(SymbologiesReceive.includes("Codabar")){
            SYMB_Sum = SYMB_Sum + 4096;
        }
        if(SymbologiesReceive.includes("Codablock F")){
            SYMB_Ex_Sum = SYMB_Ex_Sum + 2048;
        }
        if(SymbologiesReceive.includes("Code 11")){
            SYMB_Ex_Sum = SYMB_Ex_Sum + 1;
        }
        if(SymbologiesReceive.includes("Code 32")){
            SYMB_Ex_Sum = SYMB_Ex_Sum + 2;
        }
        if(SymbologiesReceive.includes("Code 39")){
            SYMB_Sum = SYMB_Sum + 1024;
        }
        if(SymbologiesReceive.includes("Code 49")){
            SYMB_Ex_Sum = SYMB_Ex_Sum + 512;
        }
        if(SymbologiesReceive.includes("Code 93")){
            SYMB_Sum = SYMB_Sum + 16384;
        }
        if(SymbologiesReceive.includes("Code 128")){
            SYMB_Sum = SYMB_Sum + 8192;
        }
        if(SymbologiesReceive.includes("Composite Code A")){
            SYMB_Sum = SYMB_Sum + 128;
        }
        if(SymbologiesReceive.includes("Composite Code B")){
            SYMB_Sum = SYMB_Sum + 256;
        }
        if(SymbologiesReceive.includes("Composite Code C")){
            SYMB_Sum = SYMB_Sum + 512;
        }
        if(SymbologiesReceive.includes("Data Matrix")){
            SYMB_Sum = SYMB_Sum + 2;
        }
        if(SymbologiesReceive.includes("Dutch Post (KIX)")){
            SYMB_Ex_Sum = SYMB_Ex_Sum + 65536;
        }
        if(SymbologiesReceive.includes("EAN-13")){
            SYMB_Sum = SYMB_Sum + 131072;
        }
        if(SymbologiesReceive.includes("EAN-8")){
            SYMB_Sum = SYMB_Sum + 262144;
        }
        if(SymbologiesReceive.includes("GoCode")){
            SYMB_Sum = SYMB_Sum + 1;
        }
        if(SymbologiesReceive.includes("Grid Matrix")){
            SYMB_Sum = SYMB_Sum + 134217728;
        }
        if(SymbologiesReceive.includes("GS1 Databar")){
            SYMB_Sum = SYMB_Sum + 524288;
        }
        if(SymbologiesReceive.includes("GS1 Databar Expanded")){
            SYMB_Sum = SYMB_Sum + 4194304;
        }
        if(SymbologiesReceive.includes("GS1 Databar Expanded Stacked")){
            SYMB_Sum = SYMB_Sum + 8388608;
        }
        if(SymbologiesReceive.includes("GS1 Databar Limited")){
            SYMB_Sum = SYMB_Sum + 2097152;
        }
        if(SymbologiesReceive.includes("GS1 Databar Stacked")){
            SYMB_Sum = SYMB_Sum + 1048576;
        }
        if(SymbologiesReceive.includes("HanXin")){
            SYMB_Sum = SYMB_Sum + 16777216;
        }
        if(SymbologiesReceive.includes("Hong Kong 2 of 5")){
            SYMB_Ex_Sum = SYMB_Ex_Sum + 2097152;
        }
        if(SymbologiesReceive.includes("Interleaved 2 of 5")){
            SYMB_Sum = SYMB_Sum + 2048;
        }
        if(SymbologiesReceive.includes("Japan Post")){
            SYMB_Ex_Sum = SYMB_Ex_Sum + 131072;
        }
        if(SymbologiesReceive.includes("korea Post")){
            SYMB_Ex_Sum = SYMB_Ex_Sum + 1048576;
        }
        if(SymbologiesReceive.includes("Matrix 2 of 5")){
            SYMB_Ex_Sum = SYMB_Ex_Sum + 128;
        }
        if(SymbologiesReceive.includes("Maxicode")){
            SYMB_Sum = SYMB_Sum + 16;
        }
        if(SymbologiesReceive.includes("Micro QR Code")){
            SYMB_Sum = SYMB_Sum + 33554432;
        }
        if(SymbologiesReceive.includes("MicroPDF417")){
            SYMB_Sum = SYMB_Sum + 64;
        }
        if(SymbologiesReceive.includes("MSI Plessey")){
            SYMB_Ex_Sum = SYMB_Ex_Sum + 8;
        }
        if(SymbologiesReceive.includes("NEC 2 of 5")){
            SYMB_Ex_Sum = SYMB_Ex_Sum + 4194304;
        }
        if(SymbologiesReceive.includes("PDF417")){
            SYMB_Sum = SYMB_Sum + 32;
        }
        if(SymbologiesReceive.includes("Pharma Code")){
            SYMB_Ex_Sum = SYMB_Ex_Sum + 64;
        }
        if(SymbologiesReceive.includes("QR Code")){
            SYMB_Sum = SYMB_Sum + 4;
        }
        if(SymbologiesReceive.includes("QR Code Model 1")){
            SYMB_Sum = SYMB_Sum + 67108864;
        }
        if(SymbologiesReceive.includes("Straight 2 of 5 (3 start/stop bars)")){
            SYMB_Ex_Sum = SYMB_Ex_Sum + 256;
        }
        if(SymbologiesReceive.includes("Straight IATA 2 of 5 (2 start/stop bars)")){
            SYMB_Ex_Sum = SYMB_Ex_Sum + 8388608;
        }
        if(SymbologiesReceive.includes("Telepen")){
            SYMB_Ex_Sum = SYMB_Ex_Sum + 16;
        }
        if(SymbologiesReceive.includes("TriOptic")){
            SYMB_Ex_Sum = SYMB_Ex_Sum + 32;
        }
        if(SymbologiesReceive.includes("UK Plessey")){
            SYMB_Ex_Sum = SYMB_Ex_Sum + 4;
        }
        if(SymbologiesReceive.includes("UK Royal Mail")){
            SYMB_Ex_Sum = SYMB_Ex_Sum + 262144;
        }
        if(SymbologiesReceive.includes("UPC-A")){
            SYMB_Sum = SYMB_Sum + 32768;
        }
        if(SymbologiesReceive.includes("UPC-E")){
            SYMB_Sum = SYMB_Sum + 65536;
        }
        if(SymbologiesReceive.includes("UPU ID")){
            SYMB_Ex_Sum = SYMB_Ex_Sum + 524288;
        }
        if(SymbologiesReceive.includes("USPS Intelligent Mail")){
            SYMB_Ex_Sum = SYMB_Ex_Sum + 16384;
        }
        if(SymbologiesReceive.includes("USPS Planet")){
            SYMB_Ex_Sum = SYMB_Ex_Sum + 8192;
        }
        if(SymbologiesReceive.includes("USPS Postnet")){
            SYMB_Ex_Sum = SYMB_Ex_Sum + 4096;
        }
        if(SymbologiesReceive[0] === undefined){
            console.log("No Symbologies Selected");
            SymbologyBitsFinal = "00" + "\\x" + "2B" + "\\x" + "5F" + "\\x" + "61" + "\\x" + "49" + "\\x" + "60" + "\\x" + "2B" + "\\x" + "5F" + "\\x" + "61" + "\\x" + "49" + "\\x" + "60";
            SymbologyBitsFinal2 = "00" + "\\x" + "2B" + "\\x" + "5F" + "\\x" + "61" + "\\x" + "49" + "\\x" + "60" + "\\x" + "2B" + "\\x" + "5F" + "\\x" + "61" + "\\x" + "49" + "\\x" + "60";
            console.log(SymbologyBitsFinal);
            console.log(SymbologyBitsFinal2);
        }else {

            function padLeadingZeros(num, size) {
                var s = num+"";
                while (s.length < size) s = "0" + s;
                return s;
            }
            var TotalSumSymbEx = padLeadingZeros(SYMB_Ex_Sum, 10);
            var TotalSumSymb = padLeadingZeros(SYMB_Sum, 10);
            console.log("Symb Ex Sum: " + TotalSumSymbEx);
            console.log("Symb Sum: " + TotalSumSymb);
            var splitNumber = TotalSumSymb.split("");
            var splitExNumber = TotalSumSymbEx.split("");
            console.log(TotalSumSymb);
            
            for(var i = 0; i < 10; i++){
            
                if( i === 0){
                    var splitPart1 = parseInt((splitNumber[i] + splitNumber[i+1])) + 1;
                    if(splitPart1 < 16){
                        splitPart1 = splitPart1.toString(16);
                        var formattedNumber1 = (("0" + splitPart1).slice(-2)).toUpperCase();
                        console.log(formattedNumber1);
                    }else{
                        formattedNumber1 = (splitPart1.toString(16)).toUpperCase();
                        console.log(splitPart1);
                    }
                    
                }
                if( i === 2){
                    var splitPart2 = parseInt((splitNumber[i] + splitNumber[i+1])) + 1;
                    if(splitPart2 < 16){
                        splitPart2 = splitPart2.toString(16);
                        var formattedNumber2 = (("0" + splitPart2).slice(-2)).toUpperCase();
                        console.log(formattedNumber2);
                    }else{
                        formattedNumber2 = (splitPart2.toString(16)).toUpperCase();
                        console.log(splitPart2);
                    }
                }
                if( i === 4){
                    var splitPart3 = parseInt((splitNumber[i] + splitNumber[i+1])) + 1;
                    if(splitPart3 < 16){
                        splitPart3 = splitPart3.toString(16);
                        var formattedNumber3 = (("0" + splitPart3).slice(-2)).toUpperCase();
                        console.log(formattedNumber3);
                    }else{
                        formattedNumber3 = (splitPart3.toString(16)).toUpperCase();
                        console.log(splitPart3);
                    }
                }
                if( i === 6){
                    var splitPart4 = parseInt((splitNumber[i] + splitNumber[i+1])) + 1;
                    if(splitPart4 < 16){
                        splitPart4 = splitPart4.toString(16);
                        var formattedNumber4 = (("0" + splitPart4).slice(-2)).toUpperCase();
                        console.log(formattedNumber4);
                    }else{
                        formattedNumber4 = (splitPart4.toString(16)).toUpperCase();
                        console.log(splitPart4);
                    }
                }
                if( i === 8){
                    var splitPart5 = parseInt((splitNumber[i] + splitNumber[i+1])) + 1;
                    if(splitPart5 < 16){
                        splitPart5 = splitPart5.toString(16);
                        var formattedNumber5 = (("0" + splitPart5).slice(-2)).toUpperCase();
                        console.log(formattedNumber5);
                    }else{
                        formattedNumber5 = (splitPart5.toString(16)).toUpperCase();
                        console.log(splitPart5);
                    }
                }
                
            }
            console.log(splitExNumber);
            for(var j = 0; j < 10; j++){
            
                if( j === 0){
                    var splitExPart1 = parseInt((splitExNumber[j] + splitExNumber[j+1])) + 1;
                    if(splitExPart1 < 16){
                        splitExPart1 = splitExPart1.toString(16);
                        var formattedExNumber1 = (("0" + splitExPart1).slice(-2)).toUpperCase();
                        console.log(formattedExNumber1);
                    }else{
                        formattedExNumber1 = (splitExPart1.toString(16)).toUpperCase();
                        console.log(splitExPart1);
                    }
                    
                }
                if( j === 2){
                    var splitExPart2 = parseInt((splitExNumber[j] + splitExNumber[j+1])) + 1;
                    if(splitExPart2 < 16){
                        splitExPart2 = splitExPart2.toString(16);
                        var formattedExNumber2 = (("0" + splitExPart2).slice(-2)).toUpperCase();
                        console.log(formattedExNumber2);
                    }else{
                        formattedExNumber2 = (splitExPart2.toString(16)).toUpperCase();
                        console.log(splitExPart2);
                    }
                }
                if( j === 4){
                    var splitExPart3 = parseInt((splitExNumber[j] + splitExNumber[j+1])) + 1;
                    if(splitExPart3 < 16){
                        splitExPart3 = splitExPart3.toString(16);
                        var formattedExNumber3 = (("0" + splitExPart3).slice(-2)).toUpperCase();
                        console.log(formattedExNumber3);
                    }else{
                        formattedExNumber3 = (splitExPart3.toString(16)).toUpperCase();
                        console.log(splitExPart3);
                    }
                }
                if( j === 6){
                    var splitExPart4 = parseInt((splitExNumber[j] + splitExNumber[j+1])) + 1;
                    if(splitExPart4 < 16){
                        splitExPart4 = splitExPart4.toString(16);
                        var formattedExNumber4 = (("0" + splitExPart4).slice(-2)).toUpperCase();
                        console.log(formattedExNumber4);
                    }else{
                        formattedExNumber4 = (splitExPart4.toString(16)).toUpperCase();
                        console.log(splitExPart4);
                    }
                }
                if( j === 8){
                    var splitExPart5 = parseInt((splitExNumber[j] + splitExNumber[j+1])) + 1;
                    if(splitExPart5 < 16){
                        splitExPart5 = splitExPart5.toString(16);
                        var formattedExNumber5 = (("0" + splitExPart5).slice(-2)).toUpperCase();
                        console.log(formattedExNumber5);
                    }else{
                        formattedExNumber5 = (splitExPart5.toString(16)).toUpperCase();
                        console.log(splitExPart5);
                    }
                }
                
            }
            var SymbologyBitsWithoutHexadecimal = "00" + "\\x" + formattedNumber1 + "\\x" + formattedNumber2 + "\\x" + formattedNumber3 + "\\x" + formattedNumber4 +
            "\\x" + formattedNumber5 + "\\x" + formattedExNumber1 + "\\x" + formattedExNumber2 + "\\x" + formattedExNumber3 + "\\x" + formattedExNumber4
            + "\\x" + formattedExNumber5;

            SymbologyBitsFinal = SymbologyBitsWithoutHexadecimal.replace(/\/\//g, "/");
            SymbologyBitsFinal2 = SymbologyBitsWithoutHexadecimal.replace(/\/\//g, "/");
            console.log(SymbologyBitsFinal);
            console.log(SymbologyBitsFinal2);
            
            
        }


        if(BarcodelenghtEqualTo !== ""){
            var equalToNumber = parseInt(BarcodelenghtEqualTo);
            var hexEqualTo = equalToNumber.toString(16);
            var equalToFormatted;
            if(equalToNumber < 16){
                 equalToFormatted = ("00" + hexEqualTo.toUpperCase()).slice(-3);

            }else{
                 equalToFormatted = ("0" + hexEqualTo.toUpperCase()).slice(-3);

            }
            console.log("hexadecimal: " + equalToFormatted);
            BarcodeLenghtFinal = "$" + equalToFormatted;
        }
        if(BarcodelenghtNotEqualTo !== ""){
            var notEqualToNumber = parseInt(BarcodelenghtNotEqualTo);
            var notEqualPart1 = notEqualToNumber - 1;
            var notEqualPart2 = notEqualToNumber + 1;
            var hexNotEqualTo1 =notEqualPart1.toString(16);
            var hexNotEqualTo2 = notEqualPart2.toString(16);
            var notEqualToFormatted1;
            var notEqualToFormatted2;
            if(notEqualPart1 < 16){
                 notEqualToFormatted1 ="001" +  ("00" + hexNotEqualTo1.toUpperCase()).slice(-3);
                
            }else{
                 notEqualToFormatted1 ="001" +  ("0" + hexNotEqualTo1.toUpperCase()).slice(-3);
            }
            if(notEqualPart2 < 16){
                notEqualToFormatted2 = ("00" + hexNotEqualTo2.toUpperCase()).slice(-3) + "FFF";
            }else {
                notEqualToFormatted2 = ("0" + hexNotEqualTo2.toUpperCase()).slice(-3) + "FFF";
            }
            
            console.log("hexadecimal: " + notEqualToFormatted1 + notEqualToFormatted2);
            BarcodeLenghtFinal = BarcodeLenghtFinal + "$" + notEqualToFormatted1 + "$" + notEqualToFormatted2;
        }
        if(BarcodelenghtGreaterThan !== ""){
            var greaterThanNumber = parseInt(BarcodelenghtGreaterThan) + 1;
            var hexGreaterThan = greaterThanNumber.toString(16);
            var greaterThanFormatted;
            if(greaterThanNumber < 16){
                greaterThanFormatted = ("00" + hexGreaterThan.toUpperCase()).slice(-3) + "FFF";
            }else{
                greaterThanFormatted = ("0" + hexGreaterThan.toUpperCase()).slice(-3) + "FFF";
            }
            console.log("hexadecimal: " + greaterThanFormatted);
            BarcodeLenghtFinal = BarcodeLenghtFinal + "$" + greaterThanFormatted;
        }
        if(BarcodelenghtLessThan !== ""){
            var lessThanNumber = parseInt(BarcodelenghtLessThan) - 1;
            var hexLessThan = lessThanNumber.toString(16);
            var lessThanFormatted;
            if(lessThanNumber < 16){
                 lessThanFormatted = "001" + ("00" + hexLessThan.toUpperCase()).slice(-3);
            }else{
                 lessThanFormatted = "001" + ("0" + hexLessThan.toUpperCase()).slice(-3);
            }
            console.log("hexadecimal: " + lessThanFormatted);
            BarcodeLenghtFinal = BarcodeLenghtFinal + "$" + lessThanFormatted;
        }
        if(BarcodelenghtGreaterThanEqualTo !== ""){
            var greaterThanEqualToNumber = parseInt(BarcodelenghtGreaterThanEqualTo);
            var hexGreaterThanEqualTo = greaterThanEqualToNumber.toString(16);
            var greaterThanEqualToFormatted;
            if(greaterThanEqualToNumber < 16){
                greaterThanEqualToFormatted = ("00" + hexGreaterThanEqualTo.toUpperCase()).slice(-3) + "FFF";
            }else{
                greaterThanEqualToFormatted = ("0" + hexGreaterThanEqualTo.toUpperCase()).slice(-3) + "FFF";
            }
            console.log("hexadecimal: " + greaterThanEqualToFormatted);
            BarcodeLenghtFinal = BarcodeLenghtFinal + "$" + greaterThanEqualToFormatted;
        }
        if(BarcodelenghtLessThanEqualTo !== ""){
            var lessThanEqualToNumber = parseInt(BarcodelenghtLessThanEqualTo);
            var hexLessThanEqualTo = lessThanEqualToNumber.toString(16);
            var lessThanEqualToFormatted;
            if(lessThanEqualToNumber < 16){
                lessThanEqualToFormatted = "001" + ("00" + hexLessThanEqualTo.toUpperCase()).slice(-3);
            }else{
                lessThanEqualToFormatted = "001" + ("0" + hexLessThanEqualTo.toUpperCase()).slice(-3);
            }
            console.log("hexadecimal: " + lessThanEqualToFormatted);
            BarcodeLenghtFinal = BarcodeLenghtFinal + "$" + lessThanEqualToFormatted;
        }
            
            if(BLGreaterANDLessthan[0] !== undefined){
                var greaterThan_AND = BLGreaterANDLessthan[0] + 1;
                var lessThan_AND = BLGreaterANDLessthan[1] - 1;
                var hex_greaterThan_AND = greaterThan_AND.toString(16);
                var hex_lessThan_AND = lessThan_AND.toString(16);
                var greaterThan_ANDformatted;
                var lessThan_ANDFormatted;
                if(greaterThan_AND < 16){
                    greaterThan_ANDformatted = ("00" + hex_greaterThan_AND.toUpperCase()).slice(-3);
                }else{
                    greaterThan_ANDformatted = ("0" + hex_greaterThan_AND.toUpperCase()).slice(-3);
                }
                if(lessThan_AND < 16){
                    lessThan_ANDFormatted = ("00" + hex_lessThan_AND.toUpperCase()).slice(-3);
                }else{
                    lessThan_ANDFormatted = ("0" + hex_lessThan_AND.toUpperCase()).slice(-3)
                }
                var greaterThanANDLessThanFormatted = greaterThan_ANDformatted + lessThan_ANDFormatted;
                console.log(greaterThanANDLessThanFormatted);
                BarcodeLenghtFinal = BarcodeLenghtFinal + "$" + greaterThanANDLessThanFormatted;
            }
            if(BLGreaterEqualANDLessEqual[0] !== undefined){
                var greaterThanEqual_AND = BLGreaterEqualANDLessEqual[0];
                var lessThanEqual_AND = BLGreaterEqualANDLessEqual[1];
                var hex_greaterThanEqual_AND = greaterThanEqual_AND.toString(16);
                var hex_lessThanEqual_AND = lessThanEqual_AND.toString(16);
                var greaterThanEqual_ANDformatted;
                var lessThanEqual_ANDFormatted;
                if(greaterThanEqual_AND < 16){
                    greaterThanEqual_ANDformatted = ("00" + hex_greaterThanEqual_AND.toUpperCase()).slice(-3);
                }else{
                    greaterThanEqual_ANDformatted = ("0" + hex_greaterThanEqual_AND.toUpperCase()).slice(-3);
                }
                if(lessThanEqual_AND < 16){
                    lessThanEqual_ANDFormatted = ("00" + hex_lessThanEqual_AND.toUpperCase()).slice(-3);
                }else{
                    lessThanEqual_ANDFormatted = ("0" + hex_lessThanEqual_AND.toUpperCase()).slice(-3)
                }
                var greaterThanEqualANDLessThanEqualFormatted = greaterThanEqual_ANDformatted + lessThanEqual_ANDFormatted;
                console.log(greaterThanEqualANDLessThanEqualFormatted);
                BarcodeLenghtFinal = BarcodeLenghtFinal + "$" + greaterThanEqualANDLessThanEqualFormatted;
            }
            if(BLNotEqualANDGreaterThan[0] !== undefined){
                var notEqual_AND = BLNotEqualANDGreaterThan[0];
                var greaterThan_AND2 = BLNotEqualANDGreaterThan[1] + 1;
                var hex_greaterThan_AND2 = greaterThan_AND2.toString(16);
                var greaterThan_AND2Formatted;
                var notEqualANDGreaterThanFormatted;
                if(notEqual_AND > greaterThan_AND2){
                    var notEqual_ANDPart1 = BLNotEqualANDGreaterThan[0] - 1;
                    var notEqual_ANDPart2 = BLNotEqualANDGreaterThan[0] + 1;
                    var hex_notEqual_ANDPart1 = notEqual_ANDPart1.toString(16);
                    var hex_notEqual_ANDpart2 = notEqual_ANDPart2.toString(16);
                   var notEqual_ANDFormattedPart1;
                   var notEqual_ANDFormattedPart2;
                    if(notEqual_ANDPart1 < 16){
                        notEqual_ANDFormattedPart1 = ("00" + hex_notEqual_ANDPart1.toUpperCase()).slice(-3);
                    }else{
                        notEqual_ANDFormattedPart1 = ("0" + hex_notEqual_ANDPart1.toUpperCase()).slice(-3);
                    }
                    if(notEqual_ANDPart2 < 16){
                        notEqual_ANDFormattedPart2 = ("00" + hex_notEqual_ANDpart2.toUpperCase()).slice(-3);
                    }else{
                        notEqual_ANDFormattedPart2 = ("0" + hex_notEqual_ANDpart2.toUpperCase()).slice(-3);
                    }
                    if(greaterThan_AND2 < 16){
                        greaterThan_AND2Formatted = ("00" + hex_greaterThan_AND2.toUpperCase()).slice(-3);
                    }else{
                        greaterThan_AND2Formatted = ("0" + hex_greaterThan_AND2.toUpperCase()).slice(-3);
                    }
                    notEqualANDGreaterThanFormatted = greaterThan_AND2Formatted + notEqual_ANDFormattedPart1 + "$" + notEqual_ANDFormattedPart2 + "FFF";
                }else{
                    if(greaterThan_AND2 < 16){
                        greaterThan_AND2Formatted = ("00" + hex_greaterThan_AND2.toUpperCase()).slice(-3);
                    }else{
                        greaterThan_AND2Formatted = ("0" + hex_greaterThan_AND2.toUpperCase()).slice(-3);
                    }
                    notEqualANDGreaterThanFormatted = greaterThan_AND2Formatted + "FFF";

                }
                console.log(notEqualANDGreaterThanFormatted);
                BarcodeLenghtFinal = BarcodeLenghtFinal + "$" + notEqualANDGreaterThanFormatted;             
            }
            if(BLNotEqualANDLessThan[0] !== undefined){
                var notEqual2_AND = BLNotEqualANDLessThan[0];
                var lessThan_AND2 = BLNotEqualANDLessThan[1] - 1;
                var hex_lessThan_AND2 = lessThan_AND2.toString(16);
                var lessThan_AND2Formatted;
                var notEqualANDLessThanFormatted;
                if(notEqual2_AND < lessThan_AND2){
                    var notEqual2_ANDPart1 = BLNotEqualANDLessThan[0] - 1;
                    var notEqual2_ANDPart2 = BLNotEqualANDLessThan[0] + 1;
                    var hex_notEqual2_ANDPart1 = notEqual2_ANDPart1.toString(16);
                    var hex_notEqual2_ANDpart2 = notEqual2_ANDPart2.toString(16);
                   var notEqual2_ANDFormattedPart1;
                   var notEqual2_ANDFormattedPart2;
                    if(notEqual2_ANDPart1 < 16){
                        notEqual2_ANDFormattedPart1 = ("00" + hex_notEqual2_ANDPart1.toUpperCase()).slice(-3);
                    }else{
                        notEqual2_ANDFormattedPart1 = ("0" + hex_notEqual2_ANDPart1.toUpperCase()).slice(-3);
                    }
                    if(notEqual2_ANDPart2 < 16){
                        notEqual2_ANDFormattedPart2 = ("00" + hex_notEqual2_ANDpart2.toUpperCase()).slice(-3);
                    }else{
                        notEqual2_ANDFormattedPart2 = ("0" + hex_notEqual2_ANDpart2.toUpperCase()).slice(-3);
                    }
                    if(lessThan_AND2 < 16){
                        lessThan_AND2Formatted = ("00" + hex_lessThan_AND2.toUpperCase()).slice(-3);
                    }else{
                        lessThan_AND2Formatted = ("0" + hex_lessThan_AND2.toUpperCase()).slice(-3);
                    }
                    notEqualANDLessThanFormatted = "001" + notEqual2_ANDFormattedPart1 + "$" + notEqual2_ANDFormattedPart2 + lessThan_AND2Formatted;
                }else{
                    if(lessThan_AND2 < 16){
                        lessThan_AND2Formatted = ("00" + hex_lessThan_AND2.toUpperCase()).slice(-3);
                    }else{
                        lessThan_AND2Formatted = ("0" + hex_lessThan_AND2.toUpperCase()).slice(-3);
                    }
                    notEqualANDLessThanFormatted = "001" + lessThan_AND2Formatted ;

                }
                console.log(notEqualANDLessThanFormatted);
                BarcodeLenghtFinal = BarcodeLenghtFinal + "$" + notEqualANDLessThanFormatted;             
            }
            if(BLNotEqualANDGreaterthanEqualTo[0] !== undefined){
                var notEqual3_AND = BLNotEqualANDGreaterthanEqualTo[0];
                var greaterThanEqual_AND2 = BLNotEqualANDGreaterthanEqualTo[1];
                var hex_greaterThanEqual_AND2 = greaterThanEqual_AND2.toString(16);
                var greaterThanEqual_AND2Formatted;
                var notEqualANDGreaterThanEqualFormatted;
                if(notEqual3_AND > greaterThanEqual_AND2){
                    var notEqual3_ANDPart1 = BLNotEqualANDGreaterthanEqualTo[0] - 1;
                    var notEqual3_ANDPart2 = BLNotEqualANDGreaterthanEqualTo[0] + 1;
                    var hex_notEqual3_ANDPart1 = notEqual3_ANDPart1.toString(16);
                    var hex_notEqual3_ANDpart2 = notEqual3_ANDPart2.toString(16);
                   var notEqual3_ANDFormattedPart1;
                   var notEqual3_ANDFormattedPart2;
                    if(notEqual3_ANDPart1 < 16){
                        notEqual3_ANDFormattedPart1 = ("00" + hex_notEqual3_ANDPart1.toUpperCase()).slice(-3);
                    }else{
                        notEqual3_ANDFormattedPart1 = ("0" + hex_notEqual3_ANDPart1.toUpperCase()).slice(-3);
                    }
                    if(notEqual3_ANDPart2 < 16){
                        notEqual3_ANDFormattedPart2 = ("00" + hex_notEqual3_ANDpart2.toUpperCase()).slice(-3);
                    }else{
                        notEqual3_ANDFormattedPart2 = ("0" + hex_notEqual3_ANDpart2.toUpperCase()).slice(-3);
                    }
                    if(greaterThanEqual_AND2 < 16){
                        greaterThanEqual_AND2Formatted = ("00" + hex_greaterThanEqual_AND2.toUpperCase()).slice(-3);
                    }else{
                        greaterThanEqual_AND2Formatted = ("0" + hex_greaterThanEqual_AND2.toUpperCase()).slice(-3);
                    }
                    notEqualANDGreaterThanEqualFormatted = greaterThanEqual_AND2Formatted + notEqual3_ANDFormattedPart1 + "$" + notEqual3_ANDFormattedPart2 + "FFF";
                }else{
                    if(greaterThanEqual_AND2 < 16){
                        greaterThanEqual_AND2Formatted = ("00" + hex_greaterThanEqual_AND2.toUpperCase()).slice(-3);
                    }else{
                        greaterThanEqual_AND2Formatted = ("0" + hex_greaterThanEqual_AND2.toUpperCase()).slice(-3);
                    }
                    notEqualANDGreaterThanEqualFormatted = greaterThanEqual_AND2Formatted + "FFF";

                }
                console.log(notEqualANDGreaterThanEqualFormatted);
                BarcodeLenghtFinal = BarcodeLenghtFinal + "$" + notEqualANDGreaterThanEqualFormatted;             
            }
            if(BLNotEqualANDLessThanEqualTo[0] !== undefined){
                var notEqual4_AND = BLNotEqualANDLessThanEqualTo[0];
                var lessThanEqualTo_AND = BLNotEqualANDLessThanEqualTo[1];
                var hex_lessThanEqualto_AND2 = lessThanEqualTo_AND.toString(16);
                var lessThanEqualTo_AND2Formatted;
                var notEqualANDLessThanEqualToFormatted;
                if(notEqual4_AND < lessThanEqualTo_AND){
                    var notEqual4_ANDPart1 = BLNotEqualANDLessThanEqualTo[0] - 1;
                    var notEqual4_ANDPart2 = BLNotEqualANDLessThanEqualTo[0] + 1;
                    var hex_notEqual4_ANDPart1 = notEqual4_ANDPart1.toString(16);
                    var hex_notEqual4_ANDpart2 = notEqual4_ANDPart2.toString(16);
                   var notEqual4_ANDFormattedPart1;
                   var notEqual4_ANDFormattedPart2;
                    if(notEqual4_ANDPart1 < 16){
                        notEqual4_ANDFormattedPart1 = ("00" + hex_notEqual4_ANDPart1.toUpperCase()).slice(-3);
                    }else{
                        notEqual4_ANDFormattedPart1 = ("0" + hex_notEqual4_ANDPart1.toUpperCase()).slice(-3);
                    }
                    if(notEqual4_ANDPart2 < 16){
                        notEqual4_ANDFormattedPart2 = ("00" + hex_notEqual4_ANDpart2.toUpperCase()).slice(-3);
                    }else{
                        notEqual4_ANDFormattedPart2 = ("0" + hex_notEqual4_ANDpart2.toUpperCase()).slice(-3);
                    }
                    if(lessThanEqualTo_AND < 16){
                        lessThanEqualTo_AND2Formatted = ("00" + hex_lessThanEqualto_AND2.toUpperCase()).slice(-3);
                    }else{
                        lessThanEqualTo_AND2Formatted = ("0" + hex_lessThanEqualto_AND2.toUpperCase()).slice(-3);
                    }
                    notEqualANDLessThanEqualToFormatted = "001" + notEqual4_ANDFormattedPart1 + "$" + notEqual4_ANDFormattedPart2 + lessThanEqualTo_AND2Formatted;
                }else{
                    if(lessThanEqualTo_AND < 16){
                        lessThanEqualTo_AND2Formatted = ("00" + hex_lessThanEqualto_AND2.toUpperCase()).slice(-3);
                    }else{
                        lessThanEqualTo_AND2Formatted = ("0" + hex_lessThanEqualto_AND2.toUpperCase()).slice(-3);
                    }
                    notEqualANDLessThanEqualToFormatted = "001" + lessThanEqualTo_AND2Formatted ;

                }
                console.log(notEqualANDLessThanEqualToFormatted);
                BarcodeLenghtFinal = BarcodeLenghtFinal + "$" + notEqualANDLessThanEqualToFormatted;             
            }
            if(BLNotEqualANDGreaterThanANDLessThan[0] !== undefined){
                var notEqual5_AND = BLNotEqualANDGreaterThanANDLessThan[0];
                var greaterThan2_AND2 = BLNotEqualANDGreaterThanANDLessThan[1] + 1;
                var lessThan2_AND2 = BLNotEqualANDGreaterThanANDLessThan[2] - 1;
                var hex_greaterThan2_AND2 = greaterThan2_AND2.toString(16);
                var hex_lessThan2_AND2 = lessThan2_AND2.toString(16);
                var greaterThan2_AND2Formatted;
                var lessThan2_AND2Formatted;
                var notEqualANDGreaterThanANDLessThanFormatted;
                if(notEqual5_AND > greaterThan2_AND2 && notEqual5_AND < lessThan2_AND2){
                    var notEqual5_ANDPart1 = BLNotEqualANDGreaterThanANDLessThan[0] - 1;
                    var notEqual5_ANDPart2 = BLNotEqualANDGreaterThanANDLessThan[0] + 1;
                    var hex_notEqual5_ANDPart1 = notEqual5_ANDPart1.toString(16);
                    var hex_notEqual5_ANDpart2 = notEqual5_ANDPart2.toString(16);
                   var notEqual5_ANDFormattedPart1;
                   var notEqual5_ANDFormattedPart2;
                    if(notEqual5_ANDPart1 < 16){
                        notEqual5_ANDFormattedPart1 = ("00" + hex_notEqual5_ANDPart1.toUpperCase()).slice(-3);
                    }else{
                        notEqual5_ANDFormattedPart1 = ("0" + hex_notEqual5_ANDPart1.toUpperCase()).slice(-3);
                    }
                    if(notEqual5_ANDPart2 < 16){
                        notEqual5_ANDFormattedPart2 = ("00" + hex_notEqual5_ANDpart2.toUpperCase()).slice(-3);
                    }else{
                        notEqual5_ANDFormattedPart2 = ("0" + hex_notEqual5_ANDpart2.toUpperCase()).slice(-3);
                    }
                    if(greaterThan2_AND2 < 16){
                        greaterThan2_AND2Formatted = ("00" + hex_greaterThan2_AND2.toUpperCase()).slice(-3);
                    }else{
                        greaterThan2_AND2Formatted = ("0" + hex_greaterThan2_AND2.toUpperCase()).slice(-3);
                    }
                    if(lessThan2_AND2 < 16){
                        lessThan2_AND2Formatted = ("00" + hex_lessThan2_AND2.toUpperCase()).slice(-3);
                    }else{
                        lessThan2_AND2Formatted = ("0" + hex_lessThan2_AND2.toUpperCase()).slice(-3);
                    }
                    notEqualANDGreaterThanANDLessThanFormatted = greaterThan2_AND2Formatted + notEqual5_ANDFormattedPart1 + "$" + notEqual5_ANDFormattedPart2 + lessThan2_AND2Formatted;
                }else{
                    if(greaterThan2_AND2 < 16){
                        greaterThan2_AND2Formatted = ("00" + hex_greaterThan2_AND2.toUpperCase()).slice(-3);
                    }else{
                        greaterThan2_AND2Formatted = ("0" + hex_greaterThan2_AND2.toUpperCase()).slice(-3);
                    }
                    if(lessThan2_AND2 < 16){
                        lessThan2_AND2Formatted = ("00" + hex_lessThan2_AND2.toUpperCase()).slice(-3);
                    }else{
                        lessThan2_AND2Formatted = ("0" + hex_lessThan2_AND2.toUpperCase()).slice(-3);
                    }
                    notEqualANDGreaterThanANDLessThanFormatted = greaterThan2_AND2Formatted + lessThan2_AND2Formatted;

                }
                console.log(notEqualANDGreaterThanANDLessThanFormatted);
                BarcodeLenghtFinal = BarcodeLenghtFinal + "$" + notEqualANDGreaterThanANDLessThanFormatted;             
            }
            if(BLNotEqualANDGreaterEqualtoANDLessEqualTo[0] !== undefined){
                var notEqual6_AND = BLNotEqualANDGreaterEqualtoANDLessEqualTo[0];
                var greaterThanEqual2_AND2 = BLNotEqualANDGreaterEqualtoANDLessEqualTo[1];
                var lessThanEqual2_AND2 = BLNotEqualANDGreaterEqualtoANDLessEqualTo[2];
                var hex_greaterThanEqual2_AND2 = greaterThanEqual2_AND2.toString(16);
                var hex_lessThanEqual2_AND2 = lessThanEqual2_AND2.toString(16);
                var greaterThanEqual2_AND2Formatted;
                var lessThan2Equal_AND2Formatted;
                var notEqualANDGreaterThanEqualANDLessThanEqualFormatted;
                if(notEqual6_AND > greaterThanEqual2_AND2 && notEqual6_AND < lessThanEqual2_AND2){
                    var notEqual6_ANDPart1 = BLNotEqualANDGreaterEqualtoANDLessEqualTo[0] - 1;
                    var notEqual6_ANDPart2 = BLNotEqualANDGreaterEqualtoANDLessEqualTo[0] + 1;
                    var hex_notEqual6_ANDPart1 = notEqual6_ANDPart1.toString(16);
                    var hex_notEqual6_ANDpart2 = notEqual6_ANDPart2.toString(16);
                   var notEqual6_ANDFormattedPart1;
                   var notEqual6_ANDFormattedPart2;
                    if(notEqual6_ANDPart1 < 16){
                        notEqual6_ANDFormattedPart1 = ("00" + hex_notEqual6_ANDPart1.toUpperCase()).slice(-3);
                    }else{
                        notEqual6_ANDFormattedPart1 = ("0" + hex_notEqual6_ANDPart1.toUpperCase()).slice(-3);
                    }
                    if(notEqual6_ANDPart2 < 16){
                        notEqual6_ANDFormattedPart2 = ("00" + hex_notEqual6_ANDpart2.toUpperCase()).slice(-3);
                    }else{
                        notEqual6_ANDFormattedPart2 = ("0" + hex_notEqual6_ANDpart2.toUpperCase()).slice(-3);
                    }
                    if(greaterThanEqual2_AND2 < 16){
                        greaterThanEqual2_AND2Formatted = ("00" + hex_greaterThanEqual2_AND2.toUpperCase()).slice(-3);
                    }else{
                        greaterThanEqual2_AND2Formatted = ("0" + hex_greaterThanEqual2_AND2.toUpperCase()).slice(-3);
                    }
                    if(lessThanEqual2_AND2 < 16){
                        lessThan2Equal_AND2Formatted = ("00" + hex_lessThanEqual2_AND2.toUpperCase()).slice(-3);
                    }else{
                        lessThan2Equal_AND2Formatted = ("0" + hex_lessThanEqual2_AND2.toUpperCase()).slice(-3);
                    }
                    notEqualANDGreaterThanEqualANDLessThanEqualFormatted = greaterThanEqual2_AND2Formatted + notEqual6_ANDFormattedPart1 + "$" + notEqual6_ANDFormattedPart2 + lessThan2Equal_AND2Formatted;
                }else{
                    if(greaterThanEqual2_AND2 < 16){
                        greaterThanEqual2_AND2Formatted = ("00" + hex_greaterThanEqual2_AND2.toUpperCase()).slice(-3);
                    }else{
                        greaterThanEqual2_AND2Formatted = ("0" + hex_greaterThanEqual2_AND2.toUpperCase()).slice(-3);
                    }
                    if(lessThanEqual2_AND2 < 16){
                        lessThan2Equal_AND2Formatted = ("00" + hex_lessThanEqual2_AND2.toUpperCase()).slice(-3);
                    }else{
                        lessThan2Equal_AND2Formatted = ("0" + hex_lessThanEqual2_AND2.toUpperCase()).slice(-3);
                    }
                    notEqualANDGreaterThanEqualANDLessThanEqualFormatted = greaterThanEqual2_AND2Formatted + lessThan2Equal_AND2Formatted;

                }
                console.log(notEqualANDGreaterThanEqualANDLessThanEqualFormatted);
                BarcodeLenghtFinal = BarcodeLenghtFinal + "$" + notEqualANDGreaterThanEqualANDLessThanEqualFormatted;             
            }
            if(BarcodeDataStartsString !== ""){
                BarcodeDataFinal = BarcodeDataFinal + "|" + BarcodeDataStartsString + "*";
                console.log(BarcodeDataFinal);
            }
            if(BarcodeDataContainsString !== ""){
                BarcodeDataFinal = BarcodeDataFinal + "|" + "*" + BarcodeDataContainsString + "*";
                console.log(BarcodeDataFinal);
            }
            if(BarcodeDataEndsString !== ""){
                BarcodeDataFinal = BarcodeDataFinal + "|" + "*" + BarcodeDataEndsString ;
                console.log(BarcodeDataFinal);
            }
            if(BarcodeDataStartsANDEndsString[0] !== undefined){
                BarcodeDataFinal = BarcodeDataFinal + "|" + BarcodeDataStartsANDEndsString[0] + "*" + BarcodeDataStartsANDEndsString[1];
                console.log(BarcodeDataFinal);
            }
            if(BarcodeDataStartsANDContainsString[0] !== undefined){
                BarcodeDataFinal = BarcodeDataFinal + "|" + BarcodeDataStartsANDContainsString[0] + "*" + BarcodeDataStartsANDContainsString[1] + "*";
                console.log(BarcodeDataFinal);
            }
            if(BarcodeDataContainsANDEndsString[0] !== undefined){
                BarcodeDataFinal = BarcodeDataFinal + "|" + "*" + BarcodeDataContainsANDEndsString[0] + "*" + BarcodeDataContainsANDEndsString[1] ;
                console.log(BarcodeDataFinal);
            }
            if(BarcodeDataStartsANDContainsANDEndsString[0] !== undefined){
                BarcodeDataFinal = BarcodeDataFinal + "|" + BarcodeDataStartsANDContainsANDEndsString[0] + "*" + BarcodeDataStartsANDContainsANDEndsString[1] + "*" + BarcodeDataStartsANDContainsANDEndsString[2];
                console.log(BarcodeDataFinal);
            }
        if(DataSubstitutioninBarcode[0] !== undefined){
            
            var match = DataSubstitutioninBarcode[0];
            var replace = DataSubstitutioninBarcode[1];
            BarcodeSubstitutionFinal = "|" + match + "^1" + replace;
            if(DataSubstitutioninBarcode2[0] !== undefined){
                var match2 = DataSubstitutioninBarcode2[0];
                var replace2 = DataSubstitutioninBarcode2[1];
                BarcodeSubstitutionFinal = BarcodeSubstitutionFinal + "|" + match2 + "^1" + replace2;
            if(DataSubstitutioninBarcode3[0] !== undefined){
                var match3 = DataSubstitutioninBarcode3[0];
                var replace3 = DataSubstitutioninBarcode3[1];
                BarcodeSubstitutionFinal = BarcodeSubstitutionFinal + "|" + match3 + "^1" + replace3;
            if(DataSubstitutioninBarcode4[0] !== undefined){
                var match4 = DataSubstitutioninBarcode4[0];
                var replace4 = DataSubstitutioninBarcode4[1];
                BarcodeSubstitutionFinal = BarcodeSubstitutionFinal + "|" + match4 + "^1" + replace4;
            if(DataSubstitutioninBarcode5[0] !== undefined){
                var match5 = DataSubstitutioninBarcode5[0];
                var replace5 = DataSubstitutioninBarcode5[1];
                BarcodeSubstitutionFinal = BarcodeSubstitutionFinal + "|" + match5 + "^1" + replace5;
            }
            }
            }
            }
            console.log(BarcodeSubstitutionFinal);
        }
        if(donotTransmitBcode === true){
            BarcodeSubstitutionFinal = BarcodeSubstitutionFinal + "|*^2";
        }
        console.log(BarcodeSubstitutionFinal);
        if(FormatOutputOperation === ""){
            console.log("No Format Output Selected");
        }
        if(FormatOutputOperation === "hexadecimal"){
            console.log("hexadecimal");
            SymbologyBitsFinal2 = SymbologyBitsFinal2.replace("00", "01");
            console.log(SymbologyBitsFinal2);
        }
        if(FormatOutputOperation === "uppercase"){
            console.log("uppercase");
            SymbologyBitsFinal2 = SymbologyBitsFinal2.replace("00", "02");
            console.log(SymbologyBitsFinal2);
        }
        if(FormatOutputOperation === "lowercase"){
            console.log("lowercase");
            SymbologyBitsFinal2 = SymbologyBitsFinal2.replace("00", "03");
            console.log(SymbologyBitsFinal2);
        }
        if(PrefixOperation !== ""){
            PrefixFinal = PrefixOperation;
            PrefixFinal = MetaCharReplaced(PrefixFinal);
            console.log(PrefixFinal);
        }
        if(AIMIDOperation !== "not selected"){
            PrefixOperation = MetaCharReplaced(PrefixOperation);
            if(AIMIDOperation === "before"){
                PrefixFinal = "^01" + PrefixOperation;
            }else{
                PrefixFinal = PrefixOperation + "^01";
            }
            console.log(PrefixFinal);
        }
        if(SendPartialBarcodeOperation[0] !== undefined){
            var startingBarcodePosition = SendPartialBarcodeOperation[0];
            var endingBarcodePosition = SendPartialBarcodeOperation[1];
            SendpartialBcodeFinal = "!" + startingBarcodePosition + "," + endingBarcodePosition + ",";
            console.log(SendpartialBcodeFinal);
        }
        if(CharToInsertOperation[0] !== undefined){
            var characterToinsert = CharToInsertOperation[0];
            characterToinsert = MetaCharReplaced(characterToinsert);
            var insertAfterposition = CharToInsertOperation[1];
            CharInsertedFinal = "!" + "1" + "," + insertAfterposition + "," + characterToinsert + "!" + (parseInt(insertAfterposition) + 1) + ",,";
            console.log(CharInsertedFinal);
        }
        
        if(SuffixOperation !== ""){
            SuffixFinal = SuffixOperation;
            console.log("Sufix: " + SuffixFinal);
        }
        
        
        if(BarcodeLenghtFinal !== ""){
            finalCode = finalCode + "||" + BarcodeLenghtFinal;
            
        }
        if(BarcodeDataFinal !== ""){
            finalCode = finalCode + "|" + BarcodeDataFinal;
            
        }
        finalCode = finalCode + "^A!,,"
        if(BarcodeSubstitutionFinal !== ""){
            finalCode = finalCode + BarcodeSubstitutionFinal;
            
        }
        var numberSegments = finalCode.length + 12;
        var hexaNumberSegments;
        if(numberSegments < 16){
             hexaNumberSegments = "00" + numberSegments.toString(16).toUpperCase();
        }else{
             hexaNumberSegments = "0" + numberSegments.toString(16).toUpperCase();
        }
        
        finalCode = "\"" + SymbologyBitsFinal + finalCode + hexaNumberSegments + endMarker + "\"";


        
        
        if(PrefixFinal !== ""){
            finalCode2 = finalCode2 + PrefixFinal;
            
            
        }
        if(SendpartialBcodeFinal !== ""){
            finalCode2 = finalCode2 + SendpartialBcodeFinal;
          
            
        }else{
            finalCode2 = finalCode2 + "!,,";
           
        }
        if(CharInsertedFinal !== ""){
            finalCode2 = finalCode2 + CharInsertedFinal;
           
        }
        if(SuffixFinal !== ""){
            finalCode2 = finalCode2 + SuffixFinal;
            
        }
        var numberSegments2 = finalCode2.length + 12;
        var hexaNumberSegments2;
        if(numberSegments2 < 16){
             hexaNumberSegments2 = "00" + numberSegments2.toString(16).toUpperCase();
        }else{
             hexaNumberSegments2 = "0" + numberSegments2.toString(16).toUpperCase();
        }
        

        finalCode2 =  "\"" + SymbologyBitsFinal2 + finalCode2 + hexaNumberSegments2 + endMarker + "\"";
       
            finalCodeString = header + dataSmValidationCommand + split + stringMatchingCmd +  finalCode + split + dataDfValidationCommand + split +  formatDataCmd + finalCode2 + offCommand + footer;
        
        console.log(finalCodeString);
        const webApiRequest = 'https://Barcodegen.ecs-dev.codecorp.com:5000/barcodegen/';
        var formData = new FormData();
         formData.append("BarcodeString", "SimpleParsingRule");
         formData.append("BarcodeData", finalCodeString);
       axios({
         method: "POST",
         url: webApiRequest,
         data: formData,
         headers: { "Content-Type": "multipart/form-data"},
         responseType: "blob"
       }).then((response) => {
         console.log(response);
         const reader = new window.FileReader();
         reader.readAsDataURL(response.data);
         reader.onload = () =>{
            setImageBarcode(reader.result);
         }
         setShowDownloadButton(true);
       });
    }


    const componentRef = useRef();
    const pageStyle = " @page { size: 8in 5in }"
                       
    const handlePrint = useReactToPrint({
        content : () => componentRef.current,
        documentTitle: 'Simple Parsing Data',
    });
    
    return (
        <>
        
        <div class="downloadButton2">
                   <button onClick={buttonClick}>Generate Code</button>
        </div>
        <div class='downloadButton' ref={componentRef}>
            <style>{pageStyle}</style>
        {imageBarcode ? (<img src={imageBarcode} alt='img' width="500" height="500"></img>) : null} 
        <div class="optionsSelected">
                 {optionsSelected}
                {optionsSelected2}
                {optionsSelected3}
                 
        </div>
        </div>
        {showDownloadButton &&
        <div id="downloadimage" class="downloadButton2">
              <Button variant="outlined" color="primary" href={imageBarcode} download="Simple Parsing Rule" > Download  </Button> 
             <div class="printButton">
             <Button variant="outlined" color="primary" onClick={handlePrint}>Print</Button>
             </div>
        </div>
        }
        </>
    );
}


export default App;