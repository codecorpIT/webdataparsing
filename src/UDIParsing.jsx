import React, { useRef, useState } from 'react';
import Multiselect from 'multiselect-react-dropdown';
import CreateBarcode from "./GetUDIBarcodeCreated";

let symbologiesSelected = [];
let separatorStringSelected = "";
let outputSuccessFormatSelected = "";
let outputFailureFormatSelected = "";

export function SymbologiesSelectedByUser (){
    return symbologiesSelected;
}
export function SeparatorStringSelectedByUser(){
    return separatorStringSelected;
}
export function OutputSuccessFormatSelectedByUser(){
    return outputSuccessFormatSelected;
}
export function OutputFailureFormatSelectedByUser(){
    return outputFailureFormatSelected;
}
function UDIParsing() {
    var [symbologiesToMatchOptions, setSymbologies] = useState([ "All","Australian Post", "Aztec", "BC412", "Canada Post", "Codabar", "Codablock F", "Code 11", "Code 32", "Code 39", "Code 49", "Code 93", "Code 128"
    ,"Composite Code A", "Composite Code B", "Composite Code C", "Data Matrix", "Dutch Post (KIX)", "EAN-13", "EAN-8", "GoCode", "Grid Matrix", "GS1 Databar", "GS1 Databar Expanded", "GS1 Databar Expanded Stacked", "GS1 Databar Limited", "GS1 Databar Stacked",
    "HanXin", "Hong Kong 2 of 5", "Interleaved 2 of 5", "Japan post", "Korea Post", "Matrix 2 of 5", "Maxicode", "Micro QR Code", "MicroPDF417", "MSI Plessey", "NEC 2 of 5", "PDF417", "Pharma Code", "QR Code", "QR Code Model 1", "Straight 2 of 5 (3 start/stop bars)",
    "Telepen", "TriOptic", "UK Plessey", "UK Royal Mail", "UPC-A", "UPC-E", "UPU ID", "USPS Intelligent Mail", "USPS Planet", "USPS Postnet" ]);    var [separatorOptions, setSeparatorOptions] = useState(["None", "Group Separator", "Carriage return and line feed", "< >"]);
    var [outputFormatOptions, setOutputFormatOptions] = useState(["None", "Demo Output With Detail Data", "[(DI)][DI attribute data]", "[(PI)][PI attribute data]", "[(DI) + (PI)][DI attribute data + PI attribute data]", "[DI attribute data]", "[PI attribute data]", "[DI attribute data + PI attribute data]"]);
    var [failureFormatOptions, setFailureFormatOptions] = useState(["No output data", "[<ERROR: error message>][space][decoded data]", "[Decoded Data]"]);
    var [symnologiesMatched, getSymbologiesMatched] = useState();
    var [separatorMatched, getSeparatorMatched] = useState();
    var [successFormatMatched, getSuccessFormatMatched] = useState();
    var [failureFormatMatched, getFailureFormatMatched] = useState();
    

    const SymbologiesToMatch =  (e) => {
        console.log(e);
       var header = "<Symbologies To Match>";
       getSymbologiesMatched(header + "Equal To " + e);
       if(e[0] === undefined){
           getSymbologiesMatched(null);
       }
       symbologiesSelected = e;
       
       }
    
    const SeparatorString = (e) =>{
        var header = "<Separator>";
        getSeparatorMatched(header+ e);
        if(e[0] === undefined){
            getSeparatorMatched(null);
        }
        separatorStringSelected = e;
    }

    const OutputSuccessFormat = (e) =>{
        var header = "<Success Action>";
        getSuccessFormatMatched(header+ e);
        if(e[0] === undefined){
            getSuccessFormatMatched(null);
        }
        outputSuccessFormatSelected = e;
    }
    const OutputFailureFormat = (e) =>{
        var header = "<Failure Action>";
        getFailureFormatMatched(header+ e);
        if(e[0] === undefined){
            getFailureFormatMatched(null);
        }
        outputFailureFormatSelected = e;
    }
    
    
        return ( 
               
                <div class="newaction">
                <h5>New Action</h5> License 5020 required
                             <div class="boxGS1">
                                <div class="GS1Buttons">
                                <small>Symbologies to Match:</small>
                                    <Multiselect 
                                    id="symboID"
                                    isObject={false}
                                     onSelect={SymbologiesToMatch}
                                     onRemove={SymbologiesToMatch}
                                     options={symbologiesToMatchOptions}
                                     showArrow
                                     hidePlaceholder
                                     
                                     /><br/>
                                     
                                    
                                <small>Define Success Action To Output</small>
                                    <Multiselect 
                                    isObject={false}
                                     onSelect={OutputSuccessFormat}
                                     onRemove={OutputSuccessFormat}
                                     options={outputFormatOptions}
                                     showArrow
                                     hidePlaceholder
                                     singleSelect
                                     /><br/>
                                      <small>Define Failure Action To Output</small>
                                    <Multiselect 
                                    isObject={false}
                                     onSelect={OutputFailureFormat}
                                     onRemove={OutputFailureFormat}
                                     options={failureFormatOptions}
                                     showArrow
                                     hidePlaceholder
                                     singleSelect
                                     />
                                
                                <br/>
                                <small>Define Separator</small>
                                    <Multiselect 
                                    isObject={false}
                                     onSelect={SeparatorString}
                                     onRemove={SeparatorString}
                                     options={separatorOptions}
                                     showArrow
                                     hidePlaceholder
                                     singleSelect
                                     /><br/>
                                     
                                <div class="buttonsdownMatch">
                           </div>
                                </div>
                                
                                <div class="imagebuttons">   
                                </div>

                               
                                <div class="databox">
                                <small><p class="text">Chosen Operations</p></small><br/>
                                                    {symnologiesMatched}<br/><br/>                                                                                                
                                                    {successFormatMatched}<br/><br/>
                                                    {failureFormatMatched}<br/><br/>
                                                    {separatorMatched}<br/><br/>

                                 </div>

                            </div> 
                            <div className="createbarcode">    
                                         <CreateBarcode />              
                                </div>                          
                 </div>
                
    )

}
         
 export default UDIParsing;