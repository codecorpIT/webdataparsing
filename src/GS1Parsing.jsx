import React, { useRef, useState } from 'react';
import cancelpic from './img/cancel.png';
import deletepic from './img/delete.png';
import Multiselect from 'multiselect-react-dropdown';
import CreateBarcode from "./GetGS1BarcodeCreated";

let symbologiesSelected = [];
let IasValidation = [];
let separatorStringSelected = "";
let dateFormatSelected = "";
let outputSuccessFormatSelected = "";
let outputFailureFormatSelected = "";

export function SymbologiesSelectedByUser (){
    return symbologiesSelected;
}
export function IASValidationSelectedByUser(){
    return IasValidation;
}
export function SeparatorStringSelectedByUser(){
    return separatorStringSelected;
}
export function DateFormatSelectedByUser(){
    return dateFormatSelected;
}
export function OutputSuccessFormatSelectedByUser(){
    return outputSuccessFormatSelected;
}
export function OutputFailureFormatSelectedByUser(){
    return outputFailureFormatSelected;
}
function GS1Parsing() {
    var [symbologiesToMatchOptions, setSymbologies] = useState([ "All","Australian Post", "Aztec", "BC412", "Canada Post", "Codabar", "Codablock F", "Code 11", "Code 32", "Code 39", "Code 49", "Code 93", "Code 128"
    ,"Composite Code A", "Composite Code B", "Composite Code C", "Data Matrix", "Dutch Post (KIX)", "EAN-13", "EAN-8", "GoCode", "Grid Matrix", "GS1 Databar", "GS1 Databar Expanded", "GS1 Databar Expanded Stacked", "GS1 Databar Limited", "GS1 Databar Stacked",
    "HanXin", "Hong Kong 2 of 5", "Interleaved 2 of 5", "Japan post", "Korea Post", "Matrix 2 of 5", "Maxicode", "Micro QR Code", "MicroPDF417", "MSI Plessey", "NEC 2 of 5", "PDF417", "Pharma Code", "QR Code", "QR Code Model 1", "Straight 2 of 5 (3 start/stop bars)",
    "Telepen", "TriOptic", "UK Plessey", "UK Royal Mail", "UPC-A", "UPC-E", "UPU ID", "USPS Intelligent Mail", "USPS Planet", "USPS Postnet" ]);    var [validationIASOptions, setValidationIAS] = useState(["All", "Global Trade Item Number (GTIN)", "Batch/lot Number", "Expiration Date", "Serial Number", "Best Before Date"]);
    var [separatorOptions, setSeparatorOptions] = useState(["None", "Group Separator", "Carriage return and line feed", "< >"]);
    var [dateFormatOptions, setDateFormatOptions] = useState(["None", "MMDDYYYY", "MM-DD-YYYY", "MM/DD/YYYY", "MM.DD.YYYY", "MMDDYY", "MM-DD-YY", "MM/DD/YY", "MM.DD.YY", "MMYY", "MM-YY", "MM/YY", "MM.YY", "YYYYMMDD", "YYYY-MM-DD", "YYYY/MM/DD", "YYYY.MM.DD", "YYMMDD", "YY-MM-DD", "YY/MM/DD", "YY.MM.DD", "YYMM", "YY-MM", "YY/MM", "YY.MM", "DDMMYYYY", "DD-MM-YYYY", "DD/MM/YYYY", "DD.MM.YYYY", "DDMMYY", "DD-MM-YY", "DD/MM/YY", "DD.MM.YY", "MON-DD-YYYY", "YYYY-MON-DD"]);
    var [outputFormatOptions, setOutputFormatOptions] = useState(["None", "Numeric", "Human Readable", "Numeric - Human Readable", "Numeric with () around the numeric AI's name"]);
    var [failureFormatOptions, setFailureFormatOptions] = useState(["No output data", "<ERROR: error message>", "Decoded Data"]);
    var [symnologiesMatched, getSymbologiesMatched] = useState();
    var [validationIasMatched, getValidationIasMatched] = useState();
    var [separatorMatched, getSeparatorMatched] = useState();
    var [dateFormatMatched, getDateFormatMatched] = useState();
    var [successFormatMatched, getSuccessFormatMatched] = useState();
    var [failureFormatMatched, getFailureFormatMatched] = useState();
    

    const SymbologiesToMatch =  (e) => {
        console.log(e);
       var header = "<Symbologies To Match>";
       getSymbologiesMatched(header + "Equal To " + e);
       if(e[0] === undefined){
           getSymbologiesMatched(null);
       }
       symbologiesSelected = e;
       
       }
    const IASValidation = (e) =>{
        var header = "<Validate IAs>";
        getValidationIasMatched(header+ e);
        if(e[0] === undefined){
            getValidationIasMatched(null);
        }
        IasValidation = e;
    }
    const SeparatorString = (e) =>{
        var header = "<Separator>";
        getSeparatorMatched(header+ e);
        if(e[0] === undefined){
            getSeparatorMatched(null);
        }
        separatorStringSelected = e;
    }
    const DateFormat = (e) =>{
        var header = "<Date Format>";
        getDateFormatMatched(header+ e);
        if(e[0] === undefined){
            getDateFormatMatched(null);
        }
        dateFormatSelected = e;
    }
    const OutputSuccessFormat = (e) =>{
        var header = "<Success Format>";
        getSuccessFormatMatched(header+ e);
        if(e[0] === undefined){
            getSuccessFormatMatched(null);
        }
        outputSuccessFormatSelected = e;
    }
    const OutputFailureFormat = (e) =>{
        var header = "<Failure Format>";
        getFailureFormatMatched(header+ e);
        if(e[0] === undefined){
            getFailureFormatMatched(null);
        }
        outputFailureFormatSelected = e;
    }
    
    
        return ( 
               
                <div class="newaction">
                <h5>New Action</h5> License 5019 required
                             <div class="boxGS1">
                                <div class="GS1Buttons">
                                <small>Symbologies to Match:</small>
                                    <Multiselect 
                                    id="symboID"
                                    isObject={false}
                                     onSelect={SymbologiesToMatch}
                                     onRemove={SymbologiesToMatch}
                                     options={symbologiesToMatchOptions}
                                     showArrow
                                     hidePlaceholder
                                     
                                     /><br/>
                                     <small>Validate User Defined AIs</small>
                                    <Multiselect 
                                    isObject={false}
                                     onSelect={IASValidation}
                                     onRemove={IASValidation}
                                     options={validationIASOptions}
                                     showArrow
                                     hidePlaceholder
                                     
                                     /><br/>
                                     
                                     <small>Define Separator</small>
                                    <Multiselect 
                                    isObject={false}
                                     onSelect={SeparatorString}
                                     onRemove={SeparatorString}
                                     options={separatorOptions}
                                     showArrow
                                     hidePlaceholder
                                     singleSelect
                                     /><br/>
                                     <small>Define Date Format</small>
                                    <Multiselect 
                                    isObject={false}
                                     onSelect={DateFormat}
                                     onRemove={DateFormat}
                                     options={dateFormatOptions}
                                     showArrow
                                     hidePlaceholder
                                     singleSelect
                                     /><br/>
                                <small>Define Output Success Format</small>
                                    <Multiselect 
                                    isObject={false}
                                     onSelect={OutputSuccessFormat}
                                     onRemove={OutputSuccessFormat}
                                     options={outputFormatOptions}
                                     showArrow
                                     hidePlaceholder
                                     singleSelect
                                     /><br/>
                                      <small>Define Output Failure Format</small>
                                    <Multiselect 
                                    isObject={false}
                                     onSelect={OutputFailureFormat}
                                     onRemove={OutputFailureFormat}
                                     options={failureFormatOptions}
                                     showArrow
                                     hidePlaceholder
                                     singleSelect
                                     />
                                
                                <br/>
                                <div class="buttonsdownMatch">
                           </div>
                                </div>
                                
                                <div class="imagebuttons">   
                                </div>

                               
                                <div class="databox">
                                <small><p class="text">Chosen Operations</p></small><br/>
                                                    {symnologiesMatched}<br/><br/>
                                                    {validationIasMatched}<br/><br/>
                                                    {separatorMatched}<br/><br/>
                                                    {dateFormatMatched}<br/><br/>
                                                    {successFormatMatched}<br/><br/>
                                                    {failureFormatMatched}

                                 </div>

                            </div> 
                            <div className="createbarcode">    
                                         <CreateBarcode />              
                                </div>                          
                 </div>
                
    )

}
         
 export default GS1Parsing;