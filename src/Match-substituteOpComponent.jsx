import React, { Component } from 'react';
import deletepic from './img/delete.png';
import { useState } from 'react';
import {TextField} from '@material-ui/core';
import { useEffect } from 'react';

var iteration = 0;
var SubstituteDataInBarcode = [];
var SubstituteDataInBarcode2 = [];
var SubstituteDataInBarcode3 = [];
var SubstituteDataInBarcode4 = [];
var SubstituteDataInBarcode5 = [];
var barReplaced= "";
var doNotTransmitBarcode = false;
var optionsSelectedInTotal = "\n";

export function AllOptionsSelectedMatchSubs(){
    return optionsSelectedInTotal;
}

export function DoNotTransmitBarcodeselected(){
   return doNotTransmitBarcode;
}
export function SubstitutedatainbarcodeOp(){
   return SubstituteDataInBarcode;
}
export function SubstitutedatainbarcodeOp2(){
   return SubstituteDataInBarcode2;
}
export function SubstitutedatainbarcodeOp3(){
   return SubstituteDataInBarcode3;
}
export function SubstitutedatainbarcodeOp4(){
   return SubstituteDataInBarcode4;
}
export function SubstitutedatainbarcodeOp5(){
   return SubstituteDataInBarcode5;
}

export function MatchSubstitute({effectOnParsing, setEffectOnAfterParsing}){

    function MetaCharReplaced(string) {

        if(string.includes("/")){
            string = string.replace(/\//g, "/2F");
         }
        if(string.includes("!")){
           string = string.replace(/\!/g, '/21');
        }
        if(string.includes("#")){
            string = string.replace(/\#/g, "/23");
         }
         if(string.includes("$")){
            string = string.replace(/\$/g, "/24");
         }
         if(string.includes("*")){
            string = string.replace(/\*/g, "/2A");
         }
         if(string.includes(",")){
            string = string.replace(/\,/g, "/2C");
         }
         if(string.includes("^")){
            string = string.replace(/\^/g, "/5E");
         }
         if(string.includes("|")){
            string = string.replace(/\|/g, "/7C");
         }
         return string;
    }
    var [Datasubstitute, setDatasustitute] = useState();
    var [Datasubstitute2, setDatasustitute2] = useState();
    var [Datasubstitute3, setDatasustitute3] = useState();
    var [Datasubstitute4, setDatasustitute4] = useState();
    var [Datasubstitute5, setDatasustitute5] = useState();
    var[effectOnSample, setEffectOnSample] = useState();
    var[effectOnSample2, setEffectOnSample2] = useState();
    var[effectOnSample3, setEffectOnSample3] = useState();
    var[effectOnSample4, setEffectOnSample4] = useState();
    var[effectOnSample5, setEffectOnSample5] = useState();
    var[effectChange, setEffectChange] = useState();
    
   
    /*
    var obj = {
      value: '',
      letMeKnow() {
        console.log(`The variable has changed to ${this.testVar}`);
       
        
      },
      get testVar() {
        return this.value;
      },
      set testVar(value) {
        this.value = value;
        this.letMeKnow();
      }
    }
    
    obj.testVar = effectOnParsing;
   */
    barReplaced = effectOnParsing;
    useEffect(() => {
 
      if(effectOnSample != null){
         SubstituteData();
         
      }
      setEffectOnAfterParsing(effectOnParsing);
    }, [effectOnParsing]);
   
    const [stringToReplace, setStringToReplace] = useState("");
    const [replaceString, setReplaceString] = useState("");
    const [showSubstitute, setShowSubstitue] = useState(false);
  const SubstituteData = (e) =>{
      
      console.log(e);
      setStringToReplace(e);    
  }
  const Replacement = (e) =>{

   if(e == null){
      return;
   }    
   console.log(e);
   setReplaceString(e);
      
   var stringToReplaceChars = MetaCharReplaced(stringToReplace);
   var replacement = MetaCharReplaced(e);
   
       SubstituteDataInBarcode = [stringToReplaceChars, replacement];
       setDatasustitute("<Sustitute> " + stringToReplaceChars + " With " + replacement);
         
       if(barReplaced.includes(stringToReplaceChars)){
         barReplaced = barReplaced.replaceAll(stringToReplaceChars, replacement);
     }
     setEffectOnSample(barReplaced);
     setEffectOnAfterParsing(barReplaced);
   
}

  const RemoveAllDataOp = () => {
    setStringToReplace("");
    setReplaceString("");
    Replacement(null);
   SubstituteDataInBarcode = "";
   setDatasustitute(null);
   setEffectOnSample(null);
   setDoNotTransmit(null);
   setTransmitButton("Do Not Transmit Barcode");
   doNotTransmitBarcode = false;
   setShowSubstitue(false);
   setEffectOnAfterParsing(barReplaced);

   
  }

var [doNotTransmit, setDoNotTransmit] = useState();
var [transmitButton, setTransmitButton] = useState("Do Not Transmit Barcode");
const DoNotTransmit = () =>{
   if(transmitButton === "Do Not Transmit Barcode"){
      setDoNotTransmit("<Do Not Transmit>");
      setTransmitButton("Transmit barcode");
      doNotTransmitBarcode = true;
   }else{
      setDoNotTransmit(null);
      setTransmitButton("Do Not Transmit Barcode");
      doNotTransmitBarcode = false;
   }
 
}
const hideSubstituteOptions = () =>{
  
   if(showSubstitute === false){
      setShowSubstitue(true);
   }else{
      setShowSubstitue(false);
   }
   optionsSelectedInTotal = Datasubstitute + "\n" + doNotTransmit;
   optionsSelectedInTotal = optionsSelectedInTotal.replace(/undefined/g, "");

} 
    
        return ( 
            <>
             <small><p class="text">Match-Substitute Operations</p></small>                                
                            <div class="box2">
                                <div>
                                

                            <button className="MatchButtons" onClick={hideSubstituteOptions}><small>Substitute Data in Barcode</small></button>
                           {showSubstitute &&
                            <div id="substitutelabel">
                                    <TextField className="MatchButtons" id="stringToreplace" label="String To Replace" variant="outlined" value={stringToReplace} onChange={e => SubstituteData(e.target.value)}/>
                                    <br/><br/><TextField className="MatchButtons" id="Replacement" label="Replacement" variant="outlined"value={replaceString} onChange={e => Replacement(e.target.value)}/>
                              </div> 
                              
                           }
                            <button className="MatchButtons" onClick={DoNotTransmit}><small>{transmitButton}</small></button><br/>
                            <div class="buttonsdownMatch">
                            <div><input type="image" src={deletepic} alt='delete' height="40px" width="40px" title="Remove all Data-manipulations Operations." onClick={RemoveAllDataOp}/></div>
                            
                            
                            </div>
                                    
                            </div>

                            <div class="imagebuttons">
                            </div>
                            <div class="databox">
                            <small><p class="text">Chosen Operations</p></small><br/>
                                               <small>{Datasubstitute}</small>
                                                <small>{Datasubstitute2}</small>
                                                <small>{Datasubstitute3}</small>
                                                <small>{Datasubstitute4}</small>
                                                <small>{Datasubstitute5}</small>
                                                <small>{doNotTransmit}</small>
                            </div>
                            <div class="databox">
                            <small><p class="text">Effect on Sample if Matched</p></small><br/>
                                                   <small>{effectOnSample}</small>
                                                   <small>{effectOnSample2}</small>
                                                   <small>{effectOnSample3}</small>
                                                   <small>{effectOnSample4}</small>
                                                   <small>{effectOnSample5}</small>
                            
                            </div>

                            </div>
            </>
        );
    }

 
export default MatchSubstitute;