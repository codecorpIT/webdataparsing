import './App.css';
import Footer from './footerComponent';
import Header from './headerComponent';
import SimpleParsing from './simpleParsingComponent';
import DataParsing from './DataParsingComponent';
import GS1Parsing from "./GS1Parsing";
import UDIParsing from './UDIParsing';
import axios from 'axios';
import React, {useState, useEffect} from 'react';
import { useAuth0 } from "@auth0/auth0-react";
import Multiselect from "multiselect-react-dropdown";


function App() {
  const { user, isAuthenticated, getAccessTokenSilently } = useAuth0();
  const [userMetadata, setUserMetadata] = useState(null);
  const { loginWithRedirect } = useAuth0();
  const { logout } = useAuth0();
  const [modeOptions, setModeOptions] = useState(["Simple Parsing", "Data Parsing", "GS1 Parsing", "UDI Parsing"]);
  const[showMainPage, setShowMainPage] = useState(true);
  const[showLogInPage, setShowLogInPage] = useState(false);
  useEffect(() => {
    const getUserMetadata = async () => {
      const domain = "dev-codecorp.us.auth0.com";
  
      try {
         const accessToken = await getAccessTokenSilently({
          authorizationParams: {
            audience: "https://codecorp.com/mycode",
            scope: "TODO"
          },
        });
        
        const userDetailsByIdUrl = `https://${domain}/api/v2/users/${user.sub}`;
  
        const metadataResponse = await fetch(userDetailsByIdUrl, {
          headers: {
            Authorization: `Bearer ${accessToken}`,
          },
        });
  
        const { user_metadata } = await metadataResponse.json();
        
        setUserMetadata(user_metadata);
      setShowMainPage(true);
      setShowLogInPage(false);
      } catch (e) {
        console.log(e.message);
        
      setShowMainPage(false);
      setShowLogInPage(true);
      }
    };
  
    getUserMetadata();
  }, [getAccessTokenSilently, user?.sub]);
  const[showSimpleParsingPage, setShowSimpleParsingPage] = useState(true);
  const[showDataParsingPage, setShowDataParsingPage] = useState(false);
  const[showGs1ParsingPage, setShowGs1ParsingPage] = useState(false);
  const[showUdiParsingPage, setShowUdiParsingPage] = useState(false);

  const selectList = async (e) => {
   
    console.log(e);
    if(e == 'Simple Parsing'){
      
      setShowSimpleParsingPage(true);
      setShowDataParsingPage(false);
      setShowGs1ParsingPage(false);
      setShowUdiParsingPage(false);      
    }
    if(e == 'Data Parsing'){
      
      setShowSimpleParsingPage(false);
      setShowDataParsingPage(true);
      setShowGs1ParsingPage(false);
      setShowUdiParsingPage(false);  
    }
    if(e == 'GS1 Parsing'){
      setShowSimpleParsingPage(false);
      setShowDataParsingPage(false);
      setShowGs1ParsingPage(true);
      setShowUdiParsingPage(false);  
    }
    if(e == 'UDI Parsing'){
      setShowSimpleParsingPage(false);
      setShowDataParsingPage(false);
      setShowGs1ParsingPage(false);
      setShowUdiParsingPage(true);
    }
   
  }

  const webApiRequest = 'http://Barcodegen.ecs-dev.codecorp.com:5000/api/codedm/user/current';      
         
   /*  const data =  axios({
         method: "GET",
         url: webApiRequest,
         headers: {'Authorization': `Bearer ${localStorage.getItem('token')}`},
       }).then((response) => {
         console.log(response.status);  
           
       }).catch((error) => {
        console.error("Respoinse was: " + error.response.status);
       });
     console.log("data is " + data);*/

  return (
    
    <>
    
    <div className="App">
      <div>
        <link
          href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light"
          rel="stylesheet"
          type="text/css" />
        <link
          href="https://fonts.googleapis.com/css2?family=Archivo+Black&family=Permanent+Marker&family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap"
          rel="stylesheet" />
        <link
          href="https://fonts.googleapis.com/css2?family=Inconsolata:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap"
          rel="stylesheet" />
        {/* Vendor CSS */}
        <link
          rel="stylesheet"
          href="https://codecorp.com/site/vendor/bootstrap/css/bootstrap.min.css" />
        <link
          rel="stylesheet"
          href="https://codecorp.com/site/vendor/fontawesome-free/css/all.min.css" />
        <link
          rel="stylesheet"
          href="https://codecorp.com/site/vendor/animate/animate.min.css" />
        <link
          rel="stylesheet"
          href="https://codecorp.com/site/vendor/simple-line-icons/css/simple-line-icons.min.css" />
        <link
          rel="stylesheet"
          href="https://codecorp.com/site/vendor/owl.carousel/assets/owl.carousel.min.css" />
        <link
          rel="stylesheet"
          href="https://codecorp.com/site/vendor/owl.carousel/assets/owl.theme.default.min.css" />
        <link
          rel="stylesheet"
          href="https://codecorp.com/site/vendor/magnific-popup/magnific-popup.min.css" />
        <link
          rel="stylesheet"
          href="https://codecorp.com/site/vendor/bootstrap-star-rating/css/star-rating.min.css" />
        <link
          rel="stylesheet"
          href="https://codecorp.com/site/vendor/bootstrap-star-rating/themes/krajee-fas/theme.min.css" />
        <link
          rel="stylesheet"
          href="https://codecorp.com/site/css/bootstrap4-toggle.min.css" />
        <link
          rel="stylesheet"
          href="https://codecorp.com/site/css/modal-video.min.css" />
        {/* Theme CSS */}
        <link rel="stylesheet" href="https://codecorp.com/site/css/theme.css" />
        <link
          rel="stylesheet"
          href="https://codecorp.com/site/css/theme-elements.css" />
        <link
          rel="stylesheet"
          href="https://codecorp.com/site/css/theme-blog.css" />
        <link
          rel="stylesheet"
          href="https://codecorp.com/site/css/theme-shop.css" />
        {/* Current Page CSS */}
        <link
          rel="stylesheet"
          href="https://codecorp.com/site/vendor/circle-flip-slideshow/css/component.css" />
        {/* Skin CSS */}
        <link rel="stylesheet" href="https://codecorp.com/site/css/default.css" />
        <link
          rel="stylesheet"
          href="https://codecorp.com/site/css/responsive.css" />
        <link
          href="https://codecorp.com/site/css/mcode-custom.css?464654"
          rel="stylesheet" />

      </div>
      <Header></Header>
      {showMainPage &&
      <div id="MainPage" >
      
        <form name="formselect" className="formselect">          
          <Multiselect 
          id="SingleMultiSelect"
          isObject={false}
          options={modeOptions} 
          onSelect={selectList}
          singleSelect
          selectedValues={['Simple Parsing']}
            />
        </form>
        {showSimpleParsingPage && 
        <div id="SimpleParsing">
          <SimpleParsing />
        </div>
        }
        {showDataParsingPage &&
        <div id="DataParsing">
          <DataParsing />
        </div>
        }
        {showGs1ParsingPage &&
        <div id="gs1Parsing">
          <GS1Parsing />
        </div>
        }
        {showUdiParsingPage &&
        <div id="udiParsing">
          <UDIParsing />
        </div>
        }
        

      </div>
      }
    </div>
    {showLogInPage &&
    <div id="LogginPage" className="requestLogIn">
        <h2><p>Please Log in to have access to advance configuration</p></h2>
        <button className="LogInButton" onClick={() => loginWithRedirect()}>Log In</button>
    </div>
    }
    
    <Footer></Footer>
    </>
    
  );
}


export default App;
