import React, { Component } from 'react';
import pencilpic from './img/pencil.png';
import cancelpic from './img/cancel.png';
import deletepic from './img/delete.png';
import arrowup from './img/arrow_up.jpg';
import arrowdown from './img/arrow_down.jpg';
import { useState, useEffect } from 'react';
//import {BarcodeContentsOnSample} from './EffectofParsingComponent';
import {TextField} from '@material-ui/core';
import Multiselect from "multiselect-react-dropdown";
import {Button} from '@material-ui/core';


var SendPartialBarcode = [];
var lastDataInserted = "";
var CharacterToInsert = [];
var iteration = 0;
var SubstituteDataInBarcode = [];
var SubstituteDataInBarcode2 = [];
var SubstituteDataInBarcode3 = [];
var SubstituteDataInBarcode4 = [];
var SubstituteDataInBarcode5 = [];
var doNotTransmitBarcode = false;
var deleteEndBarcode = "";
var alertUserBarcode = "";
var optionsSelectedInTotal = "\n";
var endingPositionString = "0";

export function AllOptionsSelected(){
    return optionsSelectedInTotal;
}
export function SendPartialBarcodeSelected(){
    return SendPartialBarcode;
}
export function InsertCharInBarcode(){
    return CharacterToInsert;
}
export function DeleteEnd(){
    return deleteEndBarcode;
}
export function Substitute1(){
    return SubstituteDataInBarcode;
}
export function Substitute2(){
    return SubstituteDataInBarcode2;
}
export function Substitute3(){
    return SubstituteDataInBarcode3;
}
export function Substitute4(){
    return SubstituteDataInBarcode4;
}
export function Substitute5(){
    return SubstituteDataInBarcode5;
}
export function Alert(){
    return alertUserBarcode;
}
export function Transmit(){
    return doNotTransmitBarcode;
}
function OperationsToPerform({effectOnParsing})  {
    var [partialBarcodeString, setPartialBarcodeString] = useState();
    var [partialBarcodeSample, setPartialBarcodeSample] = useState("");
    var [CommuniOptions, setCommunicationOptions] = useState([ "Keyboard Mode (Default)", "HID Vendor or Serial Mode"]);
    var [insertCharString, setInsertCharString] = useState();
    var [insertCharSample, setinsertCharSample] = useState("");
    var [deleteEndString, setDeleteEndString] = useState();
    var [deleteEndSample, setDeleteEndSample] = useState("");
    var [Datasubstitute, setDatasustitute] = useState();
    var[effectOnSample, setEffectOnSample] = useState("");
    var [alertUserString, setAlertUserString] = useState();
    var [alertUserSample, setAlertUserSample] = useState();
    const [option1, setOption1] = useState(false);
    const [option2, setOption2] = useState(false);
    const [option3, setOption3] = useState(false);
    const [option4, setOption4] = useState(false);
    const [option5, setOption5] = useState(false);
    var barcodeSample = effectOnParsing;
    optionsSelectedInTotal = partialBarcodeString + "\n" + insertCharString + "\n" + deleteEndString + "\n" + Datasubstitute + "\n" + alertUserString;
    optionsSelectedInTotal = optionsSelectedInTotal.replace(/undefined/g, "")
    

    useEffect(() => {
        
        if(partialBarcodeSample !== ""){
            sendPartialBarcode();
        }
        if(insertCharSample !== ""){
            insertChar();
        }
        if(deleteEndSample !== ""){
            deleteEndOfBarcode();
        }
        if(effectOnSample !== ""){
            SubstituteData();
        }
       
      }, [effectOnParsing]);

    function MetaCharReplaced(string) {

        if(string.includes("/")){
            string = string.replace(/\//g, "/2F");
         }
        if(string.includes("!")){
           string = string.replace(/\!/g, '/21');
        }
        if(string.includes("#")){
            string = string.replace(/\#/g, "/23");
         }
         if(string.includes("$")){
            string = string.replace(/\$/g, "/24");
         }
         if(string.includes("*")){
            string = string.replace(/\*/g, "/2A");
         }
         if(string.includes(",")){
            string = string.replace(/\,/g, "/2C");
         }
         if(string.includes("^")){
            string = string.replace(/\^/g, "/5E");
         }
         if(string.includes("|")){
            string = string.replace(/\|/g, "/7C");
         }
         return string;
    }
    const [partialStart, setPartialStart] = useState(0);
    const [partialEnd, setPartialEnd] = useState(0);
    
    function sendPartialbarcodeEnd(e){
        setPartialEnd(e);
    endingPositionString = e;
    sendPartialBarcode(partialStart);
    }
    function sendPartialBarcode(e){
       
                lastDataInserted = "PartialBarcode"; 
                setPartialStart(e);                 
                var startingPosition = e;
                var endingPosition = endingPositionString;
                setPartialBarcodeString("<Send partial>Position " + startingPosition + " To Position " + endingPosition);
                if(endingPosition === "0"){
                    endingPosition = 50;
                    SendPartialBarcode = [startingPosition, "End"];   
                }else{
                    SendPartialBarcode = [startingPosition, endingPosition]; 
                }
                
                var barcodeSplitted = barcodeSample.substring((startingPosition - 1), endingPosition);
                
                setPartialBarcodeSample(barcodeSplitted);
                console.log(barcodeSplitted);
    }
   
    const UpArrowChar = () =>{
        var newString2 = insertCharStringValue + "<Up Arrow-Keyboard>";
        insertChar(newString2);
    }
    const LeftArrowChar = () =>{
        var newString2 = insertCharStringValue + "<Left Arrow-Keyboard>";
        insertChar(newString2);
    }
    const RightArrowChar = () =>{
        var newString2 = insertCharStringValue + "<Right Arrow-Keyboard>";
        insertChar(newString2);
    }
    const DownArrowChar = () =>{
        var newString2 = insertCharStringValue + "<Down Arrow-Keyboard>";
        insertChar(newString2);
    }
    const TabChar = () =>{
        var newString2 = insertCharStringValue + "<Tab-Keyboard>";
        insertChar(newString2);
    }
    const DeleteChar = () =>{
        var newString2 = insertCharStringValue + "<Delete-Keyboard>";
        insertChar(newString2);
    }
    const EscChar = () =>{
        var newString2 = insertCharStringValue + "<Esc-Keyboard>";
        insertChar(newString2);
    }
    const EnterChar = () =>{
        var newString2 = insertCharStringValue + "<Enter-Keyboard>";
        insertChar(newString2);
    }
    const EndChar = () =>{
        var newString2 = insertCharStringValue + "<End-Keyboard>";
        insertChar(newString2);
    }
    const BackspaceChar = () =>{
        var newString2 = insertCharStringValue + "<Backspace-Keyboard>";
        insertChar(newString2);
    }
    const InsertChar = () =>{
        var newString2 = insertCharStringValue + "<Insert-Keyboard>";
        insertChar(newString2);
    }
    const PageUpChar = () =>{
        var newString2 = insertCharStringValue + "<Page Up-Keyboard>";
        insertChar(newString2);
    }
    const PageDownChar = () =>{
        var newString2 = insertCharStringValue + "<Page Down-Keyboard>";
        insertChar(newString2);
    }
    const HomeChar = () =>{
        var newString2 = insertCharStringValue + "<Home-Keyboard>";
        insertChar(newString2);
    }
    const Number0Char = () =>{
        var newString2 = insertCharStringValue + "0";
        insertChar(newString2);
    }
    const Number1Char = () =>{
        var newString2 = insertCharStringValue + "1";
        insertChar(newString2);
    }
    const Number2Char = () =>{
        var newString2 = insertCharStringValue + "2";
        insertChar(newString2);
    }
    const Number3Char = () =>{
        var newString2 = insertCharStringValue + "3";
        insertChar(newString2);
    }
    const Number4Char = () =>{
        var newString2 = insertCharStringValue + "4";
        insertChar(newString2);
    }
    const Number5Char = () =>{
        var newString2 = insertCharStringValue + "5";
        insertChar(newString2);
    }
    const Number6Char = () =>{
        var newString2 = insertCharStringValue + "6";
        insertChar(newString2);
    }
    const Number7Char = () =>{
        var newString2 = insertCharStringValue + "7";
        insertChar(newString2);
    }
    const Number8Char = () =>{
        var newString2 = insertCharStringValue + "8";
        insertChar(newString2);
    }
    const Number9Char = () =>{
        var newString2 = insertCharStringValue + "9";
        insertChar(newString2);
    }
    const F1Char = () =>{
        var newString2 = insertCharStringValue + "<F1-Keyboard>";
        insertChar(newString2);
    }
    const F2Char = () =>{
        var newString2 = insertCharStringValue + "<F2-Keyboard>";
        insertChar(newString2);
    }
    const F3Char = () =>{
        var newString2 = insertCharStringValue + "<F3-Keyboard>";
        insertChar(newString2);
    }
    const F4Char = () =>{
        var newString2 = insertCharStringValue + "<F4-Keyboard>";
        insertChar(newString2);
    }
    const F5Char = () =>{
        var newString2 = insertCharStringValue + "<F5-Keyboard>";
        insertChar(newString2);
    }
    const F6Char = () =>{
        var newString2 = insertCharStringValue + "<F6-Keyboard>";
        insertChar(newString2);
    }
    const F7Char = () =>{
        var newString2 = insertCharStringValue + "<F7-Keyboard>";
        insertChar(newString2);
    }
    const F8Char = () =>{
        var newString2 = insertCharStringValue + "<F8-Keyboard>";
        insertChar(newString2);
    }
    const F9Char = () =>{
        var newString2 = insertCharStringValue + "<F9-Keyboard>";
        insertChar(newString2);
    }
    const F10Char = () =>{
        var newString2 = insertCharStringValue + "<F10-Keyboard>";
        insertChar(newString2);
    }
    const F11Char = () =>{
        var newString2 = insertCharStringValue + "<F11-Keyboard>";
        insertChar(newString2);
    }
    const F12Char = () =>{
        var newString2 = insertCharStringValue + "<F12-Keyboard>";
        insertChar(newString2);
    }
    const NULL = () =>{
        var newString2 = insertCharStringValue + "<NULL>";
        insertChar(newString2);
    }
    const SOH = () =>{
        var newString2 = insertCharStringValue + "<SOH>";
        insertChar(newString2);
    }
    const STX = () =>{
        var newString2 = insertCharStringValue + "<STX>";
        insertChar(newString2);
    }
    const ETX = () =>{
        var newString2 = insertCharStringValue + "<ETX>";
        insertChar(newString2);
    }
    const EOT = () =>{
        var newString2 = insertCharStringValue + "<EOT>";
        insertChar(newString2);
    }
    const ENQ = () =>{
        var newString2 = insertCharStringValue + "<ENQ>";
        insertChar(newString2);
    }
    const ACK = () =>{
        var newString2 = insertCharStringValue + "<ACK>";
        insertChar(newString2);
    }
    const BEL = () =>{
        var newString2 = insertCharStringValue + "<BEL>";
        insertChar(newString2);
    }
    const BS = () =>{
        var newString2 = insertCharStringValue + "BS";
        insertChar(newString2);
    }
    const TAB = () =>{
        var newString2 = insertCharStringValue + "<TAB>";
        insertChar(newString2);
    }
    const LF = () =>{
        var newString2 = insertCharStringValue + "<LF-NL>";
        insertChar(newString2);
    }
    const VT = () =>{
        var newString2 = insertCharStringValue + "<VT>";
        insertChar(newString2);
    }
    const FF = () =>{
        var newString2 = insertCharStringValue + "<FF-NP>";
        insertChar(newString2);
    }
    const CR = () =>{
        var newString2 = insertCharStringValue + "<CR>";
        insertChar(newString2);
    }
    const SO = () =>{
        var newString2 = insertCharStringValue + "<SO>";
        insertChar(newString2);
    }
    const SI = () =>{
        var newString2 = insertCharStringValue + "<SI>";
        insertChar(newString2);
    }
    const DLE = () =>{
        var newString2 = insertCharStringValue + "<DLE>";
        insertChar(newString2);
    }
    const DC1 = () =>{
        var newString2 = insertCharStringValue + "<DC1>";
        insertChar(newString2);
    }
    const DC2 = () =>{
        var newString2 = insertCharStringValue + "<DC2>";
        insertChar(newString2);
    }
    const DC3 = () =>{
        var newString2 = insertCharStringValue + "<DC3>";
        insertChar(newString2);
    }
    const DC4 = () =>{
        var newString2 = insertCharStringValue + "<DC4>";
        insertChar(newString2);
    }
    const NAK = () =>{
        var newString2 = insertCharStringValue + "<NAK>";
        insertChar(newString2);
    }
    const SYN = () =>{
        var newString2 = insertCharStringValue + "<SYN>";
        insertChar(newString2);
    }
    const ETB = () =>{
        var newString2 = insertCharStringValue + "<ETB>";
        insertChar(newString2);
    }
    const CAN = () =>{
        var newString2 = insertCharStringValue + "<CAN>";
        insertChar(newString2);
    }
    const EM = () =>{
        var newString2 = insertCharStringValue + "<EM>";
        insertChar(newString2);
    }
    const SUB = () =>{
        var newString2 = insertCharStringValue + "<SUB>";
        insertChar(newString2);
    }
    const ESC = () =>{
        var newString2 = insertCharStringValue + "<ESC>";
        insertChar(newString2);
    }
    const FS = () =>{
        var newString2 = insertCharStringValue + "<FS>";
        insertChar(newString2);
    }
    const GS = () =>{
        var newString2 = insertCharStringValue + "<GS>";
        insertChar(newString2);
    }
    const RS = () =>{
        var newString2 = insertCharStringValue + "<RS>";
        insertChar(newString2);
    }
    const US = () =>{
        var newString2 = insertCharStringValue + "<US>";
        insertChar(newString2);
    }
    const DEL = () =>{
        var newString2 = insertCharStringValue + "<DEL>";
        insertChar(newString2);
    }

    const [insertCharStringValue, setInsertCharStringValue] = useState("");
    const [afterPositionValue, setAfterPositionValue] = useState(0);
    var afterPositionVariable = "0";

    function afterPositionFunction(e){
    setAfterPositionValue(e);
    afterPositionVariable = e;
    insertChar(insertCharStringValue);
    }
   function insertChar(e){
    lastDataInserted = "Char"; 
    setInsertCharStringValue(e);
   
    var charToinsert = e;
    var afterPosition = afterPositionVariable;
    CharacterToInsert = [charToinsert, afterPosition];
    setInsertCharString("<Insert Characters>" + charToinsert + " After Position " + afterPosition);
    var barcodeSplitted1 = barcodeSample.substring(0, afterPosition);
    var barcodeSplitted2 = barcodeSample.substring(afterPosition, 50);
   
        setinsertCharSample( barcodeSplitted1 + charToinsert + barcodeSplitted2);
    
    
   }
   
   const[showKeyboardChars, setShowKeyboardChars] = useState(false);
   const[showCommunicationMode, setShowCommunicationMode] = useState(false);
   const[showNonPrintableChars, setShowNonPrintableChars] = useState(false);
   const KeyboardChars2 = () =>{
    
    if(showKeyboardChars === true){
        setShowKeyboardChars(false);
        setShowCommunicationMode(false);
    }else{
        setShowKeyboardChars(true);
        setShowCommunicationMode(false);

    }
    
}
const nonPrintableChars = () =>{
    
    if(showNonPrintableChars === true){
        setShowNonPrintableChars(false);
        setShowCommunicationMode(false);
    }else{
        setShowNonPrintableChars(true);
        setShowCommunicationMode(false);
    }
}
const commModeHidden = () =>{
   
    if(showCommunicationMode === true){
        setShowCommunicationMode(false);
        setShowKeyboardChars(false);
        setShowNonPrintableChars(false);
    }else{
        setShowCommunicationMode(true);
        setShowKeyboardChars(false);
        setShowNonPrintableChars(false);
    }
}
const[showKeyboardCharButton, setShowKeyboardCharButton] = useState(true);
const[showNonPrintableButton, setShowNonPrintableButton] = useState(false);
const commMode = (e) =>{
     
    if(e.includes("Keyboard Mode (Default)")){
        setShowKeyboardCharButton(true);
        setShowNonPrintableButton(false);
    }
    if(e.includes("HID Vendor or Serial Mode")){
        setShowKeyboardCharButton(false);
        setShowNonPrintableButton(true);
    }
    
}

const[deleteEndValue, setDeleteEndValue] = useState("");

function deleteEndOfBarcode(e){
    setDeleteEndValue(e);
        
        var charactersToDelete = e;
        deleteEndBarcode = charactersToDelete;
        var totalLenght = barcodeSample.length;
        var barcodeDeleted = barcodeSample.substring(0, (totalLenght - charactersToDelete));
        setDeleteEndString("<Delete End> " + charactersToDelete + " Characters");
        setDeleteEndSample(barcodeDeleted);
        console.log(barcodeDeleted);
        lastDataInserted = "DeleteEnd"
    
}
const [stringToReplace, setStringToReplace] = useState("");
    const [replaceString, setReplaceString] = useState("");
  const SubstituteData = (e) =>{
      
      console.log(e);
      setStringToReplace(e);    
  }

function Replacement(e) {

    if(e == null){
        return;
     }    
     
     setReplaceString(e);
       lastDataInserted = "SubstituteData"
      var stringToReplaceChars = MetaCharReplaced(stringToReplace);
      var replacement = MetaCharReplaced(e);
       var barReplaced = barcodeSample;
       
           SubstituteDataInBarcode = [stringToReplaceChars, replacement];
           setDatasustitute("<Sustitute> " + stringToReplaceChars + " With " + replacement);
            
           if(barReplaced.includes(stringToReplaceChars)){
               barReplaced = barReplaced.replaceAll(stringToReplaceChars, replacement);           
           }
           setEffectOnSample(barReplaced);
       
    
      
   }

   const [alertUserBeepsValue, setAlertuserBeepsValue] = useState(1);
   
   function alertUser(e){
      
       lastDataInserted = "AlertUser";
       setAlertuserBeepsValue(e);
       var alertBeeps = e;
       if(alertBeeps < 1 || alertBeeps > 10){
           alert("ERROR: Min 1 or Max 10 beeps");
           return;
       }
       setAlertUserString("<Alert User> " + alertBeeps);
       setAlertUserSample("Alert User With " + alertBeeps + " beeps");
       alertUserBarcode = alertBeeps;
   }
   var [doNotTransmit, setDoNotTransmit] = useState();
    var [transmitButton, setTransmitButton] = useState("Do Not Transmit Barcode");
const DoNotTransmit = () =>{
   if(transmitButton === "Do Not Transmit Barcode"){
      setDoNotTransmit("<Do Not Transmit>");
      setTransmitButton("Transmit barcode");
      doNotTransmitBarcode = true;
   }else{
      setDoNotTransmit(null);
      setTransmitButton("Do Not Transmit Barcode");
      doNotTransmitBarcode = false;
   }
   
}
const[showOptionsToDelete, setShowOptionsToDelete] = useState(false);
const [showButtonsOptions, setShowButtonOptions] = useState(true);
const RemoveLastDataEntered = () =>{
   setShowOptionsToDelete(true);
   setShowButtonOptions(false);
}
const RemoveAllDataEntered = () =>{
 
    setPartialStart(0);
    setPartialEnd(0);
    setPartialBarcodeString("");
    setPartialBarcodeSample("");
    SendPartialBarcode = ""; 
 
    setInsertCharStringValue("");
    setAfterPositionValue(0);
     CharacterToInsert = "";
     setInsertCharString("");
     setinsertCharSample("");
 
    setDeleteEndValue(0);
     setDeleteEndString("");
     setDeleteEndSample("");
     deleteEndBarcode = "";

     setStringToReplace("");
     setReplaceString("");
     setDatasustitute("");
     setEffectOnSample("");
     SubstituteDataInBarcode = "";
     SubstituteDataInBarcode2 = "";
     SubstituteDataInBarcode3 = "";
     SubstituteDataInBarcode4 = "";
     SubstituteDataInBarcode5 = "";
     iteration = 0;

 
    setAlertUserString("");
    setAlertUserSample("");
    setAlertuserBeepsValue(0);
    alertUserBarcode = "";

    setShowSendPartial(false);
    setShowInsertChar(false);
    setShowDeleteEnd(false);
    setShowSubstituteOptions(false);
    setShowAlertBeep(false);


   setShowOptionsToDelete(false);
   setShowButtonOptions(true);

}
const[showSendPartial, setShowSendPartial] = useState(false);
const sendPartialHidden = () =>{
    
    if(showSendPartial === true){
        setShowSendPartial(false);
    }else{
        setShowSendPartial(true);
    }
}

const [showInsertChar, setShowInsertChar] = useState(false);
const insertCharHidden = () =>{
    
    if(showInsertChar === true){
        setShowInsertChar(false);
    }else{
        setShowInsertChar(true);
    }
}
const [showDeleteEnd, setShowDeleteEnd] = useState(false);
const deleteEndHidden = () =>{
    if(showDeleteEnd === true){
        setShowDeleteEnd(false);
    }else{
        setShowDeleteEnd(true);
    }
 
 } 

 const [showSubstituteOptions, setShowSubstituteOptions] = useState(false);
const hideSubstituteOptions = () =>{
    
    if(showSubstituteOptions === true){
        setShowSubstituteOptions(false);
    }else{
        setShowSubstituteOptions(true);
    }
 
 } 
 const[showAlertBeep, setShowAlertBeep] = useState(false);
 const alertBeepsHidden = () =>{
    if(showAlertBeep === true){
        setShowAlertBeep(false);
    }else{
        setShowAlertBeep(true);
    }
 
 } 
 const deleteOptionsSelected = () =>{
    
    
    if(option1 === true){
        setPartialStart(0);
        setPartialEnd(0);
        setPartialBarcodeString("");
        setPartialBarcodeSample("");
        SendPartialBarcode = ""; 
        setOption1(false);
    }
    if(option2 === true){
        setInsertCharStringValue("");
        setAfterPositionValue(0);
        CharacterToInsert = "";
        setInsertCharString("");
        setinsertCharSample("");
        setOption2(false);
    }
    if(option3 === true){
        setDeleteEndString("");
        setDeleteEndSample("");
        setDeleteEndValue(0);
        deleteEndBarcode = "";
        setOption3(false);
    }
    if(option4 === true){
        setStringToReplace("");
        setReplaceString("");
        setDatasustitute("");
        setEffectOnSample("");
        SubstituteDataInBarcode = "";
        SubstituteDataInBarcode2 = "";
        SubstituteDataInBarcode3 = "";
        SubstituteDataInBarcode4 = "";
        SubstituteDataInBarcode5 = "";
        iteration = iteration - 1;
        setOption4(false);
    }
    if(option5 === true){
        setAlertUserString("");
        setAlertUserSample("");
        alertUserBarcode = "";
        setOption5(false);
    }
    
    setShowOptionsToDelete(false);
   setShowButtonOptions(true);
 }
 const SPOption = () =>{
    setOption1(!option1); 
 }
 const ICOption = () =>{
    setOption2(!option2);
 }
 const DEOption = () =>{
    setOption3(!option3);
 }
 const SDOption = () =>{
    setOption4(!option4);
 }
 const AUOption = () =>{
    setOption5(!option5);
 }
 const CancelButton = () =>{
    setShowOptionsToDelete(false);
   setShowButtonOptions(true);
 }
        return ( 
            <>
                        <small><p class="text">Operations to Perform on Barcode Data</p></small>


                            <div class="box2">
                                    <div className="dataformattingoptions">
                                        <br/>
                                    {showButtonsOptions &&
                                    <div id="buttonsOptions"> 
                                    <button onClick={sendPartialHidden}><small>Send Partial Barcode</small></button><br/>
                                    {showSendPartial && 
                                    <div id="sendpartialbarcode" className="sendpartialbarcode">
                                    <small>Starting Position:</small><TextField id="startingposition"  inputProps={{min: 0}} variant="outlined"  type="number" value={partialStart} onChange={(e) => sendPartialBarcode(e.target.value)}></TextField>
                                    <small>Ending Position: </small><TextField id="endingposition"  inputProps={{min: 0}} variant="outlined"  type="number" value={partialEnd} onChange={(e) => sendPartialbarcodeEnd(e.target.value)}></TextField><small><small>Set as 0 to send the rest of the barcode.</small></small>
                                    </div>
                                    }
                                    <button onClick={insertCharHidden}><small>Insert Character in Barcode</small></button>
                                    {showInsertChar &&
                                    <div id="InsertCharacterOptions">
                                    <TextField className="MatchButtons" id="InsertCharTextfield" label="Insert Character in Barcode" variant="outlined" value={insertCharStringValue} onChange={(e) => insertChar(e.target.value)}/>
                                    <div class="divSmallButtonsDP">
                                    <button className="smallButtons" onClick={commModeHidden}><small><small>Communication Mode</small></small></button>&nbsp;{showKeyboardCharButton && <button id='keyboardCharsButton' className="smallButtons" onClick={KeyboardChars2}><small><small>keyboard Chars</small></small></button>}&nbsp;{showNonPrintableButton && <button id='nonPrintableButton' className="smallButtons" onClick={nonPrintableChars}><small><small>Non-printable Chars</small></small></button>}
                                    </div>
                                    {showKeyboardChars &&
                                    <div id="KeyboardChars2" className="KeyboardChars">
                                        <table>
                                        
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={UpArrowChar}><small><small>Up arrow</small></small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={LeftArrowChar}><small><small>Left Arrow</small></small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={RightArrowChar}><small><small>Right Arrow</small></small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={DownArrowChar}><small><small>Down Arrow</small></small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={TabChar}><small><small>Tab</small></small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={DeleteChar}><small><small>Delete</small></small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={EscChar}><small><small>Esc</small></small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={EnterChar}><small><small>Enter</small></small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={EndChar}><small><small>End</small></small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={BackspaceChar}><small><small>Backspace</small></small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={InsertChar}><small><small>Insert</small></small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={PageUpChar}><small><small>Page Up</small></small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={PageDownChar}><small><small>Page Down</small></small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={HomeChar}><small><small>Home</small></small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Number0Char}><small>0</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Number1Char}><small>1</small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Number2Char}><small>2</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Number3Char}><small>3</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Number4Char}><small>4</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Number5Char}><small>5</small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Number6Char}><small>6</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Number7Char}><small>7</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Number8Char}><small>8</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={Number9Char}><small>9</small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={F1Char}><small>F1</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={F2Char}><small>F2</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={F3Char}><small>F3</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={F4Char}><small>F4</small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={F5Char}><small>F5</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={F6Char}><small>F6</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={F7Char}><small>F7</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={F8Char}><small>F8</small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={F9Char}><small>F9</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={F10Char}><small>F10</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={F11Char}><small>F11</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={F12Char}><small>F12</small></button>
                                                        </td>
                                                    </tr>
                                          
                                            </table>
                                       
                                    </div>
                                    }
                                    {showNonPrintableChars &&
                                    <div id="NonPrintable" className="NonPrintable">
                                        <table>
                                            <tbody>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={NULL}><small>NULL</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={SOH}><small>SOH</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={STX}><small>STX</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={ETX}><small>ETX</small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={EOT}><small>EOT</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={ENQ}><small>ENQ</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={ACK}><small>ACK</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={BEL}><small>BEL</small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={BS}><small>BS</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={TAB}><small>TAB</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={LF}><small>LF/NL</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={VT}><small>VT</small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={FF}><small>FF/NP</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={CR}><small>CR</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={SO}><small>SO</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={SI}><small>SI</small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={DLE}><small>DLE</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={DC1}><small>DC1</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={DC2}><small>DC2</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={DC3}><small>DC3</small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={DC4}><small>DC4</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={NAK}><small>NAK</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={SYN}><small>SYN</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={ETB}><small>ETB</small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={CAN}><small>CAN</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={EM}><small>EM</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={SUB}><small>SUB</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={ESC}><small>ESC</small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={FS}><small>FS</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={GS}><small>GS</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={RS}><small>RS</small></button>
                                                        </td>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={US}><small>US</small></button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <button class="smallButtonsChars" onClick={DEL}><small>DEL</small></button>
                                                        </td>
                                                       
                                                    </tr>
                                                   
                                            </tbody>        
                                        </table>

                                    </div>   
                                    }       
                                    {showCommunicationMode &&                
                                    <div id="CommunicationModes" className="KeyboardChars">
                                    <Multiselect id="SingleMultiSelect"
                                    isObject={false}
                                    options={CommuniOptions} 
                                    onSelect={commMode}
                                    singleSelect
                                    />
                                    </div>
                                    }
                                    <div className="afterpositionlabel">
                                    <small>After Position:</small><TextField id="AfterPositionChar" inputProps={{min: 0}} variant="outlined"  type="number" value={afterPositionValue} onChange={(e) => afterPositionFunction(e.target.value)}></TextField>
                                    </div>
                                    </div>
                                    }
                                    <br/>
                                    <button onClick={deleteEndHidden}><small>Delete End of Barcode</small></button><br/>
                                    {showDeleteEnd &&
                                    <div id="deleteendofbarcode">
                                    <small>Number of chars to remove from end of barcode:</small><TextField id="deleteend" inputProps={{min: 0}} variant="outlined"  type="number" value={deleteEndValue} onChange={(e) => deleteEndOfBarcode(e.target.value)}></TextField><br/><br/>
                                    </div>
                                    }
                                    <button onClick={hideSubstituteOptions}><small>Substitute Data in Barcode</small></button><br/>
                                    {showSubstituteOptions &&
                                    <div id="substituteLabel">
                                    <TextField className="MatchButtons" id="stringtoreplace" label="String To Replace" variant="outlined" value={stringToReplace} onChange={(e) => SubstituteData(e.target.value)}/>
                                    <br/><br/><TextField className="MatchButtons" id="replacement" label="Replacement" variant="outlined" value={replaceString} onChange={(e) => Replacement(e.target.value)}/><br/><br/>
                                    </div>
                                    }
                                    <button onClick={alertBeepsHidden}><small>Alert the User</small></button><br/>
                                    {showAlertBeep &&
                                    <div id="alertbeepslabel">
                                    <small>Enter number of alert beeps &#40;min 1, max 10&#41;:</small><TextField id="alertbeeps" inputProps={{min: 0}} variant="outlined"  type="number" value={alertUserBeepsValue} onChange={(e) => alertUser(e.target.value)}></TextField><br/><br/>
                                    </div>
                                    }
                                    <button onClick={DoNotTransmit}><small>{transmitButton}</small></button><br/>
                                    
                                    
                                    </div>
                                    }
                                    {showOptionsToDelete &&
                                    <div id="optionsToDelete" className="deleteoptions">
                                        <small>Delete the following parameters: </small><br/><br/>
                                    <input type="checkbox"  value="sendPartial" onChange={SPOption} checked={option1}/> Send Partial Barcode
                                    <br/><br/><br/>
                                    <input type="checkbox" value="insertChar" onChange={ICOption} checked={option2}/> Insert Character in Barcode
                                    <br/><br/><br/>
                                    <input type="checkbox" value="deleteEnd" onChange={DEOption} checked={option3}/> Delete End of Barcode
                                    <br/><br/><br/>
                                    <input type="checkbox" value="substituteData" onChange={SDOption} checked={option4}/> Substitute Data in Barcode
                                    <br/><br/><br/>
                                    <input type="checkbox" value="alertUser" onChange={AUOption}  checked={option5}/> Alert The User
                                    <br/><br/><br/>

                                        <div class="cancelButtons">
                                        <Button variant="outlined" color="primary" onClick={CancelButton}>Cancel</Button>&nbsp;&nbsp; <Button variant="outlined" color="primary" onClick={deleteOptionsSelected}>Confirm</Button>
                                       
                                        </div>
                                        <br/>
                                    </div>
                                    }
                                    </div>
                                    
                                    <div class="imagebuttons">
                                    </div>
                                    <div class="databox">
                                    <small><p class="text">Chosen Operations</p></small>
                                                <small>{partialBarcodeString}</small>
                                                <small>{insertCharString}</small>
                                                <small>{deleteEndString}</small>                                               
                                                <small>{Datasubstitute}</small>
                                                <small>{alertUserString}</small>
                                                <small>{doNotTransmit}</small>
                                    </div>
                                    <div class="databox">
                                    <small><p class="text">Effect on Sample if Matched</p></small>
                                                    <small>{partialBarcodeSample}</small>
                                                    <small>{insertCharSample}</small>
                                                    <small>{deleteEndSample}</small>
                                                    <small>{effectOnSample}</small>
                                                   <small>{alertUserSample}</small>
                                                   <small>{doNotTransmit}</small>
                                                   
                                    </div>
                                    
                                    <div class="buttonsdownMatch">
                            <div><input type="image" src={deletepic} alt='delete' height="40px" width="40px" title="Remove all Data Formatting Output Operations." onClick={RemoveAllDataEntered}/></div>
                            <div><input type="image" src={cancelpic} alt='cancel' height="40px" width="40px" title="Remove Last Data Formatting Operation Entered." onClick={RemoveLastDataEntered}/></div>
                            
                            </div>
                            </div>
            </>
        );
    
}
 
export default OperationsToPerform;