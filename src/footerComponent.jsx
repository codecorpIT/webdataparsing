
import React, { Component } from 'react';

class Footer extends Component {
    
    state = {  }
    render() { 
        return ( 
			<footer id="footer" class="full-width-section">
				<div class="container text-white">
					<div class="footer-ribbon">
						<span>Get in Touch</span>
					</div>
					<div class="row py-5 my-4">
						<div class="col-md-6 col-lg-4 mb-4 mb-lg-0">
							<h5 class="text-3 mb-3">NEWSLETTER</h5>
							<p class="pr-1">Stay current on our always evolving product features and technology. Enter your email address and subscribe to our short once weekly newsletter.</p>
							<div class="alert alert-success d-none" id="newsletterSuccess">
								<strong>Success!</strong> You've been added to our email list.
							</div>
							<div class="alert alert-danger d-none" id="newsletterError"></div>
							

							<a href="https://www.getdrip.com/forms/945330930/submissions/new" data-drip-show-form="945330930" class="btn btn-modern btn-danger mt-1" >SUBSCRIBE NOW!</a>

						</div>
						<div class="col-md-6 col-lg-3 mb-4 mb-lg-0">

							<h5 class="text-3 mb-3">NEWS</h5>
							<div id="" class="text-color-light">
								<p>
									<a class="text-color-light" href="https://codecorp.com/about/blog" title="Our Blog" >Our Blog</a><br />
									<a class="text-color-light" href="https://codecorp.com/about/press" title="Press Releases" >Press Releases</a><br />
								</p>
							</div>
								<a href="https://codecorp.com/about" title="About Us" >
								<h5 class="text-3 mb-3">About Us</h5>
							</a>
								<a href="https://codecorp.com/about/careers" title="Careers" >
								<h5 class="text-3 mb-3">Careers</h5>
							</a>
						
						</div>
						<div class="col-md-6 col-lg-3 mb-4 mb-md-0">
							<div class="contact-details">
								<a href="https://codecorp.com/contact" ><h5 class="text-3 mb-3">CONTACT US</h5></a>
								<ul class="list list-icons list-icons-lg">
									<li class="mb-1"><i class="fas fa-location-arrow"></i><a class=" text-color-light" href="https://g.page/codecorp?share" target="_blank" >434 West Ascension Way, Ste. 300<br />Murray UT 84123</a></li>
									<li class="mb-1"><i class="fas fa-phone"></i><p class="m-0"><a class=" text-color-light" href="tel:8014952200" >+01 801-495-2200</a></p></li>
									<li class="mb-1"><i class="fas fa-fax"></i><p class="m-0"><a class=" text-color-light" href="tel:8014952202" > +01 801-495-2202</a></p></li>
									<li class="mb-1"><i class="far fa-envelope"></i><p class="m-0"><a class=" text-color-light" href="mailto:info@codecorp.com" >info@codecorp.com</a></p></li>
								</ul>
							</div>
						</div>
						<div class="col-md-6 col-lg-2">
							<h5 class="text-3 mb-3">FOLLOW US</h5>
							<ul class="social-icons">
								<li class="social-icons-facebook"><a href="https://www.facebook.com/code411" target="_blank" title="Facebook" ><i class="fab fa-facebook-f"></i></a></li>
								<li class="social-icons-twitter"><a href="https://twitter.com/codecorp" target="_blank" title="Twitter" ><i class="fab fa-twitter"></i></a></li>
								<li class="social-icons-linkedin"><a href="https://www.linkedin.com/company/code-corporation" target="_blank" title="Linkedin" ><i class="fab fa-linkedin-in"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="footer-copyright">
					<div class="container py-2">
						<div class="row py-4" itemProp="publisher" itemScope="" itemType="https://schema.org/Organization">
							<meta itemProp="name" content="Code Corporation" />
							<div class="col-lg-1 d-flex align-items-center justify-content-center justify-content-lg-start mb-2 mb-lg-0" itemProp="logo" itemScope="" itemType="https://schema.org/ImageObject">
								<meta itemProp="url" content="https://codecorp.com/site/img/logo.png" />
								<meta itemProp="width" content="320" />
								<meta itemProp="height" content="60" />

								<a href="https://codecorp.com" class="logo pr-0 pr-lg-3" >
									<img alt="Code Logo" src="https://codecorp.com/site/img/code-logo-white.png" class="opacity-5" height="20" />
								</a>
							</div>
							<div class="col-lg-7 d-flex align-items-center justify-content-center justify-content-lg-start mb-4 mb-lg-0">
								<p>    © Copyright 2020. All Rights Reserved.</p>

							</div>

							<div class="col-lg-4 d-flex align-items-center justify-content-center justify-content-lg-end text-color-light text-white">
							<nav id="sub-menu">
								<ul>
											<li><i class="fas fa-angle-right"></i> &nbsp; <a href="https://codecorp.com/support/privacy" title="Privacy Policy" class="ml-1 text-decoration-none text-color-light" >Privacy Policy</a></li>
											<li><i class="fas fa-angle-right"></i> &nbsp; <a href="https://codecorp.com/support/eula" title="EULA" class="ml-1 text-decoration-none text-color-light" >EULA</a></li>
											<li><i class="fas fa-angle-right"></i> &nbsp; <a href="https://codecorp.com/contact" title="Contact Us" class="ml-1 text-decoration-none text-color-light" >Contact Us</a></li>
										</ul>
							</nav>      
						</div>
					</div>
				</div>
			</div>

		</footer>

)};

}
 
export default Footer;
