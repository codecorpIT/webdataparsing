import React, {useState, useRef} from 'react';
import axios from 'axios';
import Moment from 'moment';
import crc from 'crc';
import {Button} from '@material-ui/core';
import { useReactToPrint } from 'react-to-print';
import {SymbologiesSelectedByUser, DataLenghtEqualToSelectedByUser, DataLenghtNotEqualToSelectedByUser, 
    DLGreaterThanANDLessThanSelectedByUser,DataLenghtGreaterThanSelectedByUser, DLNotEqualANDGreaterThanSelectedByUser, 
    DataLenghtLessThanSelectedByUser, DLNotEqualANDLessThanSelectedByUser, DataLenghtGreaterThanEqualToSelectedByUser, 
    DLNotEqualANDGreaterThanEqualToSelectedByUser, DataLenghtLessThanEqualToSelectedByUser, 
    DLNotEqualANDLessThanEqualToSelectedByUser, DLNotEqualANDGreaterThanANDLessThanSelectedByUser, DLNotEqualANDGreaterThanEqualToANDLessThanEqualToSelectedByUser, DLGreaterThanEqualToANDLessThanEqualToSelectedByUser,
    BarcodeDataStartsWith, BarcodeDataContains, BarcodeDataEndsWith, BarcodeDataStartsANDEnds, BarcodeDataStartANDContains, 
    BarcodeDataContainsANDEnds, BarcodeDataStartsANDContainsANDEnds, AllOptionsSelectedConditions} from './ConditionsToMatchComponent';
import {SendPartialBarcodeSelected, InsertCharInBarcode, DeleteEnd, Substitute1, Substitute2, Substitute3, Substitute4, Substitute5,
        Alert, Transmit, AllOptionsSelected} from './OperationsToPerformOnBarcodeComponent';



function DPGetBarcodeRulesCreated() {
    const [imageBarcode, setImageBarcode] = useState('');
    const [optionsSelected, setOptionsSelected] = useState('');
    const [optionsSelected2, setOptionsSelected2] = useState('');
    const [showDownloadButton, setShowDownloadButton] = useState(false);
    const buttonClick = () => {
    var SymbologiesReceive = SymbologiesSelectedByUser();
    var count = 0;
    var DataToPerformSelected = false;
    let header = "\x01Y\x1d\x02";
   let split = "\x03";
   let footer = "\x03\x04";
    var formatCmd = "RDFSXFM2";
    var BarcodeStringFinal = "";
    var SymbologieCodeString = "";
    var SymbologieCodeFinal = "";
    var BarcodeLenghtFinal = "";
    var BarcodeCodeLenghtFinal = "";
    var BarcodeDataFinal = "";
    var SendPartial = "";
    var InsertCharacter = "";
    var BarcodeEndDeleted = "";
    var SubstituteCode1 = "";
    var SubstituteCode2 = "";
    var SubstituteCode3 = "";
    var SubstituteCode4 = "";
    var SubstituteCode5 = "";
    var SubstituteCodeFinal = "";
    var AlertString = "";
    var DoNotTransmitString = "";
    var BarcodeDataCodeFinal = "";
    var BarcodelenghtEqualTo = DataLenghtEqualToSelectedByUser(); //receive an individual string with the number that the user entered
    var BarcodelenghtNotEqualTo = DataLenghtNotEqualToSelectedByUser(); //receive an individual string with the number that the user entered
    var BarcodelenghtGreaterThan = DataLenghtGreaterThanSelectedByUser(); //receive an individual string with the number that the user entered
    var BarcodelenghtLessThan = DataLenghtLessThanSelectedByUser(); //receive an individual string with the number that the user entered
    var BarcodelenghtGreaterThanEqualTo = DataLenghtGreaterThanEqualToSelectedByUser(); //receive an individual string with the number that the user entered
    var BarcodelenghtLessThanEqualTo = DataLenghtLessThanEqualToSelectedByUser();
    var BLGreaterANDLessthan = DLGreaterThanANDLessThanSelectedByUser(); //receive an Array with all numbers that user entered
    var BLGreaterEqualANDLessEqual = DLGreaterThanEqualToANDLessThanEqualToSelectedByUser(); //receive an Array with all numbers that user entered
    var BLNotEqualANDGreaterThan = DLNotEqualANDGreaterThanSelectedByUser(); //receive an Array with all numbers that user entered
    var BLNotEqualANDLessThan = DLNotEqualANDLessThanSelectedByUser(); //receive an Array with all numbers that user entered
    var BLNotEqualANDGreaterthanEqualTo = DLNotEqualANDGreaterThanEqualToSelectedByUser(); //receive an Array with all numbers that user entered
    var BLNotEqualANDLessThanEqualTo = DLNotEqualANDLessThanEqualToSelectedByUser(); //receive an Array with all numbers that user entered
    var BLNotEqualANDGreaterThanANDLessThan = DLNotEqualANDGreaterThanANDLessThanSelectedByUser(); //receive an Array with all numbers that user entered
    var BLNotEqualANDGreaterEqualtoANDLessEqualTo = DLNotEqualANDGreaterThanEqualToANDLessThanEqualToSelectedByUser(); 
    var BarcodeDataStartsString = BarcodeDataStartsWith();
    var BarcodeDataContainsString = BarcodeDataContains();
    var BarcodeDataEndsString = BarcodeDataEndsWith();
    var BarcodeDataStartsANDEndsString = BarcodeDataStartsANDEnds();
    var BarcodeDataStartsANDContainsString = BarcodeDataStartANDContains();
    var BarcodeDataContainsANDEndsString = BarcodeDataContainsANDEnds();
    var BarcodeDataStartsANDContainsANDEndsString = BarcodeDataStartsANDContainsANDEnds();
    var sendPartialBcode = SendPartialBarcodeSelected();
    var CharToInsertBcode = InsertCharInBarcode();
    var deleteEndOfBarcode = DeleteEnd();
    console.log(deleteEndOfBarcode);
    var AlertUser = Alert();
    var DoNotTransmit = Transmit();
    var SubtituteData1 = Substitute1();
    var SubtituteData2 = Substitute2();
    var SubtituteData3 = Substitute3();
    var SubtituteData4 = Substitute4();
    var SubtituteData5 = Substitute5();
    var optionsSelectedByUser = AllOptionsSelected();
    var optionsSelectedByUser2 = AllOptionsSelectedConditions();
    setOptionsSelected2(optionsSelectedByUser2);
    setOptionsSelected(optionsSelectedByUser);
    var headerForReplace = "\x01" + "\x1ean/";
    var footerForReplace = "\x04";
    function MetaCharReplaced(string) {
        if(string.includes("<NULL>")){
            string = string.replace(/<NULL>/g, "%00");
        }
        if(string.includes("<SOH>")){
            string = string.replace(/<SOH>/g, "%01");
         }
         if(string.includes("<STX>")){
            string = string.replace(/<STX>/g, "%02");
         }
         if(string.includes("<ETX>")){
            string = string.replace(/<ETX>/g, "%03");
         }
         if(string.includes("<EOT>")){
            string = string.replace(/<EOT>/g, "%04");
         }
         if(string.includes("<ENQ>")){
            string = string.replace(/<ENQ>/g, "%05");
         }
         if(string.includes("<ACK>")){
            string = string.replace(/<ACK>/g, "%06");
         }
         if(string.includes("<BEL>")){
            string = string.replace(/<BEL>/g, "%07");
         }
         if(string.includes("<BS>")){
            string = string.replace(/<BS>/g, "%08");
         }
         if(string.includes("<TAB>")){
            string = string.replace(/<TAB>/g, "%09");
         }
         if(string.includes("<LF-NL>")){
            string = string.replace(/<LF-NL>/g, "%0A");
         }
         if(string.includes("<VT>")){
            string = string.replace(/<VT>/g, "%0B");
         }
         if(string.includes("<FF-NP>")){
            string = string.replace(/<FF-NP>/g, "%0C");
         }
         if(string.includes("<CR>")){
            string = string.replace(/<CR>/g, "%0D");
         }
         if(string.includes("<SO>")){
            string = string.replace(/<SO>/g, "%0E");
         }
         if(string.includes("<SI>")){
            string = string.replace(/<SI>/g, "%0F");
         }
         if(string.includes("<DLE>")){
            string = string.replace(/<DLE>/g, "%10");
         }
         if(string.includes("<DC1>")){
            string = string.replace(/<DC1>/g, "%11");
         }
         if(string.includes("<DC2>")){
            string = string.replace(/<DC2>/g, "%12");
         }
         if(string.includes("<DC3>")){
            string = string.replace(/<DC3>/g, "%13");
         }
         if(string.includes("<DC4>")){
            string = string.replace(/<DC4>/g, "%14");
         }
         if(string.includes("<NAK>")){
            string = string.replace(/<NAK>/g, "%15");
         }
         if(string.includes("<SYN>")){
            string = string.replace(/<SYN>/g, "%16");
         }
         if(string.includes("<ETB>")){
            string = string.replace(/<ETB>/g, "%17");
         }
         if(string.includes("<CAN>")){
            string = string.replace(/<CAN>/g, "%18");
         }
         if(string.includes("<EM>")){
            string = string.replace(/<EM>/g, "%19");
         }
         if(string.includes("<SUB>")){
            string = string.replace(/<SUB>/g, "%1A");
         }
         if(string.includes("<ESC>")){
            string = string.replace(/<ESC>/g, "%1B");
         }
         if(string.includes("<FS>")){
            string = string.replace(/<FS>/g, "%1C");
         }
         if(string.includes("<GS>")){
            string = string.replace(/<GS>/g, "%1D");
         }
         if(string.includes("<RS>")){
            string = string.replace(/<RS>/g, "%1E");
         }
         if(string.includes("<US>")){
            string = string.replace(/<US>/g, "%1F");
         }
         if(string.includes("<DEL>")){
            string = string.replace(/<DEL>/g, "%7F");
         }           
        
         if(string.includes("<Up Arrow-Keyboard>")){
            string = string.replace(/<Up Arrow-Keyboard>/g, headerForReplace + "/u" + footerForReplace);
         } 
         if(string.includes("<Left Arrow-Keyboard>")){
            string = string.replace(/<Left Arrow-Keyboard>/g, headerForReplace + "/l" + footerForReplace);
         }   
         if(string.includes("<Right Arrow-Keyboard>")){
            string = string.replace(/<Right Arrow-Keyboard>/g, headerForReplace + "/r" + footerForReplace);
         }   
         if(string.includes("<Down Arrow-Keyboard>")){
            string = string.replace(/<Down Arrow-Keyboard>/g, headerForReplace + "/d" + footerForReplace);
         }   
         if(string.includes("<Tab-Keyboard>")){
            string = string.replace(/<Tab-Keyboard>/g, headerForReplace + "/t" + footerForReplace);
         }   
         if(string.includes("<Delete-Keyboard>")){
            string = string.replace(/<Delete-Keyboard>/g, headerForReplace + "/z" + footerForReplace);
         }   
         if(string.includes("<Esc-Keyboard>")){
            string = string.replace(/<Esc-Keyboard>/g, headerForReplace + "/e" + footerForReplace);
         }   
         if(string.includes("<Enter-Keyboard>")){
            string = string.replace(/<Enter-Keyboard>/g, headerForReplace + "/n" + footerForReplace);
         }   
         if(string.includes("<End-Keyboard>")){
            string = string.replace(/<End-Keyboard>/g, headerForReplace + "/v" + footerForReplace);
         }   
         if(string.includes("<Backspace-Keyboard>")){
            string = string.replace(/<Backspace-Keyboard>/g, headerForReplace + "/b" + footerForReplace);
         }   
         if(string.includes("<Insert-Keyboard>")){
            string = string.replace(/<Insert-Keyboard>/g,headerForReplace + "/i" + footerForReplace);
         }   
         if(string.includes("<Page Up-Keyboard>")){
            string = string.replace(/<Page Up-Keyboard>/g, headerForReplace + "/p" + footerForReplace);
         }   
         if(string.includes("<Page Down-Keyboard>")){
            string = string.replace(/<Page Down-Keyboard>/g, headerForReplace + "/x" + footerForReplace);
         }   
         if(string.includes("<Home-Keyboard>")){
            string = string.replace(/<Home-Keyboard>/g, headerForReplace + "/h" + footerForReplace);
         }    
         if(string.includes("<0>")){
            string = string.replace(/<0>/g, headerForReplace + "/0" + footerForReplace);
         }   
         if(string.includes("<1>")){
            string = string.replace(/<1>/g, headerForReplace + "/1" + footerForReplace);
         }   
         if(string.includes("<2>")){
            string = string.replace(/<2>/g, headerForReplace + "/2" + footerForReplace);
         }   
         if(string.includes("<3>")){
            string = string.replace(/<3>/g, headerForReplace + "/3" + footerForReplace);
         }   
         if(string.includes("<4>")){
            string = string.replace(/<4>/g, headerForReplace + "/4" + footerForReplace);
         }   
         if(string.includes("<5>")){
            string = string.replace(/<5>/g, headerForReplace + "/5" + footerForReplace);
         }   
         if(string.includes("<6>")){
            string = string.replace(/<6>/g, headerForReplace + "/6" + footerForReplace);
         } 
         if(string.includes("<7>")){
            string = string.replace(/<7>/g, headerForReplace + "/7" + footerForReplace);
         } 
         if(string.includes("<8>")){
            string = string.replace(/<8>/g, headerForReplace + "/8" + footerForReplace);
         } 
         if(string.includes("<9>")){
            string = string.replace(/<9>/g, headerForReplace + "/9" + footerForReplace);
         } 
         if(string.includes("<F1-Keyboard>")){
            string = string.replace(/<F1-Keyboard>/g, headerForReplace + "/f1" + footerForReplace);
         }   
         if(string.includes("<F2-Keyboard>")){
            string = string.replace(/<F2-Keyboard>/g, headerForReplace + "/f2" + footerForReplace);
         }   
         if(string.includes("<F3-Keyboard>")){
            string = string.replace(/<F3-Keyboard>/g, headerForReplace + "/f3" + footerForReplace);
         }   
         if(string.includes("<F4-Keyboard>")){
            string = string.replace(/<F4-Keyboard>/g, headerForReplace + "/f4" + footerForReplace);
         }   
         if(string.includes("<F5-Keyboard>")){
            string = string.replace(/<F5-Keyboard>/g, headerForReplace + "/f5" + footerForReplace);
         }   
         if(string.includes("<F6-Keyboard>")){
            string = string.replace(/<F6-Keyboard>/g, headerForReplace + "/f6" + footerForReplace);
         }   
         if(string.includes("<F7-Keyboard>")){
            string = string.replace(/<F7-Keyboard>/g, headerForReplace + "/f7" + footerForReplace);
         }   
         if(string.includes("<F8-Keyboard>")){
            string = string.replace(/<F8-Keyboard>/g, headerForReplace + "/f8" + footerForReplace);
         }   
         if(string.includes("<F9-Keyboard>")){
            string = string.replace(/<F9-Keyboard>/g, headerForReplace + "/f9" + footerForReplace);
         }   
         if(string.includes("<F10-Keyboard>")){
            string = string.replace(/<F10-Keyboard>/g, headerForReplace + "/f10" + footerForReplace);
         }   
         if(string.includes("<F11-Keyboard>")){
            string = string.replace(/<F11-Keyboard>/g, headerForReplace + "/f11" + footerForReplace);
         }   
         if(string.includes("<F12-Keyboard>")){
            string = string.replace(/<F12-Keyboard>/g, headerForReplace + "/f12" + footerForReplace);
         }   

         return string;
    }

        // SYMBOLOGIES BARCODE MUST MATCH
    if(SymbologiesReceive.includes("Australian Post")){
        if(SymbologieCodeString !== ""){
            SymbologieCodeString = SymbologieCodeString + "||(a.symbology==29&&a.symbologyModifier==97)"  
        }else{
            SymbologieCodeString = "(a.symbology==29&&a.symbologyModifier==97)"  
        }
       
    }
    if(SymbologiesReceive.includes("Aztec")){
        if(SymbologieCodeString !== ""){
            SymbologieCodeString =SymbologieCodeString +  "||(a.symbology==30&&a.symbologyModifier==48)"  
        }else{
            SymbologieCodeString = "(a.symbology==30&&a.symbologyModifier==48)"  
        }    }
    if(SymbologiesReceive.includes("BC412")){ 
        if(SymbologieCodeString !== ""){
            SymbologieCodeString = SymbologieCodeString + "||(a.symbology==59&&a.symbologyModifier==66)"  
        }else{
            SymbologieCodeString = "(a.symbology==59&&a.symbologyModifier==66)"  
        }    }
    if(SymbologiesReceive.includes("Canada Post")){ 
        if(SymbologieCodeString !== ""){
            SymbologieCodeString = SymbologieCodeString + "||(a.symbology==71&&a.symbologyModifier==99)"  
        }else{
            SymbologieCodeString = "(a.symbology==71&&a.symbologyModifier==99)"  
        }    }
    if(SymbologiesReceive.includes("Codabar")){
        if(SymbologieCodeString !== ""){
            SymbologieCodeString = SymbologieCodeString + "||(a.symbology==20&&a.symbologyModifier==48)"  
        }else{
            SymbologieCodeString = "(a.symbology==20&&a.symbologyModifier==48)"  
        }    }
    if(SymbologiesReceive.includes("Codablock F")){
        if(SymbologieCodeString !== ""){
            SymbologieCodeString = SymbologieCodeString + "||(a.symbology==50&&a.symbologyModifier==52)"  
        }else{
            SymbologieCodeString = "(a.symbology==50&&a.symbologyModifier==52)"  
        }    }
    if(SymbologiesReceive.includes("Code 11")){
        if(SymbologieCodeString !== ""){
            SymbologieCodeString = SymbologieCodeString + "||(a.symbology==51&&a.symbologyModifier==48)"  
        }else{
            SymbologieCodeString = "(a.symbology==51&&a.symbologyModifier==48)"  
        }    }
    if(SymbologiesReceive.includes("Code 32")){ 
        if(SymbologieCodeString !== ""){
            SymbologieCodeString = SymbologieCodeString + "||(a.symbology==65&&a.symbologyModifier==108)"  
        }else{
            SymbologieCodeString = "(a.symbology==65&&a.symbologyModifier==108)"  
        }    }
    if(SymbologiesReceive.includes("Code 39")){
        if(SymbologieCodeString !== ""){
            SymbologieCodeString = SymbologieCodeString + "||(a.symbology==18&&a.symbologyModifier==48)"  
        }else{
            SymbologieCodeString = "(a.symbology==18&&a.symbologyModifier==48)"  
        }    }
    if(SymbologiesReceive.includes("Code 49")){ 
        if(SymbologieCodeString !== ""){
            SymbologieCodeString = SymbologieCodeString + "||(a.symbology==72&&a.symbologyModifier==48)"  
        }else{
            SymbologieCodeString = "(a.symbology==72&&a.symbologyModifier==48)"  
        }    }
    if(SymbologiesReceive.includes("Code 93")){ 
        if(SymbologieCodeString !== ""){
            SymbologieCodeString = SymbologieCodeString + "||(a.symbology==21&&a.symbologyModifier==48)"  
        }else{
            SymbologieCodeString = "(a.symbology==21&&a.symbologyModifier==48)"  
        }    }
    if(SymbologiesReceive.includes("Code 128")){
        if(SymbologieCodeString !== ""){
            SymbologieCodeString = SymbologieCodeString + "||(a.symbology==19&&a.symbologyModifier==48)"  
        }else{
            SymbologieCodeString = "(a.symbology==19&&a.symbologyModifier==48)"  
        }    }
    if(SymbologiesReceive.includes("Composite Code A")){ 
        if(SymbologieCodeString !== ""){
            SymbologieCodeString = SymbologieCodeString + "||(a.symbology==62&&a.symbologyModifier==48)"  
        }else{
            SymbologieCodeString = "(a.symbology==62&&a.symbologyModifier==48)"  
        }    }
    if(SymbologiesReceive.includes("Composite Code B")){ 
        if(SymbologieCodeString !== ""){
            SymbologieCodeString = SymbologieCodeString + "||(a.symbology==63&&a.symbologyModifier==48)"  
        }else{
            SymbologieCodeString = "(a.symbology==63&&a.symbologyModifier==48)"  
        }    }
    if(SymbologiesReceive.includes("Composite Code C")){ 
        if(SymbologieCodeString !== ""){
            SymbologieCodeString = SymbologieCodeString + "||(a.symbology==64&&a.symbologyModifier==48)"  
        }else{
            SymbologieCodeString = "(a.symbology==64&&a.symbologyModifier==48)"  
        }     }
    if(SymbologiesReceive.includes("Data Matrix")){
        if(SymbologieCodeString !== ""){
            SymbologieCodeString = SymbologieCodeString + "||(a.symbology==31&&a.symbologyModifier==49)"  
        }else{
            SymbologieCodeString = "(a.symbology==31&&a.symbologyModifier==49)"  
        }     }
    if(SymbologiesReceive.includes("Dutch Post (KIX)")){
        if(SymbologieCodeString !== ""){
            SymbologieCodeString = SymbologieCodeString + "||(a.symbology==35&&a.symbologyModifier==100)"  
        }else{
            SymbologieCodeString = "(a.symbology==35&&a.symbologyModifier==100)"  
        }     }
    if(SymbologiesReceive.includes("EAN-13")){
        if(SymbologieCodeString !== ""){
            SymbologieCodeString = SymbologieCodeString + "||(a.symbology==49&&a.symbologyModifier==69)"  
        }else{
            SymbologieCodeString = "(a.symbology==49&&a.symbologyModifier==69)"  
        }     }
    if(SymbologiesReceive.includes("EAN-8")){
        if(SymbologieCodeString !== ""){
            SymbologieCodeString = SymbologieCodeString + "||(a.symbology==49&&a.symbologyModifier==68)"  
        }else{
            SymbologieCodeString = "(a.symbology==49&&a.symbologyModifier==68)"  
        }     }
    if(SymbologiesReceive.includes("GoCode")){ 
        if(SymbologieCodeString !== ""){
            SymbologieCodeString = SymbologieCodeString + "||(a.symbology==48&&a.symbologyModifier==71)"  
        }else{
            SymbologieCodeString = "(a.symbology==48&&a.symbologyModifier==71)"  
        }     }
    if(SymbologiesReceive.includes("Grid Matrix")){ 
        if(SymbologieCodeString !== ""){
            SymbologieCodeString = SymbologieCodeString + "||(a.symbology==73&&a.symbologyModifier==103)"  
        }else{
            SymbologieCodeString = "(a.symbology==73&&a.symbologyModifier==103)"  
        }     }
    if(SymbologiesReceive.includes("GS1 Databar")){
        if(SymbologieCodeString !== ""){
            SymbologieCodeString = SymbologieCodeString + "||(a.symbology==46&&a.symbologyModifier==48)"  
        }else{
            SymbologieCodeString = "(a.symbology==46&&a.symbologyModifier==48)"  
        }     }
    if(SymbologiesReceive.includes("GS1 Databar Expanded")){
        if(SymbologieCodeString !== ""){
            SymbologieCodeString = SymbologieCodeString + "||(a.symbology==43&&a.symbologyModifier==48)"  
        }else{
            SymbologieCodeString = "(a.symbology==43&&a.symbologyModifier==48)"  
        }     }
    if(SymbologiesReceive.includes("GS1 Databar Expanded Stacked")){ 
        if(SymbologieCodeString !== ""){
            SymbologieCodeString = SymbologieCodeString + "||(a.symbology==44&&a.symbologyModifier==48)"  
        }else{
            SymbologieCodeString = "(a.symbology==44&&a.symbologyModifier==48)"  
        }     }
    if(SymbologiesReceive.includes("GS1 Databar Limited")){
        if(SymbologieCodeString !== ""){
            SymbologieCodeString = SymbologieCodeString + "||(a.symbology==45&&a.symbologyModifier==48)"  
        }else{
            SymbologieCodeString = "(a.symbology==45&&a.symbologyModifier==48)"  
        }     }
    if(SymbologiesReceive.includes("GS1 Databar Stacked")){
        if(SymbologieCodeString !== ""){
            SymbologieCodeString = SymbologieCodeString + "||(a.symbology==47&&a.symbologyModifier==48)"  
        }else{
            SymbologieCodeString = "(a.symbology==47&&a.symbologyModifier==48)"  
        }     }
    if(SymbologiesReceive.includes("HanXin")){
        if(SymbologieCodeString !== ""){
            SymbologieCodeString = SymbologieCodeString + "||(a.symbology==61&&a.symbologyModifier==72)"  
        }else{
            SymbologieCodeString = "(a.symbology==61&&a.symbologyModifier==72)"  
        }     }
    if(SymbologiesReceive.includes("Hong Kong 2 of 5")){
        if(SymbologieCodeString !== ""){
            SymbologieCodeString = SymbologieCodeString + "||(a.symbology==67&&a.symbologyModifier==104)"  
        }else{
            SymbologieCodeString = "(a.symbology==67&&a.symbologyModifier==104)"  
        }     }
    if(SymbologiesReceive.includes("Interleaved 2 of 5")){
        if(SymbologieCodeString !== ""){
            SymbologieCodeString = SymbologieCodeString + "||(a.symbology==17&&a.symbologyModifier==48)"  
        }else{
            SymbologieCodeString = "(a.symbology==17&&a.symbologyModifier==48)"  
        }     }
    if(SymbologiesReceive.includes("Japan Post")){
        if(SymbologieCodeString !== ""){
            SymbologieCodeString = SymbologieCodeString + "||(a.symbology==34&&a.symbologyModifier==106)"  
        }else{
            SymbologieCodeString = "(a.symbology==34&&a.symbologyModifier==106)"  
        }     }
    if(SymbologiesReceive.includes("korea Post")){
        if(SymbologieCodeString !== ""){
            SymbologieCodeString =SymbologieCodeString +  "||(a.symbology==68&&a.symbologyModifier==107)"  
        }else{
            SymbologieCodeString = "(a.symbology==68&&a.symbologyModifier==107)"  
        }     }
    if(SymbologiesReceive.includes("Matrix 2 of 5")){
        if(SymbologieCodeString !== ""){
            SymbologieCodeString = SymbologieCodeString + "||(a.symbology==53&&a.symbologyModifier==77)"  
        }else{
            SymbologieCodeString = "(a.symbology==53&&a.symbologyModifier==77)"  
        }     }
    if(SymbologiesReceive.includes("Maxicode")){
        if(SymbologieCodeString !== ""){
            SymbologieCodeString = SymbologieCodeString + "||(a.symbology==37&&a.symbologyModifier==48)"  
        }else{
            SymbologieCodeString = "(a.symbology==37&&a.symbologyModifier==48)"  
        }     }
    if(SymbologiesReceive.includes("Micro QR Code")){ 
        if(SymbologieCodeString !== ""){
            SymbologieCodeString = SymbologieCodeString + "||(a.symbology==70&&a.symbologyModifier==49)"  
        }else{
            SymbologieCodeString = "(a.symbology==70&&a.symbologyModifier==49)"  
        }     }
    if(SymbologiesReceive.includes("MicroPDF417")){ 
        if(SymbologieCodeString !== ""){
            SymbologieCodeString = SymbologieCodeString + "||(a.symbology==60&&a.symbologyModifier==48)"  
        }else{
            SymbologieCodeString = "(a.symbology==60&&a.symbologyModifier==48)"  
        }     }
    if(SymbologiesReceive.includes("MSI Plessey")){
        if(SymbologieCodeString !== ""){
            SymbologieCodeString = SymbologieCodeString + "||(a.symbology==36&&a.symbologyModifier==48)"  
        }else{
            SymbologieCodeString = "(a.symbology==36&&a.symbologyModifier==48)"  
        }     }
    if(SymbologiesReceive.includes("NEC 2 of 5")){
        if(SymbologieCodeString !== ""){
            SymbologieCodeString = SymbologieCodeString + "||(a.symbology==54&&a.symbologyModifier==78)"  
        }else{
            SymbologieCodeString = "(a.symbology==54&&a.symbologyModifier==78)"  
        }     }
    if(SymbologiesReceive.includes("PDF417")){
        if(SymbologieCodeString !== ""){
            SymbologieCodeString = SymbologieCodeString + "||(a.symbology==38&&a.symbologyModifier==48)"  
        }else{
            SymbologieCodeString = "(a.symbology==38&&a.symbologyModifier==48)"  
        }     }
    if(SymbologiesReceive.includes("Pharma Code")){
        if(SymbologieCodeString !== ""){
            SymbologieCodeString = SymbologieCodeString + "||(a.symbology==52&&a.symbologyModifier==80)"  
        }else{
            SymbologieCodeString = "(a.symbology==52&&a.symbologyModifier==80)"  
        }      }
    if(SymbologiesReceive.includes("QR Code")){
        if(SymbologieCodeString !== ""){
            SymbologieCodeString = SymbologieCodeString + "||(a.symbology==41&&a.symbologyModifier==49)"  
        }else{
            SymbologieCodeString = "(a.symbology==41&&a.symbologyModifier==49)"  
        }      }
    if(SymbologiesReceive.includes("QR Code Model 1")){ 
        if(SymbologieCodeString !== ""){
            SymbologieCodeString = SymbologieCodeString + "||(a.symbology==41&&a.symbologyModifier==48)"  
        }else{
            SymbologieCodeString = "(a.symbology==41&&a.symbologyModifier==48)"  
        }      }
    if(SymbologiesReceive.includes("Straight 2 of 5 (3 start/stop bars)")){ 
        if(SymbologieCodeString !== ""){
            SymbologieCodeString = SymbologieCodeString + "||(a.symbology==32&&a.symbologyModifier==48)"  
        }else{
            SymbologieCodeString = "(a.symbology==32&&a.symbologyModifier==48)"  
        }      }
    
    if(SymbologiesReceive.includes("Telepen")){
        if(SymbologieCodeString !== ""){
            SymbologieCodeString = SymbologieCodeString + "||(a.symbology==56&&a.symbologyModifier==48)"  
        }else{
            SymbologieCodeString = "(a.symbology==56&&a.symbologyModifier==48)"  
        }      }
    if(SymbologiesReceive.includes("TriOptic")){
        if(SymbologieCodeString !== ""){
            SymbologieCodeString = SymbologieCodeString + "||(a.symbology==57&&a.symbologyModifier==84)"  
        }else{
            SymbologieCodeString = "(a.symbology==57&&a.symbologyModifier==84)"  
        }      }
    if(SymbologiesReceive.includes("UK Plessey")){
        if(SymbologieCodeString !== ""){
            SymbologieCodeString = SymbologieCodeString + "||(a.symbology==66&&a.symbologyModifier==48)"  
        }else{
            SymbologieCodeString = "(a.symbology==66&&a.symbologyModifier==48)"  
        }      }
    if(SymbologiesReceive.includes("UK Royal Mail")){
        if(SymbologieCodeString !== ""){
            SymbologieCodeString = SymbologieCodeString + "||(a.symbology==42&&a.symbologyModifier==114)"  
        }else{
            SymbologieCodeString = "(a.symbology==42&&a.symbologyModifier==114)"  
        }      }
    if(SymbologiesReceive.includes("UPC-A")){
        if(SymbologieCodeString !== ""){
            SymbologieCodeString = SymbologieCodeString + "||(a.symbology==49&&a.symbologyModifier==65)"  
        }else{
            SymbologieCodeString = "(a.symbology==49&&a.symbologyModifier==65)"  
        }      }
    if(SymbologiesReceive.includes("UPC-E")){ 
        if(SymbologieCodeString !== ""){
            SymbologieCodeString = SymbologieCodeString + "||(a.symbology==49&&(a.symbologyModifier==66||a.symbologyModifier==67))"  
        }else{
            SymbologieCodeString = "(a.symbology==49&&(a.symbologyModifier==66||a.symbologyModifier==67))"  
        }      }
    if(SymbologiesReceive.includes("UPU ID")){
        if(SymbologieCodeString !== ""){
            SymbologieCodeString = SymbologieCodeString + "||(a.symbology==69&&a.symbologyModifier==117)"  
        }else{
            SymbologieCodeString = "(a.symbology==69&&a.symbologyModifier==117)"  
        }      }
    if(SymbologiesReceive.includes("USPS Intelligent Mail")){
        if(SymbologieCodeString !== ""){
            SymbologieCodeString = SymbologieCodeString + "||(a.symbology==58&&a.symbologyModifier==105)"  
        }else{
            SymbologieCodeString = "(a.symbology==58&&a.symbologyModifier==105)"  
        }      }
    if(SymbologiesReceive.includes("USPS Planet")){
        if(SymbologieCodeString !== ""){
            SymbologieCodeString = SymbologieCodeString + "||(a.symbology==39&&a.symbologyModifier==101)"  
        }else{
            SymbologieCodeString = "(a.symbology==39&&a.symbologyModifier==101)"  
        }      }
    if(SymbologiesReceive.includes("USPS Postnet")){
        if(SymbologieCodeString !== ""){
            SymbologieCodeString = SymbologieCodeString + "||(a.symbology==40&&a.symbologyModifier==116)"  
        }else{
            SymbologieCodeString = "(a.symbology==40&&a.symbologyModifier==116)"  
        }      }

        if(SymbologieCodeString !== ""){
            SymbologieCodeFinal = "{if(" + SymbologieCodeString + ")";
            console.log(SymbologieCodeFinal);
            count++;
        }
        

        // BARCODE LENGHT TO MATCH  

        if(BarcodelenghtEqualTo !== ""){
            var equalToNumber = "(a.data.length==" + BarcodelenghtEqualTo + ")";
            if(BarcodeLenghtFinal !== ""){
                BarcodeLenghtFinal = BarcodeLenghtFinal + "||" + equalToNumber;
            }else{
                BarcodeLenghtFinal = equalToNumber;
            }  
        }
        if(BarcodelenghtNotEqualTo !== ""){
            var notEqualToNumber = "(a.data.length!=" + BarcodelenghtNotEqualTo + ")";
            if(BarcodeLenghtFinal !== ""){
                BarcodeLenghtFinal = BarcodeLenghtFinal + "||" + notEqualToNumber;
            }else{
                BarcodeLenghtFinal = notEqualToNumber;
            }
        }
        if(BarcodelenghtGreaterThan !== ""){
            var greaterThanNumber = "(a.data.length>" + BarcodelenghtGreaterThan + ")";
            if(BarcodeLenghtFinal !== ""){
                BarcodeLenghtFinal = BarcodeLenghtFinal + "||" + greaterThanNumber;
            }else{
                BarcodeLenghtFinal = greaterThanNumber;
            }
        }
        if(BarcodelenghtLessThan !== ""){
            var lessThanNumber = "(a.data.length<" + BarcodelenghtLessThan + ")";
            if(BarcodeLenghtFinal !== ""){
                BarcodeLenghtFinal = BarcodeLenghtFinal + "||" + lessThanNumber;
            }else{
                BarcodeLenghtFinal = lessThanNumber;
            }
        }
        if(BarcodelenghtGreaterThanEqualTo !== ""){
            var greaterThanEqualToNumber = "(a.data.length>=" + BarcodelenghtGreaterThanEqualTo + ")";
            if(BarcodeLenghtFinal !== ""){
                BarcodeLenghtFinal = BarcodeLenghtFinal + "||" + greaterThanEqualToNumber;
            }else{
                BarcodeLenghtFinal = greaterThanEqualToNumber;
            }
        }
        if(BarcodelenghtLessThanEqualTo !== ""){
            var lessThanEqualToNumber = "(a.data.length<=" + BarcodelenghtLessThanEqualTo + ")";
            if(BarcodeLenghtFinal !== ""){
                BarcodeLenghtFinal = BarcodeLenghtFinal + "||" + lessThanEqualToNumber;
            }else{
                BarcodeLenghtFinal = lessThanEqualToNumber;
            }
        }
        if(BLGreaterANDLessthan[0] !== undefined){
            var greaterThan_AND = BLGreaterANDLessthan[0];
            var lessThan_AND = BLGreaterANDLessthan[1];
            var greaterThanANDLessThan = "(a.data.length>" + greaterThan_AND + "&&a.data.length<" + lessThan_AND + ")";
            if(BarcodeLenghtFinal !== ""){
                BarcodeLenghtFinal = BarcodeLenghtFinal + "||" + greaterThanANDLessThan;
            }else{
                BarcodeLenghtFinal = greaterThanANDLessThan;
            }
        }
        if(BLGreaterEqualANDLessEqual[0] !== undefined){
            var greaterThanEqual_AND = BLGreaterEqualANDLessEqual[0];
            var lessThanEqual_AND = BLGreaterEqualANDLessEqual[1];
            var greaterThanEqualANDLessThanEqual = "(a.data.length>=" + greaterThanEqual_AND + "&&a.data.length<=" + lessThanEqual_AND + ")";
            if(BarcodeLenghtFinal !== ""){
                BarcodeLenghtFinal = BarcodeLenghtFinal + "||" + greaterThanEqualANDLessThanEqual;
            }else{
                BarcodeLenghtFinal = greaterThanEqualANDLessThanEqual;
            }
        }
        if(BLNotEqualANDGreaterThan[0] !== undefined){
            var notEqual_AND = BLNotEqualANDGreaterThan[0];
            var greaterThan_AND2 = BLNotEqualANDGreaterThan[1];
            var notEqualANDGreaterThan = "(a.data.length!=" + notEqual_AND + "&&a.data.length>" + greaterThan_AND2 + ")";
            if(BarcodeLenghtFinal !== ""){
                BarcodeLenghtFinal = BarcodeLenghtFinal + "||" + notEqualANDGreaterThan;
            }else{
                BarcodeLenghtFinal = notEqualANDGreaterThan;
            }      
        }
        if(BLNotEqualANDLessThan[0] !== undefined){
            var notEqual2_AND = BLNotEqualANDLessThan[0];
            var lessThan_AND2 = BLNotEqualANDLessThan[1];
            var notEqualANDLessThan = "(a.data.length!=" + notEqual2_AND + "&&a.data.length<" + lessThan_AND2 + ")";
            if(BarcodeLenghtFinal !== ""){
                BarcodeLenghtFinal = BarcodeLenghtFinal + "||" + notEqualANDLessThan;
            }else{
                BarcodeLenghtFinal = notEqualANDLessThan;
            } 
        }
        if(BLNotEqualANDGreaterthanEqualTo[0] !== undefined){
            var notEqual3_AND = BLNotEqualANDGreaterthanEqualTo[0];
            var greaterThanEqual_AND2 = BLNotEqualANDGreaterthanEqualTo[1];
            var notEqualANDGreaterThanEqual = "(a.data.length!=" + notEqual3_AND + "&&a.data.length>=" + greaterThanEqual_AND2 + ")";
            if(BarcodeLenghtFinal !== ""){
                BarcodeLenghtFinal = BarcodeLenghtFinal + "||" + notEqualANDGreaterThanEqual;
            }else{
                BarcodeLenghtFinal = notEqualANDGreaterThanEqual;
            }         
        }
        if(BLNotEqualANDLessThanEqualTo[0] !== undefined){
            var notEqual4_AND = BLNotEqualANDLessThanEqualTo[0];
            var lessThanEqualTo_AND = BLNotEqualANDLessThanEqualTo[1];
            var notEqualANDLessThanEqual = "(a.data.length!=" + notEqual4_AND + "&&a.data.length<=" + lessThanEqualTo_AND + ")";
            if(BarcodeLenghtFinal !== ""){
                BarcodeLenghtFinal = BarcodeLenghtFinal + "||" + notEqualANDLessThanEqual;
            }else{
                BarcodeLenghtFinal = notEqualANDLessThanEqual;
            }        
        }
        if(BLNotEqualANDGreaterThanANDLessThan[0] !== undefined){
            var notEqual5_AND = BLNotEqualANDGreaterThanANDLessThan[0];
            var greaterThan2_AND2 = BLNotEqualANDGreaterThanANDLessThan[1];
            var lessThan2_AND2 = BLNotEqualANDGreaterThanANDLessThan[2];
            var notEqualANDGreaterThanANDLessThan = "(a.data.length!=" + notEqual5_AND + "&&a.data.length>" + greaterThan2_AND2 + "&&a.data.length<" + lessThan2_AND2 + ")";
            if(BarcodeLenghtFinal !== ""){
                BarcodeLenghtFinal = BarcodeLenghtFinal + "||" + notEqualANDGreaterThanANDLessThan;
            }else{
                BarcodeLenghtFinal = notEqualANDGreaterThanANDLessThan;
            }  
        }
        if(BLNotEqualANDGreaterEqualtoANDLessEqualTo[0] !== undefined){
            var notEqual6_AND = BLNotEqualANDGreaterEqualtoANDLessEqualTo[0];
            var greaterThanEqual2_AND2 = BLNotEqualANDGreaterEqualtoANDLessEqualTo[1];
            var lessThanEqual2_AND2 = BLNotEqualANDGreaterEqualtoANDLessEqualTo[2];
            var notEqualANDGreaterThanEqualANDLessThanEqual = "(a.data.length!=" + notEqual6_AND + "&&a.data.length>=" + greaterThanEqual2_AND2 + "&&a.data.length<=" + lessThanEqual2_AND2 + ")";
            if(BarcodeLenghtFinal !== ""){
                BarcodeLenghtFinal = BarcodeLenghtFinal + "||" + notEqualANDGreaterThanEqualANDLessThanEqual;
            }else{
                BarcodeLenghtFinal = notEqualANDGreaterThanEqualANDLessThanEqual;
            }         
        }
        if(BarcodeLenghtFinal !== ""){
            BarcodeCodeLenghtFinal = "{if" + BarcodeLenghtFinal;
            console.log(BarcodeCodeLenghtFinal);
            count++;
        }
        

        // BARCODE DATA TO MATCH

        if(BarcodeDataStartsString !== ""){
            if(BarcodeDataFinal === ""){
                BarcodeDataFinal = "(a.data.substring(0,"+ BarcodeDataStartsString.length +")==" + "\"" + BarcodeDataStartsString + "\")";
            }else{
                BarcodeDataFinal = BarcodeDataFinal + "||" + "(a.data.substring(0," + BarcodeDataStartsString.length +")==" + "\"" + BarcodeDataStartsString + "\")";
            }
            
        }
        if(BarcodeDataEndsString !== ""){
            if(BarcodeDataFinal === ""){
                BarcodeDataFinal = "(a.data.substring(a.data.length-" + BarcodeDataEndsString.length + ")==" + "\"" + BarcodeDataEndsString + "\")";
            }else{
                BarcodeDataFinal = BarcodeDataFinal + "||" + "(a.data.substring(a.data.length-" + BarcodeDataEndsString.length + ")==" + "\"" + BarcodeDataEndsString + "\")";
            }
           
        }
        if( BarcodeDataContainsString !== ""){
            if(BarcodeDataFinal === ""){
                BarcodeDataFinal = "(a.data.indexOf(" + "\"" + BarcodeDataContainsString + "\")>-1)";
            }else{
                BarcodeDataFinal = BarcodeDataFinal + "||" + "(a.data.indexOf(" + "\"" + BarcodeDataContainsString + "\")>-1)";
            }
        }
        if(BarcodeDataStartsANDEndsString[0] !== undefined){
            if(BarcodeDataFinal === ""){
                BarcodeDataFinal = "a.data.substring(0," + BarcodeDataStartsANDEndsString[0].length + ")==" + "\"" + BarcodeDataStartsANDEndsString[0] + "\"&&a.data.substring(a.data.length-" + BarcodeDataStartsANDEndsString[1].length + ")==" + "\"" + BarcodeDataStartsANDEndsString[1] + "\"";
            }else{
                BarcodeDataFinal = BarcodeDataFinal + "||" + "(a.data.substring(0,"+ BarcodeDataStartsANDEndsString[0].length +")==" + "\"" + BarcodeDataStartsANDEndsString[0] + "\"&&a.data.substring(a.data.length-"+ BarcodeDataStartsANDEndsString[1].length + ")==" + "\"" + BarcodeDataStartsANDEndsString[1] + "\")";
            }
        }
        if(BarcodeDataStartsANDContainsString[0] !== undefined){
            if(BarcodeDataFinal === ""){
                BarcodeDataFinal = "a.data.substring(0,"+ BarcodeDataStartsANDContainsString[0].length + ")==" + "\"" + BarcodeDataStartsANDContainsString[0] + "\"&&a.data.indexOf(" + "\"" + BarcodeDataStartsANDContainsString[1] + "\")>-1";
            }else{
                BarcodeDataFinal = BarcodeDataFinal + "||" + "(a.data.substring(0,"+ BarcodeDataStartsANDContainsString[0].length + ")==" + "\"" + BarcodeDataStartsANDContainsString[0] + "\"&&a.data.indexOf(" + "\"" + BarcodeDataStartsANDContainsString[1] + "\")>-1)";
            }
        }
        if(BarcodeDataContainsANDEndsString[0] !== undefined){
            if(BarcodeDataFinal === ""){
                BarcodeDataFinal = "a.data.substring(a.data.length-" + BarcodeDataContainsANDEndsString[1].length + ")==" + "\"" + BarcodeDataContainsANDEndsString[1] + "\"&&a.data.indexOf(" + "\"" + BarcodeDataContainsANDEndsString[0] + "\")>-1";
            }else{
                BarcodeDataFinal = BarcodeDataFinal + "||" + "(a.data.substring(a.data.length-" + BarcodeDataContainsANDEndsString[1].length + ")==" + "\"" + BarcodeDataContainsANDEndsString[1] +"\"&&a.data.indexOf(" + "\"" + BarcodeDataContainsANDEndsString[0] + "\")>-1)";
            }
         
        }
        if(BarcodeDataStartsANDContainsANDEndsString[0] !== undefined){
            if(BarcodeDataFinal === ""){
                BarcodeDataFinal = "a.data.substring(0," + BarcodeDataStartsANDContainsANDEndsString[0].length + ")==" + "\"" + BarcodeDataStartsANDContainsANDEndsString[0] + "\"&&a.data.substring(a.data.length-" + BarcodeDataStartsANDContainsANDEndsString[2].length + ")==" + "\"" + BarcodeDataStartsANDContainsANDEndsString[2] + "\"&&a.data.indexOf(" + "\"" + BarcodeDataStartsANDContainsANDEndsString[1] + "\")>-1";
            }else{
                BarcodeDataFinal = BarcodeDataFinal + "||" + "(a.data.substring(0,"+ BarcodeDataStartsANDContainsANDEndsString[0].length +")==" + "\"" + BarcodeDataStartsANDContainsANDEndsString[0] + "\"&&a.data.substring(a.data.length-" + BarcodeDataStartsANDContainsANDEndsString[2].length + ")==" + "\"" + BarcodeDataStartsANDContainsANDEndsString[2] + "\"&&a.data.indexOf(" + "\"" + BarcodeDataStartsANDContainsANDEndsString[1] + "\")>-1)";
            }
        }
        if(BarcodeDataFinal !== ""){
             BarcodeDataCodeFinal = "{if" + BarcodeDataFinal;
            console.log(BarcodeDataCodeFinal);
            count++;
        }
        

        // SEND PARTIAL BARCODE // 

        if(sendPartialBcode[0] !== undefined){

            SendPartial ="a.data=a.data.substring(" + (sendPartialBcode[0] - 1) + "," + sendPartialBcode[1] + ");"
            if(DataToPerformSelected === false){
                SendPartial = "{" + SendPartial;
                DataToPerformSelected = true;
            }
            console.log(SendPartial);
            
            
        }

        // INSERT CHARACTER TO BARCODE //

        if(CharToInsertBcode[0] !== undefined){
            var characterSelected = MetaCharReplaced(CharToInsertBcode[0]);
            var positionToInsert = CharToInsertBcode[1];

            InsertCharacter = "a.data=a.data.substring(0," + positionToInsert + ")+\"" + characterSelected +"\"+a.data.substring(" + positionToInsert + ");";
            console.log(InsertCharacter);
            if(DataToPerformSelected === false){
                InsertCharacter = "{" + InsertCharacter;
                DataToPerformSelected = true;
            }
        }
        
            //DELETE END OF BARCODE //

        if(deleteEndOfBarcode !== ""){

            BarcodeEndDeleted = "a.data=a.data.slice(0,-" + deleteEndOfBarcode +");";

            console.log(BarcodeEndDeleted);
            if(DataToPerformSelected === false){
                BarcodeEndDeleted = "{" + BarcodeEndDeleted;
                DataToPerformSelected = true;
            }
        }

        // SUBSTITUTE DATA IN BARCODE //

        if(SubtituteData1[0] !== undefined){

            var substituteString1 = SubtituteData1[0];
            var replaceString1 = SubtituteData1[1];
            SubstituteCode1 = "a.data=a.data.split(\"" + substituteString1 + "\").join(\"" + replaceString1 + "\");"

            if(SubtituteData2[0] !== undefined){
                var substituteString2 = SubtituteData2[0];
                var replaceString2 = SubtituteData2[1];
                SubstituteCode2 = "a.data=a.data.split(\"" + substituteString2 + "\").join(\"" + replaceString2 + "\");"

                if(SubtituteData3[0] !== undefined){
                    var substituteString3 = SubtituteData3[0];
                    var replaceString3 = SubtituteData3[1];
                    SubstituteCode3 = "a.data=a.data.split(\"" + substituteString3 + "\").join(\"" + replaceString3 + "\");"

                    if(SubtituteData4[0] !== undefined){
                        var substituteString4 = SubtituteData4[0];
                        var replaceString4 = SubtituteData4[1];
                        SubstituteCode4 = "a.data=a.data.split(\"" + substituteString4 + "\").join(\"" + replaceString4 + "\");"
                    
                        if(SubtituteData5[0] !== undefined){
                            var substituteString5 = SubtituteData5[0];
                            var replaceString5 = SubtituteData5[1];
                            SubstituteCode5 = "a.data=a.data.split(\"" + substituteString5 + "\").join(\"" + replaceString5 + "\");"
                        }
                    }
                }
            }

            SubstituteCodeFinal = SubstituteCode1 + SubstituteCode2 + SubstituteCode3 + SubstituteCode4 + SubstituteCode5;
            console.log(SubstituteCodeFinal);
            if(DataToPerformSelected === false){
                SubstituteCodeFinal = "{" + SubstituteCodeFinal;
                DataToPerformSelected = true;
            }
        }

        // ALERT USER //

        if(AlertUser !== ""){

            AlertString = "reader.beep(" + AlertUser + ");";
            console.log(AlertString);
            DataToPerformSelected = true;
        }

        //DO NOT TRANSMIT //

        if(DoNotTransmit === true){

            DoNotTransmitString = "return false";
            console.log(DoNotTransmitString);
            DataToPerformSelected = true;
        }
    const currentDate = Moment().format("yyyy-MM-D--HH-mm-ss");
        console.log(currentDate); 
        

        BarcodeStringFinal = "rules_onDecode=function(a)" + SymbologieCodeFinal + BarcodeCodeLenghtFinal + BarcodeDataCodeFinal + SendPartial + InsertCharacter + 
        BarcodeEndDeleted + SubstituteCodeFinal + AlertString;
        console.log(BarcodeDataCodeFinal);
        
        if(DataToPerformSelected === true && DoNotTransmit === false){
            BarcodeStringFinal = BarcodeStringFinal + "return a";
        }
        if(DataToPerformSelected === false && DoNotTransmit === false){
            BarcodeStringFinal = BarcodeStringFinal + "{return a";
        }
        if(DataToPerformSelected === true && DoNotTransmit === true){
            BarcodeStringFinal = BarcodeStringFinal + "return false";
        }
        if(DataToPerformSelected === false && DoNotTransmit === true){
            BarcodeStringFinal = BarcodeStringFinal + "{return false";
        }
        for(var i=0 ; i<count ; i++){
            BarcodeStringFinal = BarcodeStringFinal + "}";
        }
        BarcodeStringFinal = BarcodeStringFinal + ";return a};" //modify here, check if causes any issue
        var fileName = ".codeRules." + currentDate + ".js";
        
       
        const filesize = new Blob([BarcodeStringFinal]).size;
        console.log(filesize);
        const filecrc = crc.crc16xmodem(BarcodeStringFinal);
        console.log(filecrc);
        BarcodeStringFinal = header + formatCmd + ",FN" + fileName + ",SZ" + filesize + ",CR" + filecrc + ",RB1" + split + "RDFD" + BarcodeStringFinal + footer;
        console.log(BarcodeStringFinal);

        
        const webApiRequest = 'https://Barcodegen.ecs-dev.codecorp.com:5000/barcodegen/';
        var formData = new FormData();
         formData.append("BarcodeString", currentDate);
         formData.append("BarcodeData", BarcodeStringFinal);
       axios({
         method: "POST",
         url: webApiRequest,
         data: formData,
         headers: { "Content-Type": "multipart/form-data"},
         responseType: "blob"
       }).then((response) => {
         console.log(response);
         const reader = new window.FileReader();
         reader.readAsDataURL(response.data);
         reader.onload = () =>{
            setImageBarcode(reader.result);
         }
         setShowDownloadButton(true);
       });
    }

    const componentRef = useRef();
    const pageStyle = " @page { size: landscape }"
                       
    const handlePrint = useReactToPrint({
        content : () => componentRef.current,
        documentTitle: 'Data Parsing',
    });
    console.log(optionsSelected);
    
        return ( 
            <>
                <div class="downloadButton2">
                   <button onClick={buttonClick}>Generate Code</button>
                   </div>
                 <div class='downloadButton' ref={componentRef}>
                
                 {imageBarcode ? (<img src={imageBarcode} alt='img' width="500" height="500"></img>) : null} 
                 
                 <div class="optionsSelected" >
                {optionsSelected2}
                 {optionsSelected}
                 </div>
                </div>
                {showDownloadButton &&
                <div id="downloadfile" class="downloadButton2">
              <Button variant="outlined" color="primary" href={imageBarcode} download="Data Parsing Action" > Download  </Button> 
             <div class="printButton">
             <Button variant="outlined" color="primary" onClick={handlePrint}>Print</Button>
             </div>
             
            </div>
            }
            </>
        );
    
}
 
export default DPGetBarcodeRulesCreated;