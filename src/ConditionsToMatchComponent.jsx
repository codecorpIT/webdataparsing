
import React, { Component } from 'react';
import { useState } from "react";
import Multiselect from "multiselect-react-dropdown";
import Select from 'react-select';


var symbologiesSelected = [];
var barcodeLenghtEqualTo = "";
var barcodeLenghtNotEqualTo = "";
var barcodeLenghtGreaterThan = "";
var barcodeLenghtLessThan = "";
var barcodeLenghtGreaterThanEqualTo = "";
var barcodeLenghtLessThanEqualTo = "";
var barLenghtGreatherThanANDLessThan = [];
var barLenghtGreatherThanEqualToANDLessThanEqualTo = [];
var barLengthNotEqualANDGreaterThan = [];
var barLengthNotEqualANDLessThan = [];
var barLengthNotEqualANDGreaterThanEqualTo = [];
var barLengthNotEqualANDLessThanEqualTo = [];
var barLengthNotEqualANDGreaterThanANDLessThan = [];
var barLengthNotEqualANDGreaterThanEqualToANDLessThanEqualTo = [];
var barDataStarts = "";
var barDataContains = "";
var barDataEnds = "";
var barDataStartsANDEnds = [];
var barDataStartsANDContains = [];
var barDataContainsANDEnds = [];
var barDataStartsANDContainsANDEnds = [];
var optionsSelectedInTotal = "\n";

export function AllOptionsSelectedConditions(){
    return optionsSelectedInTotal;
}


export function SymbologiesSelectedByUser() {
    console.log("SymbologiesReceive");
    return symbologiesSelected;
}
export function DataLenghtEqualToSelectedByUser(){
    console.log(barcodeLenghtEqualTo);
    return barcodeLenghtEqualTo;
}
export function DataLenghtNotEqualToSelectedByUser(){
    console.log(barcodeLenghtNotEqualTo);
    return barcodeLenghtNotEqualTo;
}
export function DataLenghtGreaterThanSelectedByUser(){
    console.log(barcodeLenghtGreaterThan);
    return barcodeLenghtGreaterThan;
}
export function DataLenghtLessThanSelectedByUser(){
    console.log(barcodeLenghtLessThan);
    return barcodeLenghtLessThan;
}
export function DataLenghtGreaterThanEqualToSelectedByUser(){
    console.log(barcodeLenghtGreaterThanEqualTo);
    return barcodeLenghtGreaterThanEqualTo;
}
export function DataLenghtLessThanEqualToSelectedByUser(){
    console.log(barcodeLenghtLessThanEqualTo);
    return barcodeLenghtLessThanEqualTo;
}

export function DLGreaterThanANDLessThanSelectedByUser(){
    console.log(barLenghtGreatherThanANDLessThan);
    return barLenghtGreatherThanANDLessThan;
}
export function DLGreaterThanEqualToANDLessThanEqualToSelectedByUser(){
    console.log(barLenghtGreatherThanEqualToANDLessThanEqualTo);
    return barLenghtGreatherThanEqualToANDLessThanEqualTo;
}
export function DLNotEqualANDGreaterThanSelectedByUser(){
    console.log(barLengthNotEqualANDGreaterThan);
    return barLengthNotEqualANDGreaterThan;
}
export function DLNotEqualANDLessThanSelectedByUser(){
    console.log(barLengthNotEqualANDLessThan);
    return barLengthNotEqualANDLessThan;
}
export function DLNotEqualANDGreaterThanEqualToSelectedByUser(){
    console.log(barLengthNotEqualANDGreaterThanEqualTo);
    return barLengthNotEqualANDGreaterThanEqualTo;
}
export function DLNotEqualANDLessThanEqualToSelectedByUser(){
    console.log(barLengthNotEqualANDLessThanEqualTo);
   return barLengthNotEqualANDLessThanEqualTo;
}
export function DLNotEqualANDGreaterThanANDLessThanSelectedByUser(){
    console.log(barLengthNotEqualANDGreaterThanANDLessThan);
   return barLengthNotEqualANDGreaterThanANDLessThan;
}
export function DLNotEqualANDGreaterThanEqualToANDLessThanEqualToSelectedByUser(){
    console.log(barLengthNotEqualANDGreaterThanEqualToANDLessThanEqualTo);
   return barLengthNotEqualANDGreaterThanEqualToANDLessThanEqualTo;
}
export function BarcodeDataStartsWith() {
    console.log(barDataStarts);
    return barDataStarts;
}
export function BarcodeDataContains(){
    console.log(barDataContains);
    return barDataContains;
}
export function BarcodeDataEndsWith(){
    console.log(barDataEnds);
    return barDataEnds;
}
export function BarcodeDataStartsANDEnds(){
    console.log(barDataStartsANDEnds);
    return barDataStartsANDEnds;
}
export function BarcodeDataStartANDContains(){
    console.log(barDataStartsANDContains);
    return barDataStartsANDContains;
}
export function BarcodeDataContainsANDEnds(){
    console.log(barDataContainsANDEnds);
    return barDataContainsANDEnds;
}
export function BarcodeDataStartsANDContainsANDEnds(){
    console.log(barDataStartsANDContainsANDEnds);
    return barDataStartsANDContainsANDEnds;
}
function App(){
    
    var [barcodeLengthOptions, setBarcodeLenghtOptions] = useState([ "Clear All", "Equal To:", "Not Equal To:", "Greater Than:", "Less Than:", "Greater Than/Equal To:",
         "Less Than/Equal To:"]);
    var [barcodeLengthOptions2, setBarcodeLengthOptions2]= useState(["Clear All", "Greater Than AND Less Than:", "Greater Than/Equal To AND Less Than/Equal to:", "Not Equal To AND Greater Than:", "Not Equal To AND Less Than:", "Not Equal To AND Greater Than/Equal To:",
    "Not Equal To AND Less Than/Equal To:", "Not Equal To AND Greater Than AND Less Than:", "Not Equal To AND Greater Than/Equal To AND Less Than/Equal To:"])
    var [symbologiesToMatchOptions, setSymbologies] = useState([ "All","Australian Post", "Aztec", "BC412", "Canada Post", "Codabar", "Codablock F", "Code 11", "Code 32", "Code 39", "Code 49", "Code 93", "Code 128"
        ,"Composite Code A", "Composite Code B", "Composite Code C", "Data Matrix", "Dutch Post (KIX)", "EAN-13", "EAN-8", "GoCode", "Grid Matrix", "GS1 Databar", "GS1 Databar Expanded", "GS1 Databar Expanded Stacked", "GS1 Databar Limited", "GS1 Databar Stacked",
        "HanXin", "Hong Kong 2 of 5", "Interleaved 2 of 5", "Japan post", "Korea Post", "Matrix 2 of 5", "Maxicode", "Micro QR Code", "MicroPDF417", "MSI Plessey", "NEC 2 of 5", "PDF417", "Pharma Code", "QR Code", "QR Code Model 1", "Straight 2 of 5 (3 start/stop bars)",
        "Telepen", "TriOptic", "UK Plessey", "UK Royal Mail", "UPC-A", "UPC-E", "UPU ID", "USPS Intelligent Mail", "USPS Planet", "USPS Postnet" ]);
    var [barcodeDataOptions, setBarcodeDataOptions] = useState([ "Clear All", "Starts With:", "Ends With:", "Contains:"]);
    var [barcodeDataOptions2, setBarcodeDataOptions2] = useState([ "Clear All", "Starts AND Ends With:", "Starts AND Contains:", "Contains AND Ends With:", "Start AND Contains AND Ends with:"]);


    var [isEqualTo, getIsEqualTo] = useState();
    var [isNotEqualTo, getIsNotEqualTo] = useState("");
    var [isGreaterThan, getIsGreaterThan] = useState();
    var [isLessThan, getIsLessThan] = useState();
    var [isGreaterThanEqualTo, getIsGreatThanEqualTo] = useState();
    var [isLessThanEqualTo, getIsLessThanEqualTo] = useState();

   var [barcodeLenghtLabel, getBarcodeLenghtLabel]  = useState("");
  

   var [symnologiesMatched, getSymbologiesMatched] = useState();
   var [barcodeDataOR, getBarcodeDataOR] = useState();
   var [barcodeDataAND, getBarcodeDataAND] = useState();
   const [selectedCondition1, setSelectedCondition1] = useState(1);
   const [selectedCondition2, setSelectedCondition2] = useState(1);
   const SymbologiesToMatch =  (e) => {
    console.log(e);
   var header = "<Symbologies To Match>";
   getSymbologiesMatched(header + "Equal To " + e);
   if(e[0] === undefined){
       getSymbologiesMatched(null);
   }
   symbologiesSelected = e;
   
   }
   const RadioSelectionChange1 = (e) =>{
      setSelectedCondition1(e.target.value);  
   }
   const RadioSelectionChange2 = (e) =>{
    setSelectedCondition2(e.target.value);  
 }
    const BarcodelengthToMatchOR =  (e) => {
        console.log(e);
       var header = "<Barcode Length>";
        if(e.includes("Equal To:")){

            var equalTo = prompt("Please Enter the length to match: ");
            if(equalTo == null || equalTo === ""){
                return;
            }
            console.log(equalTo);
            barcodeLenghtEqualTo = equalTo;
            getIsEqualTo(true);
            if(isGreaterThan !== true && isNotEqualTo !== true && isLessThan !== true && isGreaterThanEqualTo !== true && isLessThanEqualTo !== true ){

            getBarcodeLenghtLabel(header + "Equal To: " + equalTo);
            }else{
                getBarcodeLenghtLabel(barcodeLenghtLabel + " OR " + "Equal To: " + equalTo);

            }
           
        }

        if(e.includes("Not Equal To:")){
           getIsNotEqualTo(true);
           var notEqualTo = prompt("Please Enter the length to match: ");
           if(notEqualTo == null || notEqualTo === ""){
            return;
        }
           console.log(notEqualTo);
            barcodeLenghtNotEqualTo = notEqualTo;
            if(isGreaterThan !== true && isEqualTo !== true && isLessThan !== true && isGreaterThanEqualTo !== true && isLessThanEqualTo !== true ){

            getBarcodeLenghtLabel(header + "Not Equal To: " + notEqualTo);
            }else{
                getBarcodeLenghtLabel(barcodeLenghtLabel + " OR " + "Not Equal To: " + notEqualTo);

            }
                
            
    
        }

        if(e.includes("Greater Than:")){
           
            getIsGreaterThan(true);
            var greaterThan = prompt("Please Enter the length to match: ");
            if(greaterThan == null || greaterThan === ""){
                return;
            }
            console.log(greaterThan);
            barcodeLenghtGreaterThan = greaterThan;
            if(isNotEqualTo !== true && isEqualTo !== true && isLessThan !== true && isGreaterThanEqualTo !== true && isLessThanEqualTo !== true ){

            getBarcodeLenghtLabel(header + "Greater than: " + greaterThan);
            }else{
                getBarcodeLenghtLabel(barcodeLenghtLabel + " OR " + "Greater than: " + greaterThan);

            }
        }

        if(e.includes("Less Than:")){
            getIsLessThan(true);
            var lessThan = prompt("Please Enter the length to match: ");
            if(lessThan == null || lessThan === ""){
                return;
            }
            console.log(lessThan);
            barcodeLenghtLessThan = lessThan;
            if(isNotEqualTo !== true && isEqualTo !== true && isGreaterThan !== true && isGreaterThanEqualTo !== true && isLessThanEqualTo !== true ){
                getBarcodeLenghtLabel(header + "Less Than: " + lessThan);

            }else{
                getBarcodeLenghtLabel(barcodeLenghtLabel + " OR " + "Less Than: " + lessThan);

            }
        }

        if(e.includes("Greater Than/Equal To:")){
            
            getIsGreatThanEqualTo(true);
            var greaterThanEqualTo = prompt("Please Enter the length to match: ");
            if(greaterThanEqualTo == null || greaterThanEqualTo === ""){
                return;
            }
        console.log(greaterThanEqualTo);
            barcodeLenghtGreaterThanEqualTo = greaterThanEqualTo;
           if(isNotEqualTo !== true && isEqualTo !== true && isGreaterThan !== true && isLessThan !== true && isLessThanEqualTo !== true ){
              
            getBarcodeLenghtLabel(header + "Greater Than/Equal To: " + greaterThanEqualTo);
            }else{
                getBarcodeLenghtLabel(barcodeLenghtLabel + " OR " + "Greater Than/Equal To: ");
            }
        }

        if(e.includes("Less Than/Equal To:")){
            
            getIsLessThanEqualTo(true);
            var lessThanEqualTo = prompt("Please Enter the length to match: ");
            if(lessThanEqualTo == null || lessThanEqualTo === ""){
                return;
            }
            console.log(lessThanEqualTo);
            barcodeLenghtLessThanEqualTo = lessThanEqualTo;
           if(isNotEqualTo !== true && isEqualTo !== true && isGreaterThan !== true && isLessThan !== true && isGreaterThanEqualTo !== true ){
               
            getBarcodeLenghtLabel(header + "Less Than/Equal To: " + lessThanEqualTo);
            }else{
                getBarcodeLenghtLabel(barcodeLenghtLabel + " OR " + "Less Than/Equal To: " + lessThanEqualTo);
            }
        }
        if(e.includes("Clear All")){
            barcodeLenghtEqualTo = "";
            
                barcodeLenghtNotEqualTo = "";
                 barcodeLenghtGreaterThan = "";
                 barcodeLenghtLessThan = "";
                 barcodeLenghtGreaterThanEqualTo = "";
                barcodeLenghtLessThanEqualTo = "";
                getIsEqualTo(false);
                getIsNotEqualTo(false);
                getIsGreaterThan(false);
                getIsLessThan(false);
                getIsGreatThanEqualTo(false);
                getIsLessThanEqualTo(false);
               
                
                
                getBarcodeLenghtLabel(null)
               
           
               

               
               
        }     
        
    }
    const [barcodeLenghtLabelAND, setBarcodeLenghtLabelAND] = useState();
    var [greaterThanANDLessThan, setGreaterThanANDLessThan] = useState("");
    var [greaterThanEqualANDLessThanEqual, setGreaterThanEqualANDLessThanEqual] = useState("");
    var [notEqualANDGreaterThan, setNotEqualANDGreaterThan] = useState();
    var [notEqualANDLessThan, setNotEqualANDLessThan] = useState();
    var [notEqualANDGreaterThanEqualTo, setNotEqualANDGreaterThanEqualTo] = useState();
    var [notEqualANDLessThanEqualTo, setNotEqualANDLessThanEqualTo] = useState();
    var [notEqualANDGreaterThanANDLessThan, setNotEqualANDGreaterThanANDLessThan] = useState();
    var [notEqualANDGreaterThanEqualToANDLessThanEqualTo, setNotEqualANDGreaterThanEqualToANDLessThanEqualTo] = useState();

    const BarcodelengthToMatchAND =  (event) => {
        console.log(event);
        var header = "<Barcode Lenght>"
        if(event.includes("Greater Than AND Less Than:")){
            setGreaterThanANDLessThan(true);
            var barlenGreaterThan =parseInt(prompt("Greater Than: ")) ;    
            var barlenLessThan = parseInt(prompt("Less Than: ")) ; 
            if(isNaN(barlenGreaterThan)|| isNaN(barlenLessThan)){
                alert("Error sending values");
                return;
            }  
            console.log(barlenGreaterThan);
            console.log(barlenLessThan);
            if(barlenGreaterThan > barlenLessThan){
                alert("Error in expression: No Barcode Lenght can satisfy the specified conditions");
                return;
            }
            barLenghtGreatherThanANDLessThan = [barlenGreaterThan, barlenLessThan];
            if(notEqualANDGreaterThan !== true && notEqualANDLessThan !== true && notEqualANDGreaterThanEqualTo !== true && notEqualANDLessThanEqualTo !== true && notEqualANDGreaterThanANDLessThan !== true && notEqualANDGreaterThanEqualToANDLessThanEqualTo !== true && greaterThanEqualANDLessThanEqual !== true){
                setBarcodeLenghtLabelAND(header + "Greater Than: " + barlenGreaterThan + " AND " + "Less Than:" + barlenLessThan)
            }else{
               setBarcodeLenghtLabelAND(barcodeLenghtLabelAND + " OR " + "Greater Than: " + barlenGreaterThan + " AND " + "Less Than:" + barlenLessThan)

            }
        }
        if(event.includes("Greater Than/Equal To AND Less Than/Equal to:")){
            setGreaterThanEqualANDLessThanEqual(true);
            var barlenGreaterThanEqual =parseInt(prompt("Greater Than/Equal To: ")) ;    
            var barlenLessThanequal = parseInt(prompt("Less Than/Equal To: ")) ; 
            if(isNaN(barlenGreaterThanEqual)|| isNaN(barlenLessThanequal)){
                alert("Error sending values");
                return;
            }    
            console.log(barlenGreaterThanEqual);
            console.log(barlenLessThanequal);
            if(barlenGreaterThanEqual > barlenLessThanequal){
                alert("Error in expression: No Barcode Lenght can satisfy the specified conditions");
                return;
            }
            barLenghtGreatherThanEqualToANDLessThanEqualTo = [barlenGreaterThanEqual, barlenLessThanequal];
            if(notEqualANDGreaterThan !== true && notEqualANDLessThan !== true && notEqualANDGreaterThanEqualTo !== true && notEqualANDLessThanEqualTo !== true && notEqualANDGreaterThanANDLessThan !== true && notEqualANDGreaterThanEqualToANDLessThanEqualTo !== true && greaterThanEqualANDLessThanEqual !== true){
                setBarcodeLenghtLabelAND(header + "Greater Than/equal To: " + barlenGreaterThanEqual + " AND " + "Less Than/Equal To:" + barlenLessThanequal)
            }else{
               setBarcodeLenghtLabelAND(barcodeLenghtLabelAND + " OR " + "Greater Than: " + barlenGreaterThanEqual + " AND " + "Less Than:" + barlenLessThanequal)

            }
        }
        if(event.includes("Not Equal To AND Greater Than:")){
            setNotEqualANDGreaterThan(true);
           var barlenNotEqualTo = parseInt(prompt("Not Equal To: "));
           var barlenGreaterThan2 = parseInt(prompt("Greater Than: ")); 
           if(isNaN(barlenNotEqualTo)|| isNaN(barlenGreaterThan2)){
            alert("Error sending values");
            return;
        }    
            barLengthNotEqualANDGreaterThan = [barlenNotEqualTo, barlenGreaterThan2];
            if(greaterThanANDLessThan !== true && notEqualANDLessThan !== true && notEqualANDGreaterThanEqualTo !== true && notEqualANDLessThanEqualTo !== true && notEqualANDGreaterThanANDLessThan !== true && notEqualANDGreaterThanEqualToANDLessThanEqualTo !== true && greaterThanEqualANDLessThanEqual !== true){
                      setBarcodeLenghtLabelAND(header + "Not Equal To: " + barlenNotEqualTo + " AND " + "Greater Than: " + barlenGreaterThan2)
            }else{
                setBarcodeLenghtLabelAND(barcodeLenghtLabelAND + " OR " + "Not Equal To: " + barlenNotEqualTo + " AND " + "Greater Than: " + barlenGreaterThan2)
            }
        }
        if(event.includes("Not Equal To AND Less Than:")){
            setNotEqualANDLessThan(true);
            var barLenNotEqualTo2 = parseInt(prompt("Not Equal To: "));
            var barLenLessThan2 = parseInt(prompt("Less Than: "));
            if(isNaN(barLenNotEqualTo2)|| isNaN(barLenLessThan2)){
                alert("Error sending values");
                return;
            } 
            barLengthNotEqualANDLessThan = [barLenNotEqualTo2, barLenLessThan2];
            if(greaterThanANDLessThan !== true && notEqualANDGreaterThan !== true && notEqualANDGreaterThanEqualTo !== true && notEqualANDLessThanEqualTo !== true && notEqualANDGreaterThanANDLessThan !== true && notEqualANDGreaterThanEqualToANDLessThanEqualTo !== true && greaterThanEqualANDLessThanEqual !== true){
                     setBarcodeLenghtLabelAND(header + "Not Equal To:  " + barLenNotEqualTo2 + " AND " + "Less Than: " + barLenLessThan2)
            }else{
                setBarcodeLenghtLabelAND(barcodeLenghtLabelAND + " OR " + "Not Equal To:  " + barLenNotEqualTo2 + " AND " + "Less Than: " + barLenLessThan2)
            }
        }
        if(event.includes("Not Equal To AND Greater Than/Equal To:")){
            setNotEqualANDGreaterThanEqualTo(true);
            var barLenNotEqualTo3 = parseInt(prompt("Not Equal To: "));
            var barlenGreaterThanEqualTo = parseInt(prompt("Greater Than/Equal To: "));
            if(isNaN(barLenNotEqualTo3)|| isNaN(barlenGreaterThanEqualTo)){
                alert("Error sending values");
                return;
            } 
            barLengthNotEqualANDGreaterThanEqualTo = [barLenNotEqualTo3, barlenGreaterThanEqualTo];
            if(greaterThanANDLessThan !== true && notEqualANDGreaterThan !== true && notEqualANDLessThan !== true && notEqualANDLessThanEqualTo !== true && notEqualANDGreaterThanANDLessThan !== true && notEqualANDGreaterThanEqualToANDLessThanEqualTo !== true && greaterThanEqualANDLessThanEqual !== true){
                      setBarcodeLenghtLabelAND(header + "Not Equal To: " + barLenNotEqualTo3 + " AND " + "Greater Than/Equal To: " + barlenGreaterThanEqualTo);
            }else{
                setBarcodeLenghtLabelAND(barcodeLenghtLabelAND + " OR " + "Not Equal To: " + barLenNotEqualTo3 + " AND " + "Greater Than/Equal To: " + barlenGreaterThanEqualTo);

            }
        }
        if(event.includes("Not Equal To AND Less Than/Equal To:")){
            setNotEqualANDLessThanEqualTo(true);
            var barlenNotEqualTo4 = parseInt(prompt("Not Equal To: "));
            var barlenLessThanEqualTo = parseInt(prompt("Less Than/Equal To: "));
            if(isNaN(barlenNotEqualTo4)|| isNaN(barlenLessThanEqualTo)){
                alert("Error sending values");
                return;
            } 
            barLengthNotEqualANDLessThanEqualTo = [barlenNotEqualTo4, barlenLessThanEqualTo];
            if(greaterThanANDLessThan !== true && notEqualANDGreaterThan !== true && notEqualANDLessThan !== true && notEqualANDGreaterThanEqualTo !== true && notEqualANDGreaterThanANDLessThan !== true && notEqualANDGreaterThanEqualToANDLessThanEqualTo !== true && greaterThanEqualANDLessThanEqual !== true){
                     setBarcodeLenghtLabelAND(header + "Not Equal To: " + barlenNotEqualTo4 + " AND " + "Less Than/Equal To: " + barlenLessThanEqualTo)
            }else{
                setBarcodeLenghtLabelAND(barcodeLenghtLabelAND + " OR " + "Not Equal To: " + barlenNotEqualTo4 + " AND " + "Less Than/Equal To: " + barlenLessThanEqualTo)

            }
        }
        if(event.includes("Not Equal To AND Greater Than AND Less Than:")){
            setNotEqualANDGreaterThanANDLessThan(true);
            var barlenNotEqualTo5 = parseInt(prompt("Not Equal To: "));
            var barlenGreaterThan3 = parseInt(prompt("Greater Than: "));
            var barlenLessThan3 = parseInt(prompt("Less Than: "));
            if(isNaN(barlenNotEqualTo5)|| isNaN(barlenGreaterThan3 || isNaN(barlenLessThan3))){
                alert("Error sending values");
                return;
            } 
            if(barlenGreaterThan3 > barlenLessThan3){
                alert("Error in expression: No Barcode Lenght can satisfy the specified conditions");
                return;
            }
            barLengthNotEqualANDGreaterThanANDLessThan = [barlenNotEqualTo5, barlenGreaterThan3, barlenLessThan3];
            if(greaterThanANDLessThan !== true && notEqualANDGreaterThan !== true && notEqualANDLessThan !== true && notEqualANDGreaterThanEqualTo !== true && notEqualANDLessThanEqualTo !== true && notEqualANDGreaterThanEqualToANDLessThanEqualTo !== true && greaterThanEqualANDLessThanEqual !== true){
                     setBarcodeLenghtLabelAND(header + "Not Equal To: " + barlenNotEqualTo5 + " AND " + "Greater Than: " + barlenGreaterThan3 + " AND " + "Less Than: " + barlenLessThan3)
            }else{
                setBarcodeLenghtLabelAND(barcodeLenghtLabelAND + " OR " + "Not Equal To: " + barlenNotEqualTo5 + " AND " + "Greater Than: " + barlenGreaterThan3 + " AND " + "Less Than: " + barlenLessThan3)

            }
        }
        if(event.includes("Not Equal To AND Greater Than/Equal To AND Less Than/Equal To:")){
            setNotEqualANDGreaterThanEqualToANDLessThanEqualTo(true);
            var barlenNotEqualTo6 = parseInt(prompt("Not Equal To: "));
            var barlenGreaterThanEqualTo2 = parseInt(prompt("Greater Than/Equal To: "));
            var barlenLessThanEqualTo2 = parseInt(prompt("Less Than/Equal To: "));
            if(isNaN(barlenNotEqualTo6)|| isNaN(barlenGreaterThanEqualTo2 || isNaN(barlenLessThanEqualTo2))){
                alert("Error sending values");
                return;
            } 
            if(barlenGreaterThanEqualTo2 > barlenLessThanEqualTo2){
                alert("Error in expression: No Barcode Lenght can satisfy the specified conditions");
                return;
            }
            barLengthNotEqualANDGreaterThanEqualToANDLessThanEqualTo = [barlenNotEqualTo6, barlenGreaterThanEqualTo2, barlenLessThanEqualTo2];
            console.log(barLengthNotEqualANDGreaterThanEqualToANDLessThanEqualTo);
            if(greaterThanANDLessThan !== true && notEqualANDGreaterThan !== true && notEqualANDLessThan !== true && notEqualANDGreaterThanEqualTo !== true && notEqualANDLessThanEqualTo !== true && notEqualANDGreaterThanANDLessThan !== true && greaterThanEqualANDLessThanEqual !== true){
                     setBarcodeLenghtLabelAND(header + "Not Equal To: " + barlenNotEqualTo6 + " AND " + "Greater Than/Equal To: " + barlenGreaterThanEqualTo2 + " AND " + "Less Than/Equal To: " + barlenLessThanEqualTo2)
            }else{
                setBarcodeLenghtLabelAND(barcodeLenghtLabelAND + " OR " + "Not Equal To: " + barlenNotEqualTo6 + " AND " + "Greater Than/Equal To: " + barlenGreaterThanEqualTo2 + " AND " + "Less Than/Equal To: " + barlenLessThanEqualTo2)

            }
        }
        if(event.includes("Clear All")){
             barLenghtGreatherThanANDLessThan = [];
             barLenghtGreatherThanEqualToANDLessThanEqualTo = [];
             barLengthNotEqualANDGreaterThan = [];
             barLengthNotEqualANDLessThan = [];
             barLengthNotEqualANDGreaterThanEqualTo = [];
             barLengthNotEqualANDLessThanEqualTo = [];
             barLengthNotEqualANDGreaterThanANDLessThan = [];
             barLengthNotEqualANDGreaterThanEqualToANDLessThanEqualTo = [];
            setBarcodeLenghtLabelAND(null);
            setGreaterThanANDLessThan(false);
            setGreaterThanEqualANDLessThanEqual(false)
            setNotEqualANDGreaterThan(false);
            setNotEqualANDLessThan(false);
            setNotEqualANDGreaterThanEqualTo(false);
            setNotEqualANDLessThanEqualTo(false);
            setNotEqualANDGreaterThanANDLessThan(false);
            setNotEqualANDGreaterThanEqualToANDLessThanEqualTo(false);

        }
    }
    const [StartsWith , setStartsWith] = useState();
    const [EndsWith, setEndsWith] = useState();
    const [Contains , setContains] = useState();
    function MetaCharReplaced(string) {

        if(string.includes("/")){
            string = string.replace(/\//g, "/2F");
         }
        if(string.includes("!")){
           string = string.replace(/\!/g, '/21');
        }
        if(string.includes("#")){
            string = string.replace(/\#/g, "/23");
         }
         if(string.includes("$")){
            string = string.replace(/\$/g, "/24");
         }
         if(string.includes("*")){
            string = string.replace(/\*/g, "/2A");
         }
         if(string.includes(",")){
            string = string.replace(/\,/g, "/2C");
         }
         if(string.includes("^")){
            string = string.replace(/\^/g, "/5E");
         }
         if(string.includes("|")){
            string = string.replace(/\|/g, "/7C");
         }
         return string;
    }
    const BarcodeDataToMatch =  (e) => {
        console.log(e);
       var header = "<Barcode Data>";

       if(e.includes("Starts With:")){
        setStartsWith(true)
           var startsWith = prompt("Barcode Starts With: ");
           if(startsWith == null || startsWith === ""){
            return;
        }
           barDataStarts = MetaCharReplaced(startsWith);
           if(EndsWith !== true && Contains !== true){
            getBarcodeDataOR(header + "Starts With: " + startsWith);
           }else{
            getBarcodeDataOR(barcodeDataOR + " OR " + "Starts With: " + startsWith);
           }
           
       }
       if(e.includes("Ends With:")){
            setEndsWith(true);
            var endsWith = prompt("Barcode Ends With: ");
            if(endsWith == null || endsWith === ""){
                return;
            }
            barDataEnds = MetaCharReplaced(endsWith);
            if(StartsWith !== true && Contains !== true){
                getBarcodeDataOR(header + "Ends With: " + endsWith);
               }else{
                getBarcodeDataOR(barcodeDataOR + " OR " + "Ends With: " + endsWith);
               }
        }
        if(e.includes("Contains:")){
            setContains(true);
            var contains = prompt("Barcode Contains: ");
            if(contains == null || contains === ""){
                return;
            }
            barDataContains = MetaCharReplaced(contains);
            if(StartsWith !== true && EndsWith !== true){
                getBarcodeDataOR(header + "Contains: " + contains);
               }else{
                getBarcodeDataOR(barcodeDataOR + " OR " + "Contains:  " + contains);
               }
        }
        if(e.includes("Clear All")){
            barDataStarts = "";
            barDataEnds = "";
            barDataContains = "";
            getBarcodeDataOR(null);
            setStartsWith(false);
            setEndsWith(false);
            setContains(false);
        }
    }
    const [StartsANDEnds , setStartsANDEnds] = useState();
    const [StartsANDContains, setStartsANDContains] = useState();
    const [ContainsANDEnd , setContainsANDEnds] = useState();
    const [StartsANDContainsANDEnds, setStartsANDContainsANDEnds] = useState();
    
    const BarcodeDataToMatchAND =  (e) => {
        console.log(e);
       var header = "<Barcode Data>";

       if(e.includes("Starts AND Ends With:")){
        setStartsANDEnds(true);
           var startsWith = prompt("Barcode Starts With:");
           var endsWith = prompt("Barcode Ends With: ");
           if(isNaN(startsWith)|| isNaN(endsWith)){
            alert("Error sending values");
            return;
        } 
           barDataStartsANDEnds = [MetaCharReplaced(startsWith), MetaCharReplaced(endsWith)];
           if(StartsANDContains !== true && ContainsANDEnd !== true && StartsANDContainsANDEnds !== true){
             getBarcodeDataAND(header + "Starts With: " + startsWith + " AND " + "Ends With: " + endsWith);
           }else{
            getBarcodeDataAND(barcodeDataAND + " OR " + "Starts With: " + startsWith + " AND " + "Ends With: " + endsWith);

           }
       }
       if(e.includes("Starts AND Contains:")){
           setStartsANDContains(true)
           var startsWith2 = prompt("Barcode Starts With: ")
        var contains = prompt("Barcode Contains: ");
        if(isNaN(startsWith2)|| isNaN(contains)){
            alert("Error sending values");
            return;
        } 
        barDataStartsANDContains = [MetaCharReplaced(startsWith2), MetaCharReplaced(contains)];
        if(StartsANDEnds !== true && ContainsANDEnd !== true && StartsANDContainsANDEnds !== true){
            getBarcodeDataAND(header + "Starts With: " + startsWith2 + " AND " + "Contains: " + contains);
          }else{
           getBarcodeDataAND(barcodeDataAND + " OR " + "Starts With: " + startsWith2 + " AND " + "Contains: " + contains);
          }    
        }
    if(e.includes("Contains AND Ends With:")){
        setContainsANDEnds(true);
        var contains2 = prompt("Barcode Contains: ");
        var endsWith2 = prompt("Barcode Ends With: ");
         if(isNaN(contains2)|| isNaN(endsWith2)){
            alert("Error sending values");
            return;
        } 
        barDataContainsANDEnds = [MetaCharReplaced(contains2), MetaCharReplaced(endsWith2)];
        if(StartsANDEnds !== true && StartsANDContains !== true && StartsANDContainsANDEnds !== true){
            getBarcodeDataAND(header + "Contains: " + contains2 + " AND " + "Ends With: " + endsWith2);
          }else{
           getBarcodeDataAND(barcodeDataAND + " OR " + "Contains: " + contains2 + " AND " + "Ends With: " + endsWith2);

          }
    }
    if(e.includes("Start AND Contains AND Ends with:")){
        setStartsANDContainsANDEnds(true)
        var startsWith3 = prompt("Barcode Starts With: ")
        var contains3 = prompt("Barcode Contains: ");
        var endsWith3 = prompt("Barcode Ends With: ");
        if(isNaN(startsWith3)|| isNaN(contains3) || isNaN(endsWith3)){
            alert("Error sending values");
            return;
        }
        barDataStartsANDContainsANDEnds = [MetaCharReplaced(startsWith3), MetaCharReplaced(contains3), MetaCharReplaced(endsWith3)];
     if(StartsANDEnds !== true && StartsANDContains !== true && ContainsANDEnd !== true){
        getBarcodeDataAND(header + "Starts With: " + startsWith3 + " AND " + "Contains: " + contains3 + " AND " + "Ends With: " + endsWith3);
       }else{
        getBarcodeDataAND(barcodeDataAND + " OR " + "Starts With: " + startsWith3 + " AND " + "Contains: " + contains3 + " AND " + "Ends With: " + endsWith3);
       }     
     }
    if(e.includes("Clear All")){
        getBarcodeDataAND(null);
        setStartsANDEnds(false);
        setStartsANDContains(false);
        setContainsANDEnds(false);
        setStartsANDContainsANDEnds(false);
        barDataStartsANDEnds = "";
        barDataContainsANDEnds = "";
        barDataStartsANDContains = "";
        barDataStartsANDContainsANDEnds = "";
    }
    }
    optionsSelectedInTotal = symnologiesMatched + "\n" + barcodeLenghtLabel + "\n" + barcodeLenghtLabelAND + "\n" + barcodeDataOR + "\n" + barcodeDataAND;
    optionsSelectedInTotal = optionsSelectedInTotal.replace(/undefined/g, "");
    
    return ( 
        <>
        <small><p class="text">Conditions to match</p></small>
                                <div class="box">
                                    
                                    <div>
                                    <br/>
                                    <small>Symbologies to Match:</small>
                                    <Multiselect 
                                    isObject={false}
                                     onSelect={SymbologiesToMatch}
                                     onRemove={SymbologiesToMatch}
                                     options={symbologiesToMatchOptions}
                                     showArrow
                                     hidePlaceholder
                                     
                                     />
                                                    
                                                    <small>Barcode length to Match: &nbsp;&nbsp;
                                                    <small>
                                    <input type="radio" id="radioORBL" value="1" checked={selectedCondition1 == 1 ? true : false} onChange={RadioSelectionChange1}/> OR &nbsp;&nbsp; 
                                    <input type="radio" value="2" checked={selectedCondition1 == 2 ? true : false} onChange={RadioSelectionChange1}/> AND
                                    
                                    </small>
                                                    </small>
                                                    
                                    <Multiselect id="SingleMultiSelect"
                                    isObject={false}
                                    options={selectedCondition1 == 1 ? barcodeLengthOptions : barcodeLengthOptions2} 
                                    onSelect={selectedCondition1 == 1 ? BarcodelengthToMatchOR : BarcodelengthToMatchAND}
                                    onRemove={selectedCondition1 == 1 ? BarcodelengthToMatchOR : BarcodelengthToMatchAND}
                                    singleSelect
                                    />
                                   
                                        <small>Barcode Data to Match:
                                        <small>
                                    <input type="radio" value="1" checked={selectedCondition2 == 1 ? true : false} onChange={RadioSelectionChange2} /> OR &nbsp;&nbsp; 
                                    <input type="radio" value="2" checked={selectedCondition2 == 2 ? true : false} onChange={RadioSelectionChange2}/> AND
                                    
                                    </small> </small>
                                    <Multiselect 
                                    isObject={false}
                                    options={selectedCondition2 == 1 ? barcodeDataOptions : barcodeDataOptions2} 
                                    onSelect={selectedCondition2 == 1 ? BarcodeDataToMatch : BarcodeDataToMatchAND}
                                    onRemove={selectedCondition2 == 1 ? BarcodeDataToMatch : BarcodeDataToMatchAND}
                                    singleSelect
                                    />
                                    </div>
                                    
                                    <div id="databox" class="databoxChosenConditions">     
                                    <small><p class="text">Chosen conditions</p></small>   
                                                            <div id="symbologies">
                                                                <br/>
                                                                {symnologiesMatched}
                                                                
                                                            </div>
                                                            <div id="barcodelength">
                                                                    {barcodeLenghtLabel}<br/>
                                                                    {barcodeLenghtLabelAND}<br/>
                                                                   
                                                                    
                                                            </div>
                                                            <div id="barcodeData">
                                                                {barcodeDataOR}<br/>
                                                                {barcodeDataAND}<br/>
                                                                
                                                                </div>    
                                    </div>
                                </div>
        </>
    );
}
    
    
    
        
        
    

 
export default App;
