import React, {useState, useRef} from 'react';
import axios from 'axios';
import {Button} from '@material-ui/core';
import { useReactToPrint } from 'react-to-print';
import {SymbologiesSelectedByUser, SeparatorStringSelectedByUser, OutputSuccessFormatSelectedByUser, OutputFailureFormatSelectedByUser} from "./UDIParsing";


    function App(){

        const [imageBarcode, setImageBarcode] = useState('');
        const [showDownloadButton, setShowDownloadButton] = useState(false);

    const buttonClick = () =>{
        var symbologies = SymbologiesSelectedByUser();
        var separator = SeparatorStringSelectedByUser();
        var successOutput = OutputSuccessFormatSelectedByUser();
        var failureOutput = OutputFailureFormatSelectedByUser();
        let header = "\x01Y\x1d\x02";
        let split = "\x03";
        let footer = "\x03\x04";
        let GS1ValidationCommand = "CDOPSDV6";  
        let GS1ConfigStringCommand = "CDOPSUD"; 
        var endMarker = "\\x" + "01@";
        var SYMB_Sum = 0;
        var SYMB_Ex_Sum = 0;
        var symbologieFinal = "";
        var separatorFinal = "";
        var successOutputFinal = "";
        var failureOutputFinal = "";
        var UDIFinalString = "";

        console.log("Symbologie: " + symbologies);
        console.log("Separator: " + separator);
        console.log("Success Output: " + successOutput);
        console.log("Failure output: " + failureOutput);

        
        if(symbologies.includes("Australian Post")){
            SYMB_Ex_Sum = SYMB_Ex_Sum + 32768;
            
        }
        if(symbologies.includes("Aztec")){
            SYMB_Sum = SYMB_Sum + 8;
        }
        if(symbologies.includes("BC412")){
            SYMB_Ex_Sum = SYMB_Ex_Sum + 33554432;
        }
        if(symbologies.includes("Canada Post")){
            SYMB_Ex_Sum = SYMB_Ex_Sum + 16777216;
        }
        if(symbologies.includes("Codabar")){
            SYMB_Sum = SYMB_Sum + 4096;
        }
        if(symbologies.includes("Codablock F")){
            SYMB_Ex_Sum = SYMB_Ex_Sum + 2048;
        }
        if(symbologies.includes("Code 11")){
            SYMB_Ex_Sum = SYMB_Ex_Sum + 1;
        }
        if(symbologies.includes("Code 32")){
            SYMB_Ex_Sum = SYMB_Ex_Sum + 2;
        }
        if(symbologies.includes("Code 39")){
            SYMB_Sum = SYMB_Sum + 1024;
        }
        if(symbologies.includes("Code 49")){
            SYMB_Ex_Sum = SYMB_Ex_Sum + 512;
        }
        if(symbologies.includes("Code 93")){
            SYMB_Sum = SYMB_Sum + 16384;
        }
        if(symbologies.includes("Code 128")){
            SYMB_Sum = SYMB_Sum + 8192;
        }
        if(symbologies.includes("Composite Code A")){
            SYMB_Sum = SYMB_Sum + 128;
        }
        if(symbologies.includes("Composite Code B")){
            SYMB_Sum = SYMB_Sum + 256;
        }
        if(symbologies.includes("Composite Code C")){
            SYMB_Sum = SYMB_Sum + 512;
        }
        if(symbologies.includes("Data Matrix")){
            SYMB_Sum = SYMB_Sum + 2;
        }
        if(symbologies.includes("Dutch Post (KIX)")){
            SYMB_Ex_Sum = SYMB_Ex_Sum + 65536;
        }
        if(symbologies.includes("EAN-13")){
            SYMB_Sum = SYMB_Sum + 131072;
        }
        if(symbologies.includes("EAN-8")){
            SYMB_Sum = SYMB_Sum + 262144;
        }
        if(symbologies.includes("GoCode")){
            SYMB_Sum = SYMB_Sum + 1;
        }
        if(symbologies.includes("Grid Matrix")){
            SYMB_Sum = SYMB_Sum + 134217728;
        }
        if(symbologies.includes("GS1 Databar")){
            SYMB_Sum = SYMB_Sum + 524288;
        }
        if(symbologies.includes("GS1 Databar Expanded")){
            SYMB_Sum = SYMB_Sum + 4194304;
        }
        if(symbologies.includes("GS1 Databar Expanded Stacked")){
            SYMB_Sum = SYMB_Sum + 8388608;
        }
        if(symbologies.includes("GS1 Databar Limited")){
            SYMB_Sum = SYMB_Sum + 2097152;
        }
        if(symbologies.includes("GS1 Databar Stacked")){
            SYMB_Sum = SYMB_Sum + 1048576;
        }
        if(symbologies.includes("HanXin")){
            SYMB_Sum = SYMB_Sum + 16777216;
        }
        if(symbologies.includes("Hong Kong 2 of 5")){
            SYMB_Ex_Sum = SYMB_Ex_Sum + 2097152;
        }
        if(symbologies.includes("Interleaved 2 of 5")){
            SYMB_Sum = SYMB_Sum + 2048;
        }
        if(symbologies.includes("Japan Post")){
            SYMB_Ex_Sum = SYMB_Ex_Sum + 131072;
        }
        if(symbologies.includes("korea Post")){
            SYMB_Ex_Sum = SYMB_Ex_Sum + 1048576;
        }
        if(symbologies.includes("Matrix 2 of 5")){
            SYMB_Ex_Sum = SYMB_Ex_Sum + 128;
        }
        if(symbologies.includes("Maxicode")){
            SYMB_Sum = SYMB_Sum + 16;
        }
        if(symbologies.includes("Micro QR Code")){
            SYMB_Sum = SYMB_Sum + 33554432;
        }
        if(symbologies.includes("MicroPDF417")){
            SYMB_Sum = SYMB_Sum + 64;
        }
        if(symbologies.includes("MSI Plessey")){
            SYMB_Ex_Sum = SYMB_Ex_Sum + 8;
        }
        if(symbologies.includes("NEC 2 of 5")){
            SYMB_Ex_Sum = SYMB_Ex_Sum + 4194304;
        }
        if(symbologies.includes("PDF417")){
            SYMB_Sum = SYMB_Sum + 32;
        }
        if(symbologies.includes("Pharma Code")){
            SYMB_Ex_Sum = SYMB_Ex_Sum + 64;
        }
        if(symbologies.includes("QR Code")){
            SYMB_Sum = SYMB_Sum + 4;
        }
        if(symbologies.includes("QR Code Model 1")){
            SYMB_Sum = SYMB_Sum + 67108864;
        }
        if(symbologies.includes("Straight 2 of 5 (3 start/stop bars)")){
            SYMB_Ex_Sum = SYMB_Ex_Sum + 256;
        }
        if(symbologies.includes("Straight IATA 2 of 5 (2 start/stop bars)")){
            SYMB_Ex_Sum = SYMB_Ex_Sum + 8388608;
        }
        if(symbologies.includes("Telepen")){
            SYMB_Ex_Sum = SYMB_Ex_Sum + 16;
        }
        if(symbologies.includes("TriOptic")){
            SYMB_Ex_Sum = SYMB_Ex_Sum + 32;
        }
        if(symbologies.includes("UK Plessey")){
            SYMB_Ex_Sum = SYMB_Ex_Sum + 4;
        }
        if(symbologies.includes("UK Royal Mail")){
            SYMB_Ex_Sum = SYMB_Ex_Sum + 262144;
        }
        if(symbologies.includes("UPC-A")){
            SYMB_Sum = SYMB_Sum + 32768;
        }
        if(symbologies.includes("UPC-E")){
            SYMB_Sum = SYMB_Sum + 65536;
        }
        if(symbologies.includes("UPU ID")){
            SYMB_Ex_Sum = SYMB_Ex_Sum + 524288;
        }
        if(symbologies.includes("USPS Intelligent Mail")){
            SYMB_Ex_Sum = SYMB_Ex_Sum + 16384;
        }
        if(symbologies.includes("USPS Planet")){
            SYMB_Ex_Sum = SYMB_Ex_Sum + 8192;
        }
        if(symbologies.includes("USPS Postnet")){
            SYMB_Ex_Sum = SYMB_Ex_Sum + 4096;
        }
        if(symbologies.includes("All") || symbologies[0] === undefined){
            symbologieFinal = "00" + "\\x" + "2B" + "\\x" + "5F" + "\\x" + "61" + "\\x" + "49" + "\\x" + "60" + "\\x" + "2B" + "\\x" + "5F" + "\\x" + "61" + "\\x" + "49" + "\\x" + "60";
        }else{

            function padLeadingZeros(num, size) {
                var s = num+"";
                while (s.length < size) s = "0" + s;
                return s;
            }
            var TotalSumSymbEx = padLeadingZeros(SYMB_Ex_Sum, 10);
            var TotalSumSymb = padLeadingZeros(SYMB_Sum, 10);
            console.log("Symb Ex Sum: " + TotalSumSymbEx);
            console.log("Symb Sum: " + TotalSumSymb);
            var splitNumber = TotalSumSymb.split("");
            var splitExNumber = TotalSumSymbEx.split("");
            console.log(TotalSumSymb);
            
            for(var i = 0; i < 10; i++){
            
                if( i === 0){
                    var splitPart1 = parseInt((splitNumber[i] + splitNumber[i+1])) + 1;
                    if(splitPart1 < 16){
                        splitPart1 = splitPart1.toString(16);
                        var formattedNumber1 = (("0" + splitPart1).slice(-2)).toUpperCase();
                        console.log(formattedNumber1);
                    }else{
                        formattedNumber1 = (splitPart1.toString(16)).toUpperCase();
                        console.log(splitPart1);
                    }
                    
                }
                if( i === 2){
                    var splitPart2 = parseInt((splitNumber[i] + splitNumber[i+1])) + 1;
                    if(splitPart2 < 16){
                        splitPart2 = splitPart2.toString(16);
                        var formattedNumber2 = (("0" + splitPart2).slice(-2)).toUpperCase();
                        console.log(formattedNumber2);
                    }else{
                        formattedNumber2 = (splitPart2.toString(16)).toUpperCase();
                        console.log(splitPart2);
                    }
                }
                if( i === 4){
                    var splitPart3 = parseInt((splitNumber[i] + splitNumber[i+1])) + 1;
                    if(splitPart3 < 16){
                        splitPart3 = splitPart3.toString(16);
                        var formattedNumber3 = (("0" + splitPart3).slice(-2)).toUpperCase();
                        console.log(formattedNumber3);
                    }else{
                        formattedNumber3 = (splitPart3.toString(16)).toUpperCase();
                        console.log(splitPart3);
                    }
                }
                if( i === 6){
                    var splitPart4 = parseInt((splitNumber[i] + splitNumber[i+1])) + 1;
                    if(splitPart4 < 16){
                        splitPart4 = splitPart4.toString(16);
                        var formattedNumber4 = (("0" + splitPart4).slice(-2)).toUpperCase();
                        console.log(formattedNumber4);
                    }else{
                        formattedNumber4 = (splitPart4.toString(16)).toUpperCase();
                        console.log(splitPart4);
                    }
                }
                if( i === 8){
                    var splitPart5 = parseInt((splitNumber[i] + splitNumber[i+1])) + 1;
                    if(splitPart5 < 16){
                        splitPart5 = splitPart5.toString(16);
                        var formattedNumber5 = (("0" + splitPart5).slice(-2)).toUpperCase();
                        console.log(formattedNumber5);
                    }else{
                        formattedNumber5 = (splitPart5.toString(16)).toUpperCase();
                        console.log(splitPart5);
                    }
                }
                
            }
            console.log(splitExNumber);
            for(var j = 0; j < 10; j++){
            
                if( j === 0){
                    var splitExPart1 = parseInt((splitExNumber[j] + splitExNumber[j+1])) + 1;
                    if(splitExPart1 < 16){
                        splitExPart1 = splitExPart1.toString(16);
                        var formattedExNumber1 = (("0" + splitExPart1).slice(-2)).toUpperCase();
                        console.log(formattedExNumber1);
                    }else{
                        formattedExNumber1 = (splitExPart1.toString(16)).toUpperCase();
                        console.log(splitExPart1);
                    }
                    
                }
                if( j === 2){
                    var splitExPart2 = parseInt((splitExNumber[j] + splitExNumber[j+1])) + 1;
                    if(splitExPart2 < 16){
                        splitExPart2 = splitExPart2.toString(16);
                        var formattedExNumber2 = (("0" + splitExPart2).slice(-2)).toUpperCase();
                        console.log(formattedExNumber2);
                    }else{
                        formattedExNumber2 = (splitExPart2.toString(16)).toUpperCase();
                        console.log(splitExPart2);
                    }
                }
                if( j === 4){
                    var splitExPart3 = parseInt((splitExNumber[j] + splitExNumber[j+1])) + 1;
                    if(splitExPart3 < 16){
                        splitExPart3 = splitExPart3.toString(16);
                        var formattedExNumber3 = (("0" + splitExPart3).slice(-2)).toUpperCase();
                        console.log(formattedExNumber3);
                    }else{
                        formattedExNumber3 = (splitExPart3.toString(16)).toUpperCase();
                        console.log(splitExPart3);
                    }
                }
                if( j === 6){
                    var splitExPart4 = parseInt((splitExNumber[j] + splitExNumber[j+1])) + 1;
                    if(splitExPart4 < 16){
                        splitExPart4 = splitExPart4.toString(16);
                        var formattedExNumber4 = (("0" + splitExPart4).slice(-2)).toUpperCase();
                        console.log(formattedExNumber4);
                    }else{
                        formattedExNumber4 = (splitExPart4.toString(16)).toUpperCase();
                        console.log(splitExPart4);
                    }
                }
                if( j === 8){
                    var splitExPart5 = parseInt((splitExNumber[j] + splitExNumber[j+1])) + 1;
                    if(splitExPart5 < 16){
                        splitExPart5 = splitExPart5.toString(16);
                        var formattedExNumber5 = (("0" + splitExPart5).slice(-2)).toUpperCase();
                        console.log(formattedExNumber5);
                    }else{
                        formattedExNumber5 = (splitExPart5.toString(16)).toUpperCase();
                        console.log(splitExPart5);
                    }
                }
                
            }
            var SymbologyBitsWithoutHexadecimal = "00" + "\\x" + formattedNumber1 + "\\x" + formattedNumber2 + "\\x" + formattedNumber3 + "\\x" + formattedNumber4 +
            "\\x" + formattedNumber5 + "\\x" + formattedExNumber1 + "\\x" + formattedExNumber2 + "\\x" + formattedExNumber3 + "\\x" + formattedExNumber4
            + "\\x" + formattedExNumber5;

           var SymbologyBitsFinal = SymbologyBitsWithoutHexadecimal.replace(/\/\//g, "/");
           symbologieFinal = SymbologyBitsWithoutHexadecimal.replace(/\/\//g, "/");
            console.log(SymbologyBitsFinal);
            console.log(symbologieFinal);
        }

        if(separator === "" || separator === "None"){
            // do nothing
        }else{
            if(separator == "Group Separator"){
                separatorFinal = "#/1D";
            }
            if(separator == "Carriage return and line feed"){
                separatorFinal = "#/0D/0A";
            }
            if(separator == "< >"){
                separatorFinal = "#<>";
            }
            console.log("separator: " + separatorFinal);
        }
        if(successOutput === "" || successOutput === "None"){
            // do nothing
        }else{
            if(successOutput == "Demo Output With Detail Data"){
                    successOutputFinal = "^0";
            }
            if(successOutput == "[(DI)][DI attribute data]"){
                successOutputFinal = "^1";
            }
            if(successOutput == "[(PI)][PI attribute data]"){
                successOutputFinal = "^2";
            }
            if(successOutput == "[(DI) + (PI)][DI attribute data + PI attribute data]"){
                successOutputFinal = "^3";
            }
            if(successOutput == "[DI attribute data]"){
                successOutputFinal = "^4";
            }
            if(successOutput == "[PI attribute data]"){
                successOutputFinal = "^5";
            }
            if(successOutput == "[DI attribute data + PI attribute data]"){
                successOutputFinal = "^6";
            }
            console.log("Success Output: " + successOutputFinal);
        }
        if(failureOutput === "" || failureOutput === "No output data"){
            //do nothing
        }else{
            if(failureOutput == "[<ERROR: error message>][space][decoded data]"){
                failureOutputFinal = "^B";
            }
            if(failureOutput == "[Decoded Data]"){
                failureOutputFinal = "^C";
            }
            console.log("Failure oputput: " + failureOutputFinal);
        }
       
   
       if(successOutputFinal !== ""){
        UDIFinalString = UDIFinalString + successOutputFinal;
       }
       if(failureOutputFinal !== ""){
        UDIFinalString = UDIFinalString + failureOutputFinal;
       }
       if(separatorFinal !== ""){
        UDIFinalString = UDIFinalString + separatorFinal;
       }
       
       var numberSegments = UDIFinalString.length + 12;
        var hexaNumberSegments;
        if(numberSegments < 16){
             hexaNumberSegments = "00" + numberSegments.toString(16).toUpperCase();
        }else{
             hexaNumberSegments = "0" + numberSegments.toString(16).toUpperCase();
        }
        UDIFinalString = "\"" + symbologieFinal + UDIFinalString + hexaNumberSegments + endMarker + "\"";
        UDIFinalString = header + GS1ValidationCommand + split + GS1ConfigStringCommand + UDIFinalString + footer;
       console.log(UDIFinalString);

        
        const webApiRequest = 'https://Barcodegen.ecs-dev.codecorp.com:5000/barcodegen/';
        var formData = new FormData();
         formData.append("BarcodeString", "UDIParsing");
         formData.append("BarcodeData", UDIFinalString);
       axios({
         method: "POST",
         url: webApiRequest,
         data: formData,
         headers: { "Content-Type": "multipart/form-data"},
         responseType: "blob"
       }).then((response) => {
         console.log(response);
         const reader = new window.FileReader();
         reader.readAsDataURL(response.data);
         reader.onload = () =>{
            setImageBarcode(reader.result);
         }
            setShowDownloadButton(true);
       });
    }
    const componentRef = useRef();
    const pageStyle = " @page { size: 8in 5in }"
                       
    const handlePrint = useReactToPrint({
        content : () => componentRef.current,
        documentTitle: 'Simple Parsing Data',
    });
    
    return (
        <>
 <div class="downloadButtonGS1">
                   <button onClick={buttonClick}>Generate Code</button>
        </div>
        <div class='BarcodeGS1' ref={componentRef}>
            <style>{pageStyle}</style>
        {imageBarcode ? (<img src={imageBarcode} alt='img' width="500" height="500"></img>) : null} 
        <div class="optionsSelected">
                 
                 
        </div>
        </div>
        {showDownloadButton &&
        <div id="DownloadImageUDI" class="downloadButtonGS1">
              <Button variant="outlined" color="primary" href={imageBarcode} download="UDI Parsing Rule" > Download  </Button> 
             <div class="printButton">
             <Button variant="outlined" color="primary" onClick={handlePrint}>Print</Button>
             </div>
        </div>}<br/>
        </>
        );
}


export default App;