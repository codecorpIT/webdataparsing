import React, { useRef, useState } from 'react';
import ConditionsToMatch from './ConditionsToMatchComponent';
import OperationsToPerform from './OperationsToPerformOnBarcodeComponent';
//import DefinedParsingActions from './DefinedParsingActionsComponent';
import EffectOfParsing from './EffectofParsingComponent';
import { useReactToPrint } from 'react-to-print';
import CreateBarcodeRules from './DPGetBarcodeRulesCreated';
import {Button} from '@material-ui/core';

function DataParsing() {
   
const [effectOnParsing, setEffectOnParsing] = useState('CortexTools2');

    
        return ( 
                
                <div class="containerbox" >   
                <div class="leftpanel2">     
                            <div>
                            <EffectOfParsing setEffectOnParsing={setEffectOnParsing}></EffectOfParsing>  
                             
                            </div>   
                                
                                                                                                                               
                            </div>
                        <div class="newaction">
                                
                                    <h5>New Action</h5>
                                    <ConditionsToMatch />
                                    <OperationsToPerform effectOnParsing={effectOnParsing}/>
                                    <div className="createbarcode">    
                                         <CreateBarcodeRules />              
                                 </div>                                      
                        </div>
                        
                
                
                </div>
                
                
    );

}
         
 export default DataParsing;