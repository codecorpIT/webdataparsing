from node:16.1.0-buster
WORKDIR /webdataparsing/

RUN apt -y update
RUN apt -y upgrade

COPY . .
RUN npm install

EXPOSE 3000

ENTRYPOINT ["npm", "start"]
